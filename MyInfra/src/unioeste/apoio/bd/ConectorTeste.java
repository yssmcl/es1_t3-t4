package unioeste.apoio.bd;


import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ConectorTeste {
//	String sql = ;
//	private InitialContext ctx = null;
//	private DataSource ds = null;
//	private Connection conn = null;
//	private PreparedStatement ps = null;
//	private ResultSet rs = null;

//	public void teste() throws NamingException, SQLException {
////		Hashtable env = new Hashtable();
////		env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
////		ctx = new InitialContext(env);
//
////		Properties props = new Properties();
////		props.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
////		props.put(Context.PROVIDER_URL, "http://localhost:1099");
////		ctx = new InitialContext(props);
//
//		Properties props = new Properties();
//		props.setProperty("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
//		props.setProperty("java.naming.factory.url.pkgs", "org.jboss.naming");
//		props.setProperty("java.naming.provider.url", "jnp://localhost:1099");
//		ctx = new InitialContext(props);
//
//		ds = (DataSource) ctx.lookup("jdbc/MySqlNaoExistente");
//		conn = ds.getConnection();
//		ps = conn.prepareStatement(sql);
//
//		rs = ps.executeQuery();
//		rs.next();
//		System.out.println(rs.getString("nomeTipoProduto"));
//
//		if (rs != null) rs.close();
//		if (ps != null) ps.close();
//		if (conn != null) conn.close();
//		if (ctx != null) ctx.close();
//	}

	public static void main(String[] args) throws Exception {
		LocalContext ctx = LocalContextFactory.createLocalContext("com.mysql.jdbc.Driver");
		ctx.addDataSource("jdbc/MySqlNaoExistente", "jdbc:mysql://localhost/faturamento", "root", "root");

		DataSource ds = (DataSource) new InitialContext().lookup("jdbc/MySqlNaoExistente");
		Connection con = ds.getConnection();

		PreparedStatement ps = con.prepareStatement("SELECT * FROM Produto");
		ResultSet rs = ps.executeQuery();
		while (rs.next()) System.out.println(rs.getInt(1));
		con.close();
		System.out.println(con.isClosed());
	}

}

