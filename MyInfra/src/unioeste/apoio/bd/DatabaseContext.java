package unioeste.apoio.bd;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.util.Properties;

public class DatabaseContext extends InitialContext {

	public DatabaseContext() throws NamingException {
	}

	@Override
	public Object lookup(String name) throws NamingException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			DataSource ds = new LocalDataSource("jdbc:mysql://localhost/faturamento", "root", "root");

			Properties prop = new Properties();
			prop.put("jdbc/MySqlNaoExistente", ds);

			Object value = prop.get(name);
			return (value != null) ? value : super.lookup(name);
		} catch(Exception e) {
			System.err.println("Lookup Problem " + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

}

