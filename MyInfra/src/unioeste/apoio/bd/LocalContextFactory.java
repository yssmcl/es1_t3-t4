package unioeste.apoio.bd;

import javax.naming.spi.NamingManager;

public class LocalContextFactory {

	private LocalContextFactory() {
	}

	public static LocalContext createLocalContext(String databaseDriver) throws Exception {
		try {
			LocalContext ctx = new LocalContext();
			Class.forName(databaseDriver);
			NamingManager.setInitialContextFactoryBuilder(ctx);
			return ctx;
		} catch(Exception e) {
			throw new Exception("Error Initializing Context: " + e.getMessage(),e);
		}
	}

}