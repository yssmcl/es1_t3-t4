<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="unioeste.geral.bo.pessoa.Fornecedor" %>
<%@ page import="unioeste.geral.bo.produto.TipoProduto" %>
<%@ page import="unioeste.geral.pessoa.manager.UCManterFornecedorBean" %>
<%@ page import="unioeste.geral.produto.manager.TipoProdutoControle" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" language="java" %>
<!--suppress HtmlFormInputWithoutLabel -->
<!DOCTYPE html>
<html class="no-js" lang="en" dir="ltr">

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Sistema de Controle de Estoque</title>
		<link rel="stylesheet" href="css/foundation.css">
		<link rel="stylesheet" href="css/app.css">
		<script src="js/vendor/jquery.js"></script>
	</head>

	<body>
		<jsp:include page="/biblioteca/menu.jsp"/><br>

		<div class="row medium-8 columns borda">
			<div class="blog-post">

				<h4 class="titulo">Cadastro de Produto</h4>

				<form action="CadastroProdutoServlet" method="post" autocomplete="on">
					<label class="titulo">Código de barras</label>
					<input name="codigoDeBarras" type="number" pattern="[0-9]+" required="required" />

					<label class="titulo">Nome do produto</label>
					<input name="nomeProduto" type="text" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+" required="required" />

					<div class="row">
						<div class="medium-6 columns">
							<label class="titulo">Tipo de produto</label>
							<select name="tipoProduto">
								<%
								List<TipoProduto> listaTipoProduto = null;
								try {
									listaTipoProduto = new TipoProdutoControle().recuperarTodosTipoProduto();
								} catch (Exception e) {
									e.printStackTrace();
								}
								for (TipoProduto obj : listaTipoProduto) {%>
									<option value="<%= obj.getIdTipoProduto() %>">
                    <%= obj.getNomeTipoProduto() %>
                  </option>
								<%}%>
							</select>
						</div>
						<div class="medium-6 columns">
							<label class="titulo">Fornecedor principal</label>
							<select name="fornecedor">
								<%
									List<Fornecedor> listaFornecedor = null;
									try {
										listaFornecedor = new UCManterFornecedorBean().obterTodosFornecedores();
									} catch (Exception e) {
										e.printStackTrace();
									}
									for (Fornecedor obj : listaFornecedor) {%>
                    <option value="<%= obj.getIdFornecedor() %>">
                      <%= obj.getNomeFantasia() %>
                    </option>
									<%}
								%>
							</select>
						</div>
					</div>

					<div class="row">
						<div class="medium-6 columns">
							<label class="titulo">Preço de custo atual</label>
							<div class="input-group">
								<span class="input-group-label">$</span>
								<input class="input-group-field" name="precoCusto" type="number" min="0" step="any" required="required" />
							</div>
						</div>

						<div class="medium-6 columns">
							<label class="titulo">Preço de venda</label>
							<div class="input-group">
								<span class="input-group-label">$</span>
								<input class="input-group-field" name="precoVenda" type="number" min="0" step="any" required="required" />
							</div>
						</div>
					</div>

					<div class="row">
						<div class="large-6 medium-6 columns">
							<label class="titulo">Quantidade atual em estoque</label>
							<input name="qtd" type="number" min="0" required="required" />
						</div>

						<%--<div class="large-6 medium-6 columns">--%>
							<%--<label class="titulo">Imagem do produto</label>--%>
							<%--<img src="http://www.flavorsys.com.br/img/Produtos.png" /><br>--%>
							<%--<label for="exampleFileUpload" class="button">Enviar arquivo</label>--%>
							<%--<input type="file" id="exampleFileUpload" class="show-for-sr">--%>
							<%--</div>--%>
					</div>

					<input class="button" type="submit" value="Cadastrar"/>
					<a href="index.jsp" class="secondary button">Voltar</a><br>
				</form>

			</div>
		</div>

		<script src="js/vendor/what-input.js"></script>
		<script src="js/vendor/foundation.js"></script>
		<script src="js/app.js"></script>
	</body>

</html>
