<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<!--suppress HtmlFormInputWithoutLabel -->
<html class="no-js" lang="en" dir="ltr">

  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistema de Controle de Estoque</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <script src="js/vendor/jquery.js"></script>
  </head>

  <body>
    <jsp:include page="/biblioteca/menu.jsp"/><br>

    <div class="row columns borda">
      <div class="blog-post">

        <h4 class="titulo">Exclusão de Fornecedor</h4>

        <div class="row">
          <div class="large-2 medium-2 columns">
            <label>
              Busca por:
              <select id="tipoBusca">
                <option value="buscaPorID">ID</option>
                <option value="buscaPorCNPJ">CNPJ</option>
              </select>
            </label>
          </div>
        </div>

        <script>
          $(document).ready(function () {
            $('.group').hide();
            $('#buscaPorID').show();
            $('#tipoBusca').change(function () {
              $('.group').hide();
              $('#'+$(this).val()).show();
            })
          });
        </script>

        <form action="ExcluiPesquisaFornecedorServlet" method="post" autocomplete="on">
          <div id="buscaPorID" class="group">
            <div class="large-2 medium-2">
              <label>
                <strong>ID</strong>
                <input name="id" type="number" min="1"/>
                <p class="help-text">Número >= 1</p>
              </label>
              <input class="button" type="submit" value="Pesquisar"/><br>
            </div>
          </div>

          <div id="buscaPorCNPJ" class="group">
            <div class="large-2 medium-2">
              <label>
                <strong>CNPJ</strong>
                <input name="cnpj" type="number" />
              </label>
              <input class="button" type="submit" value="Pesquisar"/><br>
            </div>
          </div>
        </form>

      </div>
    </div>

    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>

</html>

