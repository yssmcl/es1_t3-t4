<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Sistema de Controle de Estoque</title>
  <link rel="stylesheet" href="css/foundation.css">
  <link rel="stylesheet" href="css/app.css">
  <script src="js/vendor/jquery.js"></script>
</head>

<body>
<jsp:include page="ExibeFornecedorExcluir.jsp"/>

<div class="row columns borda">
  <div class="blog-post">

    <div class="reveal" id="excluir" data-reveal>
      <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
      </button>
      <div class="center">
        Deseja realmente excluir?<br><br>
        <form action="ExcluiFornecedorServlet" method="post">
          <input type="hidden" name="uuid" value="${uuid}" />
          <input class="button" type="submit" value="Sim"/>
          <a href="#" class="secondary button" data-close>Não</a>
        </form>
      </div>
    </div>

  </div>
</div>

<script src="js/vendor/what-input.js"></script>
<script src="js/vendor/foundation.js"></script>
<script src="js/app.js"></script>
</body>

</html>

