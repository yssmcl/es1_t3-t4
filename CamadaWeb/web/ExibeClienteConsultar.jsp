<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%--@elvariable id="atributos" type="java.util.List"--%>
<!DOCTYPE html>
<html>

  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistema de Controle de Estoque</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <script src="js/vendor/jquery.js"></script>
  </head>

  <body>
    <jsp:include page="ConsultaCliente.jsp"/>

    <br>
    <div class="row columns borda">
      <div class="blog-post">

        <h4 class="titulo">Dados do Cliente</h4>

        <c:choose>
          <c:when test="${atributos.get(3) != 'M'}">
            <strong>ID: </strong> <c:out value="${atributos.get(0)}"/> <br>
            <strong>Nome fantasia: </strong> <c:out value="${atributos.get(1)}"/> <br>
            <strong>Razão social: </strong> <c:out value="${atributos.get(2)}"/> <br>
            <strong>CNPJ: </strong> <c:out value="${atributos.get(3)}"/> <br>
            <strong>Nro. endereço: </strong> <c:out value="${atributos.get(4)}"/> <br>
            <strong>Complemento: </strong> <c:out value="${atributos.get(5)}"/> <br>
            <strong>CEP: </strong> <c:out value="${atributos.get(6)}"/> <br>
          </c:when>
          <c:when test="${atributos.get(3) == 'M'}">
            <strong>ID: </strong> <c:out value="${atributos.get(0)}"/> <br>
            <strong>Nome completo: </strong> <c:out value="${atributos.get(1)}"/> <br>
            <strong>CPF: </strong> <c:out value="${atributos.get(2)}"/> <br>
            <strong>Sexo: </strong> <c:out value="${atributos.get(3)}"/> <br>
            <strong>Nro. endereço: </strong> <c:out value="${atributos.get(4)}"/> <br>
            <strong>Complemento: </strong> <c:out value="${atributos.get(5)}"/> <br>
            <strong>CEP: </strong> <c:out value="${atributos.get(6)}"/> <br>
          </c:when>
        </c:choose>
        <br>

      </div>
    </div>

    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>

</html>