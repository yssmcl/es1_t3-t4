<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="unioeste.geral.bo.pessoa.Fornecedor" %>
<%@ page import="unioeste.geral.bo.produto.TipoProduto" %>
<%@ page import="unioeste.geral.pessoa.manager.UCManterFornecedorBean" %>
<%@ page import="unioeste.geral.produto.manager.TipoProdutoControle" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" language="java" %>
<%--@elvariable id="atributos" type="java.util.List"--%>
<%--@elvariable id="produto" type="unioeste.geral.bo.produto.Produto"--%>
<!--suppress HtmlFormInputWithoutLabel -->
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Sistema de Controle de Estoque</title>
		<link rel="stylesheet" href="css/foundation.css">
		<link rel="stylesheet" href="css/app.css">
		<script src="js/vendor/jquery.js"></script>
	</head>

	<body>
		<jsp:include page="AlteraPesquisaProduto.jsp"/>
    <jsp:useBean id="produtoBean" class="unioeste.geral.bo.produto.Produto" scope="session"/>

    <c:set var="produtoBean" value="${produto}" scope="session"/>

		<br>
		<div class="row columns borda">
			<div class="blog-post">

				<h4 class="titulo">Alteração de Produto</h4>

				<form action="AlteraProdutoServlet" method="post" autocomplete="on">
          <label class="titulo">ID</label>
          <input name="id" type="number" readonly="readonly"
                 value="${produto.idProduto}" />

					<label class="titulo">Código de barras</label>
					<input name="codigoDeBarras" type="number" pattern="[0-9]+" required="required"
                 value="${produto.codigoBarrasProduto}" />

					<label class="titulo">Nome do produto</label>
					<input name="nomeProduto" type="text" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ0-9 ]+" required="required"
                 value="${produto.nomeProduto}" />

					<div class="row">
						<div class="medium-6 columns">
							<label class="titulo">Tipo de produto</label>
							<select name="tipoProduto">
								<%
                  List<TipoProduto> listaTipoProduto = null;
                  try {
                    listaTipoProduto = new TipoProdutoControle().recuperarTodosTipoProduto();
                  } catch (Exception e) { e.printStackTrace(); }
                  for (TipoProduto obj : listaTipoProduto) {%>
                      <option><% out.print(obj.getNomeTipoProduto()); %></option>
                  <%}
                %>
                <%--<c:forEach var="produtoBean" items="${listaTipoProduto}">--%>
                  <%--<option value="${item.k}" ${item.key == selectedDept ? 'selected="selected"' : ''}>${item.value}</option>--%>
                <%--</c:forEach>--%>
							</select>
						</div>
						<div class="medium-6 columns">
							<label class="titulo">Fornecedor principal</label>
							<select name="fornecedor">
								<%
									List<Fornecedor> listaFornecedor = null;
									try {
										listaFornecedor = new UCManterFornecedorBean().obterTodosFornecedores();
									} catch (Exception e) { e.printStackTrace(); }
									for (Fornecedor obj : listaFornecedor) {%>
										<option><% out.print(obj.getNomeFantasia()); %></option>
									<%}
								%>
							</select>
						</div>
					</div>

					<div class="row">
						<div class="large-6 medium-6 columns">
							<label class="titulo">Preço de custo atual</label>
							<input name="precoCusto" type="number" min="0" step="any" required="required"
                     value="${produto.precoCustoProduto}" />
						</div>

						<div class="large-6 medium-6 columns">
							<label class="titulo">Preço de venda</label>
							<input name="precoVenda"  type="number" min="0" step="any" required="required"
                     value="${produto.precoVendaProduto}" />
						</div>
					</div>

					<div class="row">
						<div class="large-6 medium-6 columns">
							<label class="titulo">Quantidade atual em estoque</label>
							<input name="qtd" type="number" min="0" required="required"
                     value="${produto.qtdProduto}" />
						</div>
					</div>

					<input class="button" type="submit" value="Salvar"/>
					<a href="index.jsp" class="secondary button">Cancelar</a><br>
				</form>

			</div>
		</div>

		<script src="js/vendor/what-input.js"></script>
		<script src="js/vendor/foundation.js"></script>
		<script src="js/app.js"></script>
	</body>

</html>
