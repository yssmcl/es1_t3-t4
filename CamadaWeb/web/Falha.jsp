<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Serviços de Endereço</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <script src="js/vendor/jquery.js"></script>
  </head>

  <body>
    <jsp:useBean id="e" class="java.lang.Exception" scope="request"/>
    <jsp:useBean id="pagina" class="java.lang.String" scope="request"/>
    <jsp:include page="${pagina}"/>

    <script type="text/javascript">
      $(document).ready(function() {
        $('#modal').foundation('open')
      });
    </script>
    <div class="reveal" id="modal" data-reveal>
      <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
      </button>
      <div class="center">
        ${e.message}<br><br>
        <a href="${pagina}" class="button" data-close>OK</a>
      </div>
    </div>

    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>

</html>
