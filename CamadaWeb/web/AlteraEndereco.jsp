<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
            <meta charset="utf-8">
            <meta http-equiv="x-ua-compatible" content="ie=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Serviços de Endereço</title>
            <link rel="stylesheet" href="css/foundation.css">
            <link rel="stylesheet" href="css/app.css">
            <script src="js/vendor/jquery.js"></script>
    </head>

    <body onload= hide(0,Altera),hide(1,busca),preenchePais(comboBoxPais,idPais);>

	
	
        <jsp:include page="/biblioteca/menu.jsp"/><br>
            <h2	class="TituloPagina center">Alterações</h2>			
            <div class="row medium-8 large-7 columns borda" id=busca>
                <div class="blog-post">
                    <h3 class="TituloPagina center">Busca de endereço</h3>

                    <form action="BuscaEndereco" method="post" autocomplete="on">
                        <%-- TODO: combobox pra escolher o tipo de logradouro? Campo de sigla pra país e UF? --%>
                        <label class="textoTitulo">Nome do País e id:</label>
                        <div class ="row">
                            <div class="large-6 medium-6 columns">
                                <select id=comboBoxPais onclick="acorrentar(comboBoxPais,idPais),preencheUF(comboBoxUF,idUF,idPais);"></select>
                            </div>    
                            <div class="group large-6 medium-6 columns">
                                <select id="idPais"  disabled="disabled" ></select>
                            </div>
                        </div>

                       <label class="textoTitulo">Unidade Federativa:</label>
                       <div class ="row">
                            <div class="large-6 medium-6 columns">
                                <select id=comboBoxUF onclick="acorrentar(comboBoxUF,idUF),preencheCidade(comboBoxCidade,idCidade,idUF);"></select>
                            </div>    
                            <div class="group large-6 medium-6 columns">
                                <select id="idUF" disabled="disabled" ></select>
                            </div>
                        </div>

                       <label class="textoTitulo">Cidade:</label>
                       <div class ="row">
                            <div class="large-6 medium-6 columns">
                                <select id=comboBoxCidade onclick="acorrentar(comboBoxCidade,idCidade),preencheBairro(comboBoxBairro,idBairro);"></select>
                            </div>    
                            <div  class="group large-6 medium-6 columns">
                                <select id="idCidade" onclick="acorrentar(comboBoxCidade,idCidade);" disabled="disabled"></select>
                            </div>
                        </div>

                       <label class="textoTitulo">Bairro:</label>
                       <div class ="row">
                            <div class="large-6 medium-6 columns">
                                <select id=comboBoxBairro onclick="acorrentar(comboBoxBairro,idBairro),preencheTipoLogradouro(comboBoxTipoLogradoro,idTipoLogradouro);"></select>
                            </div>    
                            <div class="group large-6 medium-6 columns">
                                <select id="idBairro" disabled="disabled"></select>
                            </div>
                        </div>

                       <label class="textoTitulo">Tipo do logradouro:</label>
                       <div class ="row">
                            <div class="large-6 medium-6 columns">
                                <select id=comboBoxTipoLogradoro onclick="acorrentar(comboBoxTipoLogradoro,idTipoLogradouro),preencheLogradouro(comboBoxLogradouro,idLogradouro,idTipoLogradouro);"></select>
                            </div>    
                            <div  class="group large-6 medium-6 columns">
                                <select id="idTipoLogradouro" disabled="disabled"></select>
                            </div>
                        </div>

                        <label class="textoTitulo">Logradouro:</label>
                        <div class ="row">
                            <div class="large-6 medium-6 columns">
                                <select id=comboBoxLogradouro onclick="acorrentar(comboBoxLogradouro,idLogradouro),preencheCEP(comboBoxCEP,idCEP,idUF, idCidade, idBairro, idLogradouro);"></select>
                            </div>    
                            <div class="group large-6 medium-6 columns">
                                <select id="idLogradouro" disabled="disabled"></select>
                            </div>
                        </div>

                       <label class="textoTitulo">cep:</label>
                       <div class ="row">
                            <div class="large-6 medium-6 columns">
                                <select id=comboBoxCEP onclick="acorrentar(comboBoxCEP,idCEP);"></select>
                            </div>     
                            <div  class="group large-6 medium-6 columns">
                                <select id="idCEP" disabled="disabled"></select>
                            </div>
                        </div>
                            
                         <a class="button" onclick=hide(1,Altera),hide(0,busca),preencheParaAlterar();>selecionar</a><br><br>
                        
                    </form>
                </div>
            </div>
            <br>

        <div class="row medium-8 large-7 columns borda" id="Altera">

            <div class="blog-post">
                <h3 class="TituloPagina center">Alteração de endereço</h3>

                <form id="alterar" autocomplete="on">
                     <%-- TODO: combobox pra escolher o tipo de logradouro? Campo de sigla pra país e UF? --%>
                    
                    <label class="ComboBox">País:</label>
                    <div class ="row">
                        <div class="large-6 medium-6 columns">
                            <input name="PaisNome" id="nomePais" type="text"  pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
                        </div>    
                        <div  class="group large-6 medium-6 columns">
                            <input name="PaisId" id="iDdoPais" disabled="disabled"/>
                        </div>
                    </div>               
                    
                    <label class="textoTitulo">Unidade Federativa:</label>
                    <div class ="row">
                        <div class="large-6 medium-6 columns">
                            <input name="UFNome" id="nomeUF" type="text"  pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
                        </div>    
                        <div  class="group large-6 medium-6 columns">
                            <input name="UFId" id="iDdoUF" disabled="disabled"/>
                        </div>
                    </div>               
                    
                    
                    <label class="textoTitulo">Cidade:</label>
                    <div class ="row">
                        <div class="large-6 medium-6 columns">
                            <input name="CidadeNome" id="nomeCidade" type="text"  pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
                        </div>    
                        <div  class="group large-6 medium-6 columns">
                            <input name="CidadeId" id="iDdaCidade" disabled="disabled"/>
                        </div>
                    </div>  
                                    
                    
                    <label class="textoTitulo">Bairro:</label>
                    <div class ="row">
                        <div class="large-6 medium-6 columns">
                            <input name="BairroNome" id="nomeBairro" type="text"  pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
                        </div>    
                        <div  class="group large-6 medium-6 columns">
                            <input name="BairroId" id="iDdoBairro" disabled="disabled"/>
                        </div>
                    </div>  
                    
                    
                    <label class="textoTitulo">Tipo do logradouro:</label>
                    <div class ="row">
                        <div class="large-6 medium-6 columns">
                            <input name="TipoLogradouroNome" id="nomeTipoLogradouro" type="text" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
                        </div>    
                        <div  class="group large-6 medium-6 columns">
                            <input name="TipoLogradouroId" id="iDdoTipoLogradouro" disabled="disabled"/>
                        </div>
                    </div>  
                    
                    <label class="textoTitulo">Logradouro:</label>
                    <div class ="row">
                        <div class="large-6 medium-6 columns">
                            <input name="LogradouroNome" id="nomeLogradouro" type="text" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
                        </div>    
                        <div  class="group large-6 medium-6 columns">
                            <input name="LogradouroId" id="iDdoLogradouro" disabled="disabled"/>
                        </div>
                    </div>  
                    
                    
                    <label class="textoTitulo">Cep:</label>
                    <input name="cep" id="numeroDoCEP" type="text"  pattern="[0-9]{5}[\-]?[0-9]{3}"/>
                     
                     
                    <a href="Sucesso.jsp" class="button" onclick=hide(0,Altera),hide(1,busca),mandaAlterar();>Alterar</a><br><br>
                </form>		
				
            </div>

        </div>
		

        <script>
           
        function limpaCombo(elemento1,elementoID){
            comboboxNome = document.getElementById(elemento1.id);
            comboboxId = document.getElementById(elementoID.id);            
            while(comboboxNome.options.length>0){
                comboboxNome.remove(0);
                comboboxId.remove(0);
            }
        }        

        function acorrentar(comboBoxNome,comboBoxId){
           elemento = document.getElementById(comboBoxNome.id);
           acorrentado = document.getElementById(comboBoxId.id);
           acorrentado.value = elemento.value;
        }

        function hide(elemento,objeto){
                if(elemento!=1) document.getElementById(objeto.id).style.display="none";
                else document.getElementById(objeto.id).style.display="block";
        }

        function preenchePais(elemento,elemento2){            
            limpaCombo(comboBoxUF,idUF);
            limpaCombo(comboBoxCidade,idCidade);
            limpaCombo(comboBoxBairro,idBairro);
            limpaCombo(comboBoxTipoLogradoro,idTipoLogradouro);
            limpaCombo(comboBoxLogradouro,idLogradouro);
            limpaCombo(comboBoxCEP,idCEP);
            
            var pais = document.getElementById(elemento.id);
            var id = document.getElementById(elemento2.id);
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function(){
                if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
                    var valores = JSON.parse(xmlhttp.responseText);
                    var i = 0;
                  
                    var contador=1;
                    for(;i<valores.length;i+=2){
                        var option = document.createElement("option");
                        var option2 = document.createElement("option");
                        option.text = valores[i].atributo;
                        option.value = contador;
                        pais.add(option);
                        option2.text = valores[i+1].id;
                        option2.value = contador;
                        option2.id=valores[i+1].id;
                        id.add(option2);
                        contador++;
                    }
                }
            };
            xmlhttp.open("GET","/CamadaWeb/servlet/AlteraEnderecoServlet");
            xmlhttp.send();
        }

        function preencheUF(elemento,elemento2,idPais){
            var xmlhttp = new XMLHttpRequest();
            var comboBoxNome = document.getElementById(elemento.id);
            var idBoxId = document.getElementById(elemento2.id);
            limpaCombo(elemento,elemento2);
            xmlhttp.onreadystatechange = function(){
                if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
                    var valores = JSON.parse(xmlhttp.responseText);
                    var i = 0;
                    var contador=1;
                    for(;i<valores.length;i+=3){
                        var option = document.createElement("option");
                        var option2 = document.createElement("option");
                        option.text = valores[i].atributo;
                        option.value = contador;
                        comboBoxNome.add(option);
                        option2.text = valores[i+1].id;
                        option2.value = contador;
                        option2.id= valores[i+2].idPai;
                        idBoxId.add(option2);
                        contador++;
                    }
                }
            };
            var idDoPais = document.getElementById(idPais.id);
            var valorId= idDoPais.options[idDoPais.selectedIndex].value;
            xmlhttp.open("GET","/CamadaWeb/servlet/AlteraEnderecoServletUF?pais="+valorId);
            xmlhttp.send();
        }

        function preencheCidade(elemento,elemento2,idPai){
            var xmlhttp = new XMLHttpRequest();
            limpaCombo(elemento,elemento2);
            xmlhttp.onreadystatechange = function(){
                if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
                    var valores = JSON.parse(xmlhttp.responseText);
                    var i = 0;
                    var comboBoxNome = document.getElementById(elemento.id);
                    var idBoxId = document.getElementById(elemento2.id);
                    var contador=1;
                    for(;i<valores.length;i+=3){
                        var option = document.createElement("option");
                        var option2 = document.createElement("option");
                        option.text = valores[i].atributo;
                        option.value = contador;
                        comboBoxNome.add(option);
                        option2.text = valores[i+1].id;
                        option2.value = contador;
                        option2.id= valores[i+2].idPai;
                        idBoxId.add(option2);
                        contador++;
                    }
                }
            };
            var idDoPai = document.getElementById(idPai.id);
            var valorId= idDoPai.options[idDoPai.selectedIndex].text;
            xmlhttp.open("GET","/CamadaWeb/servlet/AlteraEnderecoServletCidade?UF="+valorId);
            xmlhttp.send();
        }
        
        function preencheBairro(elemento,elemento2){
            limpaCombo(elemento,elemento2);
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function(){
                if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
                    var valores = JSON.parse(xmlhttp.responseText);
                    var i = 0;
                    var bairro = document.getElementById(elemento.id);
                    var id = document.getElementById(elemento2.id);
                    var contador=1;
                    for(;i<valores.length;i+=2){
                        var option = document.createElement("option");
                        var option2 = document.createElement("option");
                        option.text = valores[i].atributo;
                        option.value = contador;
                        bairro.add(option);
                        option2.text = valores[i+1].id;
                        option2.value = contador;
                        option2.id=valores[i+1].id;
                        id.add(option2);
                        contador++;
                    }
                }
            };
            xmlhttp.open("GET","/CamadaWeb/servlet/AlteraEnderecoServletBairro");
            xmlhttp.send();
        }
        
        function preencheTipoLogradouro(elemento,elemento2){
            limpaCombo(elemento,elemento2);
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function(){
                if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
                    var valores = JSON.parse(xmlhttp.responseText);
                    var i = 0;
                    var bairro = document.getElementById(elemento.id);
                    var id = document.getElementById(elemento2.id);
                    var contador=1;
                    for(;i<valores.length;i+=2){
                        var option = document.createElement("option");
                        var option2 = document.createElement("option");
                        option.text = valores[i].atributo;
                        option.value = contador;
                        bairro.add(option);
                        option2.text = valores[i+1].id;
                        option2.value = contador;
                        option2.id=valores[i+1].id;
                        id.add(option2);
                        contador++;
                    }
                }
            };
            xmlhttp.open("GET","/CamadaWeb/servlet/AlteraEnderecoServletTipoLogradoro");
            xmlhttp.send();
        }
        
        function preencheLogradouro(elemento,elemento2,idPai){
            limpaCombo(elemento,elemento2);
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function(){
                if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
                    var valores = JSON.parse(xmlhttp.responseText);
                    var i = 0;
                    var comboBoxNome = document.getElementById(elemento.id);
                    var idBoxId = document.getElementById(elemento2.id);
                    var contador=1;
                    for(;i<valores.length;i+=3){
                        var option = document.createElement("option");
                        var option2 = document.createElement("option");
                        option.text = valores[i].atributo;
                        option.value = contador;
                        comboBoxNome.add(option);
                        option2.text = valores[i+1].id;
                        option2.value = contador;
                        option2.id= valores[i+2].idPai;
                        idBoxId.add(option2);
                        contador++;
                    }
                }
            };
            var idDoPais = document.getElementById(idPai.id);
            var valorId= idDoPais.options[idDoPais.selectedIndex].value;
            xmlhttp.open("GET","/CamadaWeb/servlet/AlteraEnderecoServletLogradouro?IdTipo="+valorId);
            xmlhttp.send();
        }
        
        function preencheCEP(elemento, elemento2, idUF, idCidade, idBairro, idLogradouro){
            limpaCombo(elemento,elemento2);
            
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function(){
                if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
                    var valores = JSON.parse(xmlhttp.responseText);
                    var i = 0;
                    var comboBoxNome = document.getElementById(elemento.id);
                    var idBoxId = document.getElementById(elemento2.id);
                    
                    var contador=1;
                    for(;i<valores.length;i+=3){
                        var option = document.createElement("option");
                        option.text = valores[i+1].cep;
                        option.value = contador;
                        comboBoxNome.add(option);
                        var option2 = document.createElement("option");
                        option2.text = valores[i].idEndereco;
                        option2.id= contador;
                        idBoxId.add(option2);
                    }
                }
            };
            var uF = document.getElementById(idUF.id);
            var cidade = document.getElementById(idCidade.id);
            var bairro = document.getElementById(idBairro.id);
            var logradouro = document.getElementById(idLogradouro.id);
            
            var idLogradouro = logradouro.options[logradouro.selectedIndex].text;
            var idBairro= bairro.options[bairro.selectedIndex].text;
            var idCidade= cidade.options[cidade.selectedIndex].text;
            var idUF= uF.options[uF.selectedIndex].text;
            
            xmlhttp.open("GET","/CamadaWeb/servlet/AlteraEnderecoServletCEP?idLogradouro="+idLogradouro+"&idBairro="+idBairro+"&idCidade="+idCidade+"&idUF="+idUF);
            xmlhttp.send();
        }
        
        function preencheApartirdoCEP(elemento, elemento2){
            limpaCombo(comboBoxPais,idPais);
            limpaCombo(comboBoxUF,idUF);
            limpaCombo(comboBoxCidade,idCidade);
            limpaCombo(comboBoxBairro,idBairro);
            limpaCombo(comboBoxTipoLogradoro,idTipoLogradouro);
            limpaCombo(comboBoxLogradouro,idLogradouro);
            limpaCombo(comboBoxCEP,idCEP);
            
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function(){
                if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
                    var valores = JSON.parse(xmlhttp.responseText);
                    var i = 0;
                    var comboBoxNome = document.getElementById(elemento.id);
                    var idBoxId = document.getElementById(elemento2.id);
                    
                    var contador=1;
                    for(;i<valores.length;i+=6){
                        var option = document.createElement("option");
                        option.text = valores[i+1].cep;
                        option.value = contador;
                        comboBoxNome.add(option);
                        var option2 = document.createElement("option");
                        option2.text = valores[i].idEndereco;
                        option2.id= contador;
                        idBoxId.add(option2);
                        
                    }
                }
            };
            
            xmlhttp.open("GET","/CamadaWeb/servlet/AlteraEnderecoServletCEPBusca");
            xmlhttp.send();
        }
        
        function preencheParaAlterar(){
            // variaveis do segundo Form
            var textoPais = document.getElementById("nomePais");
            var textoUF = document.getElementById("nomeUF");
            var textoCidade= document.getElementById("nomeCidade");
            var textoBairro= document.getElementById("nomeBairro");
            var textoTipoLogradouro = document.getElementById("nomeTipoLogradouro");
            var textoLogradouro = document.getElementById("nomeLogradouro");
            var textoCep = document.getElementById("numeroDoCEP");
            
            var idPais=document.getElementById("iDdoPais");
            var idUF = document.getElementById("iDdoUF");
            var idCidade= document.getElementById("iDdaCidade");
            var idBairro= document.getElementById("iDdoBairro");
            var idTipoLogradouro = document.getElementById("iDdoTipoLogradouro");
            var idLogradouro = document.getElementById("iDdoLogradouro");
            //var idCep = document.getElementById("numeroDoCEP");
            
            // variaveis do primeiro Form
            var nomePais = document.getElementById("comboBoxPais");
            var nomeUF = document.getElementById("comboBoxUF");
            var nomeCidade = document.getElementById("comboBoxCidade");
            var nomeBairro = document.getElementById("comboBoxBairro");
            var nomeTipoLogradouro = document.getElementById("comboBoxTipoLogradoro");
            var nomeLogradouro = document.getElementById("comboBoxLogradouro");
            var nomeCEP = document.getElementById("comboBoxCEP");
            
            var idDOPais = document.getElementById("idPais");
            var idDOUF = document.getElementById("idUF");
            var idDACidade = document.getElementById("idCidade");
            var idDOBairro = document.getElementById("idBairro");
            var idDOTipoLogradouro = document.getElementById("idTipoLogradouro");
            var idDOLogradouro = document.getElementById("idLogradouro");
            var idDOCEP = document.getElementById("idCEP");
           
            var valorIdPais= idDOPais.options[idDOPais.selectedIndex].value;
            var valorIdUF= idDOUF.options[idDOUF.selectedIndex].value;
            var valorIdCidade= idDACidade.options[idDACidade.selectedIndex].value;
            var valorIdBairro= idDOBairro.options[idDOBairro.selectedIndex].value;
            var valorIdTipoLogrador= idDOTipoLogradouro.options[idDOTipoLogradouro.selectedIndex].value;
            var valorIdLogradouro= idDOLogradouro.options[idDOLogradouro.selectedIndex].value;
            var valorIdCEP= idDOCEP.options[idDOCEP.selectedIndex].value;
            //---------------------------------
            
            textoPais.value= nomePais.options[nomePais.selectedIndex].text;
            textoUF.value= nomeUF.options[nomeUF.selectedIndex].text;
            textoCidade.value= nomeCidade.options[nomeCidade.selectedIndex].text;
            textoBairro.value= nomeBairro.options[nomeBairro.selectedIndex].text;
            textoTipoLogradouro.value= nomeTipoLogradouro.options[nomeTipoLogradouro.selectedIndex].text;
            textoLogradouro.value= nomeLogradouro.options[nomeLogradouro.selectedIndex].text;
            textoCep.value= nomeCEP.options[nomeCEP.selectedIndex].text;
            
            idPais.value = idDOPais.options[idDOPais.selectedIndex].value;
            idUF.value = idDOUF.options[idDOUF.selectedIndex].value;
            idCidade.value = idDACidade.options[idDACidade.selectedIndex].value;
            idBairro.value = idDOBairro.options[idDOBairro.selectedIndex].value;
            idTipoLogradouro.value = idDOTipoLogradouro.options[idDOTipoLogradouro.selectedIndex].value;
            idLogradouro.value =idDOLogradouro.options[idDOLogradouro.selectedIndex].value;
            //idCep.value =idDOCEP.options[idDOCEP.selectedIndex].value;
        }
        
        function mandaAlterar(){
            var textoPais = document.getElementById("nomePais");
            var textoUF = document.getElementById("nomeUF");
            var textoCidade= document.getElementById("nomeCidade");
            var textoBairro= document.getElementById("nomeBairro");
            var textoTipoLogradouro = document.getElementById("nomeTipoLogradouro");
            var textoLogradouro = document.getElementById("nomeLogradouro");
            var textoCep = document.getElementById("numeroDoCEP");
            
            var idPais=document.getElementById("iDdoPais");
            var idUF = document.getElementById("iDdoUF");
            var idCidade= document.getElementById("iDdaCidade");
            var idBairro= document.getElementById("iDdoBairro");
            var idTipoLogradouro = document.getElementById("iDdoTipoLogradouro");
            var idLogradouro = document.getElementById("iDdoLogradouro");
            
            var xmlhttp = new XMLHttpRequest();
            
            xmlhttp.open("POST","/CamadaWeb/servlet/AlteraEnderecoServletAlterar",true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send("TipoLogradouroNome="+textoTipoLogradouro.value
                            +"&LogradouroNome="+textoLogradouro.value
                            +"&BairroNome="+textoBairro.value
                            +"&CidadeNome="+textoCidade.value
                            +"&UFNome="+textoUF.value
                            +"&PaisNome="+textoPais.value
                            +"&cep="+textoCep.value
                    // id >>>
                            +"&TipoLogradouroId="+idTipoLogradouro.value
                            +"&LogradouroId="+idLogradouro.value
                            +"&BairroId="+idBairro.value
                            +"&CidadeId="+idCidade.value
                            +"&UFId="+idUF.value
                            +"&PaisId="+idPais.value);
            
        }
        
        </script>
        <script src="js/vendor/what-input.js"></script>
        <script src="js/vendor/foundation.js"></script>
        <script src="js/app.js"></script>
    </body>
</html>