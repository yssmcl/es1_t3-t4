<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%--@elvariable id="atributos" type="java.util.List"--%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Sistema de Controle de Estoque</title>
		<link rel="stylesheet" href="css/foundation.css">
		<link rel="stylesheet" href="css/app.css">
		<script src="js/vendor/jquery.js"></script>
	</head>

	<body>
		<jsp:include page="/biblioteca/menu.jsp"/><br>

		<%--TODO: com isso não funciona--%>
		<%--<jsp:include page="ExcluiPesquisaNotaDeVenda.jsp"/>--%>

		<div class="row columns borda">
			<div class="blog-post">

				<h4 class="titulo">Resultados da Pesquisa </h4>

				<table style="width:100%">
					<thead>
						<tr>
							<th>Nro. da nota</th>
							<th>Data de emissão</th>
							<th>Total bruto</th>
							<th>Desconto total</th>
							<th>Valor líquido</th>
							<th>Cliente</th>
							<th>Itens da nota</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<c:forEach var="atributos" items="${atributos}">
								<td><c:out value="${atributos}"/></td>
							</c:forEach>
							<td>
								<div class="small button-group">
									<a data-open="excluir" class="alert button">Excluir</a>
								</div>
							</td>
						</tr>
					</tbody>
				</table>

				<div class="reveal" id="excluir" data-reveal>
					<button class="close-button" data-close aria-label="Close reveal" type="button">
						<span aria-hidden="true">&times;</span>
					</button>
					<div class="center">
						Deseja realmente excluir?<br><br>
						<form action="ExcluiNotaDeVendaServlet" method="post">
							<input type="hidden" name="uuid" value="${uuid}"/>
							<input class="button" type="submit" value="Sim"/>
							<a href="#" class="secondary button" data-close>Não</a>
						</form>
					</div>
				</div>

			</div>
		</div>

		<script src="js/vendor/what-input.js"></script>
		<script src="js/vendor/foundation.js"></script>
		<script src="js/app.js"></script>
	</body>

</html>
