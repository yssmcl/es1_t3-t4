<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="unioeste.geral.bo.nota.FormaPagamento" %>
<%@ page import="unioeste.geral.bo.pessoa.Fornecedor" %>
<%@ page import="unioeste.geral.bo.produto.Produto" %>
<%@ page import="unioeste.geral.nota.manager.FormaPagamentoControle" %>
<%@ page import="unioeste.geral.pessoa.manager.UCManterFornecedorBean" %>
<%@ page import="unioeste.geral.produto.manager.UCManterProdutoImpl" %>
<%@ page import="java.util.List" %>
<%@ page import="unioeste.geral.pessoa.manager.UCManterFornecedorBean" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<!--suppress HtmlFormInputWithoutLabel -->
<html class="no-js" lang="en" dir="ltr">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Sistema Mini</title>
    <%--TODO: baixar esses .js e .css--%>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<link rel="stylesheet" href="css/foundation.css">
		<link rel="stylesheet" href="css/app.css">
		<script src="js/vendor/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	</head>

	<body>
		<jsp:include page="/biblioteca/menu.jsp"/><br>

		<jsp:useBean id="produtoBean" class="unioeste.geral.bo.produto.Produto" scope="session" />

    <%
      List<Fornecedor> listaFornecedor = null;
      try {
        listaFornecedor = new UCManterFornecedorBean().obterTodosFornecedores();
      } catch (Exception e) {
        e.printStackTrace();
      }

      List<Produto> listaProduto = null;
      try {
        listaProduto = new UCManterProdutoImpl().obterTodosProdutos();
      } catch (Exception e) {
        e.printStackTrace();
      }
    %>

		<div class="row medium-10 columns borda">
			<div class="blog-post">

					<h3 class="titulo">Cadastro de Compra de Produto</h3>

          <form action="CadastroNotaDeCompraServlet" method="post" autocomplete="on">
						<div class="row">
							<div class="medium-6 columns">
								<label class="titulo">Data Nota:</label>
								<input name="dataNota" type="date" required="required" placeholder="DD/MM/AAAA" />
							</div>
							<div class="medium-5 columns">
								<label class="titulo">Fornecedor:</label>
                <div class="ui-widget">
                  <input id="nomeFornecedor" name="nomeFornecedor" type="text" onblur="preencherFornecedor()" required="required"
                         placeholder="Digite o nome do fornecedor..." />
                </div>
							</div>
              <div class="medium-1 columns">
                <label class="titulo">ID:</label>
                <input id="idFornecedor" name="idFornecedor" type="text" readonly="readonly" value="" />
              </div>
						</div>
						<br>
						<div id="container_produtos" class="row">
              <div>
                <label class="titulo medium-1 columns">Cod</label>
                <label class="titulo medium-5 columns">Produto</label>
								<label class="titulo medium-2 columns">Qtde</label>
								<label class="titulo medium-2 columns">Preço</label>
								<label class="titulo medium-2 columns">Total</label>
							</div>
              <div id="produto">
                <div class="medium-1 columns">
                  <input id="idProduto" name="idProduto" type="text" readonly="readonly" />
                </div>
                <div class="medium-5 columns">
                  <div class="ui-widget">
                    <input id="nomeProduto" name="nomeProduto" type="text" onblur="preencherProduto()" required="required"
                           placeholder="Digite o nome do produto..."/>
                  </div>
                </div>
                <div class="medium-2 columns">
                  <input id="qtdProduto" name="qtdProduto" type="number" value="0" onchange="calcularTotalProduto()" />
                </div>
                <div class="medium-2 columns">
                  <div class="input-group">
                    <span class="input-group-label">$</span>
                    <input id="precoProduto" class="input-group-field" type="number" step="any" readonly="readonly" />
                  </div>
                </div>
                <div class="medium-2 columns">
                  <div class="input-group">
                    <span class="input-group-label">$</span>
                    <input id="totalProduto" name="totalProduto" class="input-group-field" type="number" step="any"
                           readonly="readonly" value="0" />
                  </div>
                </div>
              </div>
						</div>
            <div class="center">
              <a id="adicionarProduto" class="button" onclick="addProduto()">+</a>
							<a class="alert button" onclick="removerProduto()">−</a>
						</div>
            <br>

            <div class="row">
							<div class="medium-2 columns">
								<label class="titulo">Total Nota:</label>
								<div class="input-group">
									<span class="input-group-label">$</span>
									<input class="input-group-field" id="totalNota" name="totalNota" type="number" min="0" step="any"
                         readonly="readonly" />
								</div>
							</div>
							<div class="medium-2 columns">
								<label class="titulo">Desconto Total:</label>
								<div class="input-group">
									<span class="input-group-label">$</span>
									<input class="input-group-field" id="descontoTotal" name="descontoTotal" type="number" min="0"
                         step="any" value=0 required="required" onchange="calcularValorLiquido()" />
								</div>
							</div>
							<div class="medium-2 columns float-left">
								<label class="titulo">Valor Líquido:</label>
								<div class="input-group">
									<span class="input-group-label">$</span>
									<input class="input-group-field" id="valorLiquido" name="valorLiquido" type="number" min="0" step="any"
                         readonly="readonly" />
								</div>
							</div>
						</div>
						<br>
						<input class="button" type="submit" value="Cadastrar"/>
						<a href="index.jsp" class="secondary button">Voltar</a><br>
					</form>

			</div>
		</div>

		<script>
      var arrayNomeFornecedor = [100];
      var arrayIdFornecedor = [100];
      $(function() {
        <%
          for(int i = 0; i < listaFornecedor.size(); i++) {%>
            arrayIdFornecedor[<%= i %>] = "<%= listaFornecedor.get(i).getIdFornecedor() %>";
            arrayNomeFornecedor[<%= i %>] = "<%= listaFornecedor.get(i).getNomeFantasia() %>";
          <%}
        %>
        $("#nomeFornecedor").autocomplete({
          source: arrayNomeFornecedor
        });
      });

      var arrayNomeProduto = [100];
      var arrayIdProduto = [100];
      var arrayPrecoProduto = [100];
      $(function() {
        <%
          for(int i = 0; i < listaProduto.size(); i++) {%>
            arrayNomeProduto[<%= i %>] = "<%= listaProduto.get(i).getNomeProduto() %>";
            arrayIdProduto[<%= i %>] = "<%= listaProduto.get(i).getIdProduto() %>";
            arrayPrecoProduto[<%= i %>] = "<%= listaProduto.get(i).getPrecoCustoProduto() %>";
          <%}
        %>
        $("#nomeProduto").autocomplete({
          source: arrayNomeProduto
        });
      });

      var num = -1;
      function addProduto() {
        var html = document.getElementById("produto").innerHTML;
        var div = document.createElement("div" + ++num);
        div.innerHTML = html;
        $("#container_produtos").append(div);
        $("#container_produtos").find('input[id=nomeProduto]:last').autocomplete({
          source: arrayNomeProduto
        });
      }

//      $("#adicionarProduto").on("click", function() {
//        var html = document.getElementById("produto").innerHTML;
//        var div = document.createElement("div" + ++num);
//        div.innerHTML = html;
//        $("#container_produtos").append(div);
//        $("#container_produtos").find('input[id=nomeProduto]:last').autocomplete({
//          source: arrayNomeProduto
//        });
//      });

      function removerProduto() {
        $("div" + num).remove();
        $("div" + num--).empty();
      }

      function preencherFornecedor() {
        for (var ii = 0; ii < <%= listaFornecedor.size() %>; ii++) {
          if (arrayNomeFornecedor[ii] === document.getElementById("nomeFornecedor").value) {
            document.getElementById("idFornecedor").value = arrayIdFornecedor[ii];
          }
        }
      }

      function preencherProduto() {
        for (var ii = 0; ii < <%= listaProduto.size() %>; ii++) {
          if (arrayNomeProduto[ii] === document.getElementById("nomeProduto").value) {
            document.getElementById("idProduto").value = arrayIdProduto[ii];
            document.getElementById("precoProduto").value = arrayPrecoProduto[ii];
          }
        }
        calcularTotalProduto();
      }

      function calcularTotalProduto() {
        for (var ii = 0; ii < <%= listaProduto.size() %>; ii++) {
          if (arrayIdProduto[ii] === document.getElementById("idProduto").value) {
            var varQtdProduto = document.getElementById("qtdProduto").value;
            document.getElementById("totalProduto").value = arrayPrecoProduto[ii] * varQtdProduto;
          }
        }
        calcularTotalNota();
        calcularValorLiquido();
      }

      function calcularTotalNota() {
        var varDescontoTotal = document.getElementById("descontoTotal").defaultValue = 0;
        var varTotalProduto = document.getElementById("totalProduto").value;
        var varTotalNota = varTotalProduto;
        document.getElementById("totalNota").value = varTotalProduto;
      }
      
      function calcularValorLiquido() {
        var varTotalNota = document.getElementById("totalNota").value;
        var varDescontoTotal = document.getElementById("descontoTotal").value;
        document.getElementById("valorLiquido").value = varTotalNota - varDescontoTotal;
      }
		</script>

		<script src="js/vendor/what-input.js"></script>
		<script src="js/vendor/foundation.js"></script>
		<script src="js/app.js"></script>

	</body>
</html>
