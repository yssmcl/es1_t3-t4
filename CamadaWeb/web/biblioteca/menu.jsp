<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>

	<div class="top-bar">
    <div class="top-bar-left">
      <ul class="dropdown menu" data-dropdown-menu>
        <li>
          <a href="index.jsp">Principal</a>
        </li>

        <li>
          <a href="#">Cliente</a>
          <ul class="menu vertical">
            <li><a href="CadastroCliente.jsp">Cadastrar</a></li>
            <li><a href="ConsultaCliente.jsp">Consultar</a></li>
            <li><a href="">Alterar</a></li>
            <li><a href="ExcluiPesquisaCliente.jsp">Excluir</a></li>
          </ul>
        </li>
        
        <li>
          <a href="#">Fornecedor</a>
          <ul class="menu vertical">
            <li><a href="CadastroFornecedor.jsp">Cadastrar</a></li>
            <li><a href="ConsultaFornecedor.jsp">Consultar</a></li>
            <li><a href="">Alterar</a></li>
            <li><a href="ExcluiPesquisaFornecedor.jsp">Excluir</a></li>
          </ul>
        </li>
        
        <li>
          <a href="#">Produto</a>
          <ul class="menu vertical">
            <li><a href="CadastroProduto.jsp">Cadastrar</a></li>
            <li><a href="ConsultaProduto.jsp">Consultar</a></li>
            <li><a href="AlteraPesquisaProduto.jsp">Alterar</a></li>
            <li><a href="ExcluiPesquisaProduto.jsp">Excluir</a></li>
          </ul>
        </li>

        <li>
          <a href="#">Compras</a>
          <ul class="menu vertical">
            <li><a href="CadastroNotaDeCompra.jsp">Cadastrar nota de compra</a></li>
            <li><a href="ConsultaNotaDeCompra.jsp">Consultar nota de compra</a></li>
            <li><a href="AlteraPesquisaNotaDeCompra.jsp">Alterar nota de compra</a></li>
            <li><a href="ExcluiPesquisaNotaDeCompra.jsp">Excluir nota de compra</a></li>
          </ul>
        </li>

        <li>
          <a href="#">Vendas</a>
          <ul class="menu vertical">
            <li><a href="CadastroNotaDeVenda.jsp">Cadastrar nota de venda</a></li>
            <li><a href="ConsultaNotaDeVenda.jsp">Consultar nota de venda</a></li>
            <li><a href="AlteraPesquisaNotaDeVenda.jsp">Alterar nota de venda</a></li>
            <li><a href="ExcluiPesquisaNotaDeVenda.jsp">Excluir nota de venda</a></li>
          </ul>
        </li>

      </ul>
    </div>
	</div>

</html>
