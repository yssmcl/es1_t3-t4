<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%--@elvariable id="atributos" type="java.util.List"--%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Sistema de Controle de Estoque</title>
		<link rel="stylesheet" href="css/foundation.css">
		<link rel="stylesheet" href="css/app.css">
		<script src="js/vendor/jquery.js"></script>
	</head>

	<body>
		<jsp:include page="ConsultaNotaDeCompra.jsp"/>

		<br>
		<div class="row columns borda">
			<div class="blog-post">

				<h4 class="titulo">Dados da Nota de Compra</h4>
				<strong>Nro. da nota: </strong> <c:out value="${atributos.get(0)}"/> <br>
				<strong>Data de emissão: </strong> <c:out value="${atributos.get(1)}"/> <br>
				<strong>Total bruto: </strong> <c:out value="${atributos.get(2)}"/> <br>
				<strong>Desconto total: </strong> <c:out value="${atributos.get(3)}"/> <br>
				<strong>Valor líquido: </strong> <c:out value="${atributos.get(4)}"/> <br>
				<strong>Fornecedor: </strong> <c:out value="${atributos.get(5)}"/> <br>
				<strong>Itens da nota: </strong> <c:out value="${atributos.get(6)}"/> <br>
				<br>

			</div>
		</div>

		<script src="js/vendor/what-input.js"></script>
		<script src="js/vendor/foundation.js"></script>
		<script src="js/app.js"></script>
	</body>

</html>
