<%@ page import="unioeste.geral.bo.endereco.*" %>
<%@ page import="unioeste.geral.endereco.manager.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<!--suppress HtmlFormInputWithoutLabel -->
<html class="no-js" lang="en" dir="ltr">

  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistema de Controle de Estoque</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/jquery.maskedinput.js"></script>
    <script src="js/validarcpf.js"></script>
    <script src="js/mascaras.js"></script>
  </head>

  <body>
    <jsp:include page="/biblioteca/menu.jsp"/><br>

    <%
      ArrayList<Endereco> listaEndereco = new ArrayList<Endereco>();
      try {
        listaEndereco = new EnderecoControle().obterListaEndereco();
      } catch (Exception e) {
        e.printStackTrace();
      }
      ArrayList<Pais> listaPais = new PaisControle().obterListaPais();
      ArrayList<UnidadeFederativa> listaUF = new UnidadeFederativaControle().obterListaUnidadeFederativa();
      ArrayList<Cidade> listaCidade = new CidadeControle().obterListaCidade();
      ArrayList<Bairro> listaBairro = new BairroControle().obterListaBairro();
      ArrayList<Logradouro> listaLogradouro = new LogradouroControle().obterListaLogradouro();
      ArrayList<TipoLogradouro> listaTipoLogradouro = new TipoLogradouroControle().obterListaTipoLogradouro();
    %>

    <div class="row medium-8 large-7 columns borda">
      <div class="blog-post">

        <h4 class="titulo">Cadastro de Cliente</h4>


        <form action="CadastroClienteServlet" method="post" autocomplete="on">
          <div class="row">
            <div class="large-4 medium-4 columns">
              <label class="titulo">Tipo de cadastro</label>
              <select id="tipoCadastro" name="tipoCadastro">
                <option value="pessoaFisica">Pessoa Física</option>
                <option value="pessoaJuridica">Pessoa Jurídica</option>
              </select>
            </div>
          </div>

          <!-- Campos de cadastro para pessoa física -->
          <div id="pessoaFisica" class="group">
            <div class="row">
              <div class="large-4 medium-4 columns">
                <label class="titulo">Primeiro nome</label>
                <input name="primeiroNome" type="text" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+" />
              </div>
              <div class="large-4 medium-4 columns">
                <label class="titulo">Nome do meio</label>
                <input name="nomeDoMeio" type="text" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+" />
              </div>
              <div class="large-4 medium-4 columns">
                <label class="titulo">Último nome</label>
                <input name="ultimoNome" type="text" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+" />
              </div>
            </div>
            <div class="row">
              <div class="large-4 medium-4 columns">
                <label class="titulo">CPF</label>
                <input id="cpf" name="cpf" type="text" pattern="[0-9]{3}[\.][0-9]{3}[\.][0-9]{3}[\-][0-9]{2}"/>
              </div>
            </div>
          </div>

          <!-- Campos de cadastro para pessoa jurídica -->
          <div id="pessoaJuridica" class="group">
            <div class="row">
              <div class="large-6 medium-6 columns">
                <label class="titulo">Razão Social</label>
                <input name="nomeRazaoSocial" type="text" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
              </div>
              <div class="large-6 medium-6 columns">
                <label class="titulo">Nome Fantasia</label>
                <input name="nomeFantasia" type="text" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+" />
              </div>
            </div>
            <div class="row">
              <div class="large-4 medium-4 columns">
                <label class="titulo">CNPJ</label>
                <input id="cnpj" name="cnpj" type="text" pattern="[0-9]{2}[\.][0-9]{3}[\.][0-9]{3}[\/][0-9]{4}[\-][0-9]{2}"/>
              </div>
            </div>
          </div>

          <!-- Campos de cadastro em comum para pessoa física e jurídica -->
          <div class="row">
            <div class="large-4 medium-4 columns">
              <label class="titulo">Telefone</label>
              <input id="telefone" name="telefone" type="text" />
            </div>
            <div class="medium-4 columns float-left">
              <label class="titulo">Sexo:</label>
              <input type="radio" name="sexo" required="required"
                     id="masculino" value="masculino">
              <label for="masculino">Masculino</label>
              <input type="radio" name="sexo" required="required"
                     id="feminino" value="feminino">
              <label for="feminino">Feminino</label>
            </div>
          </div>

          <div class ="row">
            <div class="medium-4 columns left">
              <label class="titulo">CEP</label>
              <input id="cep" name="cep" type="text"
                     onchange="preencheEndereco()" />
            </div>
            <div class="medium-4 columns float-left">
              <label class="titulo">Nro. endereço</label>
              <input id="nroEndereco" name="nroEndereco" type="number" min="1"  />
            </div>
            <div class="medium-4 columns">
              <label class="titulo">Complemento</label>
              <input id="complemento" name="complemento" type="text" pattern="[a-zA-ZçÇáÁéÉíÍóÓúÚ ]+"/>
            </div>
          </div>
          <div class="row">
            <div class="large-4 medium-4 columns">
              <label class="titulo">País</label>
              <input id="nomePais" name="nomePais" type="text" readonly="readonly" />
            </div>
            <div class="medium-4 columns">
              <label class="titulo">Estado</label>
              <input id="nomeUF" name="nomeUF" type="text" readonly="readonly" />
            </div>
            <div class="medium-4 columns ">
              <label class="titulo">Cidade</label>
              <input id="nomeCidade" name="nomeCidade" type="text" readonly="readonly" />
            </div>
            <div class="medium-4 columns">
              <label class="titulo">Bairro</label>
              <input id="nomeBairro" name="nomeBairro" type="text" readonly="readonly" />
            </div>
            <div class="medium-4 columns">
              <label class="titulo">Tipo do logradouro</label>
              <input id="nomeTipoLogradouro" name="nomeTipoLogradouro" type="text" readonly="readonly" />
            </div>
            <div class="medium-4 columns">
              <label class="titulo">Logradouro</label>
              <input id="nomeLogradouro" name="nomeLogradouro" type="text" readonly="readonly" />
            </div>
          </div>
          <br>
          <input class="button" type="submit" value="Cadastrar" onclick="checa()" />
          <a href="index.jsp" class="secondary button">Voltar</a><br>
        </form>

        <script type="text/javascript">
          $(document).ready(function () {
            $('.group').hide();
            $('#pessoaFisica').show();
            $('#tipoCadastro').change(function () {
              $('.group').hide();
              $('#'+$(this).val()).show();
            })
          });
        </script>

      </div>
    </div>

    <script>
      function preencheEndereco() {
        var arrayCEP = [100];
        var arrayPais = [100];
        var arrayUF = [100];
        var arrayCidade = [100];
        var arrayBairro = [100];
        var arrayLogradouro = [100];
        var arrayTipoLogradouro = [100];

        <%
          int ii;
          for (ii = 0; ii < listaEndereco.size(); ii++) {%>
            arrayCEP[<%= ii %>] = "<%= listaEndereco.get(ii).getCep() %>";
          <%}
          for (ii = 0; ii < listaPais.size(); ii++) {%>
            arrayPais[<%= ii %>] = "<%= listaPais.get(ii).getNomePais() %>";
          <%}
          for (ii = 0; ii < listaUF.size(); ii++) {%>
            arrayUF[<%= ii %>] = "<%= listaUF.get(ii).getNomeUnidadeFederativa() %>";
          <%}
          for (ii = 0; ii < listaCidade.size(); ii++) {%>
            arrayCidade[<%= ii %>] = "<%= listaCidade.get(ii).getNomeCidade() %>";
          <%}
          for (ii = 0; ii < listaBairro.size(); ii++) {%>
            arrayBairro[<%= ii %>] = "<%= listaBairro.get(ii).getNomeBairro() %>";
          <%}
          for (ii = 0; ii < listaLogradouro.size(); ii++) {%>
            arrayLogradouro[<%= ii %>] = "<%= listaLogradouro.get(ii).getNomeLogradouro() %>";
          <%}
          for (ii = 0; ii < listaTipoLogradouro.size(); ii++) {%>
            arrayTipoLogradouro[<%= ii %>] = "<%= listaTipoLogradouro.get(ii).getNomeTipoLogradouro() %>";
          <%}
        %>

        for (var ii = 0; ii < arrayCEP.length; ii++) {
          if (arrayCEP[ii] === document.getElementById("cep").value) {
            document.getElementById("nomePais").value = arrayPais[ii];
            document.getElementById("nomeUF").value = arrayUF[ii];
            document.getElementById("nomeCidade").value = arrayCidade[ii];
            document.getElementById("nomeBairro").value = arrayBairro[ii];
            document.getElementById("nomeLogradouro").value = arrayLogradouro[ii];
            document.getElementById("nomeTipoLogradouro").value = arrayTipoLogradouro[ii];
          }
        }
      }
    </script>

    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
  </body>

</html>
