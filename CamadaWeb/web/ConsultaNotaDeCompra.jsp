<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<!--suppress HtmlFormInputWithoutLabel -->
<html class="no-js" lang="en" dir="ltr">

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Sistema de Controle de Estoque</title>
		<link rel="stylesheet" href="css/foundation.css">
		<link rel="stylesheet" href="css/app.css">
		<script src="js/vendor/jquery.js"></script>
	</head>

	<body>
		<jsp:include page="/biblioteca/menu.jsp"/><br>

		<div class="row columns borda">
			<div class="blog-post">

				<h4 class="titulo">Consulta de Nota de Compra</h4>

				<form action="ConsultaNotaDeCompraServlet" method="post" autocomplete="on">
					<div class="group">
						<div class="large-2 medium-2">
							<label>
								<strong>Nro. da nota</strong>
								<input name="nroNota" type="number" min="1" required="required"/>
								<p class="help-text">Número >= 1</p>
							</label>
							<input class="button" type="submit" value="Pesquisar"/><br>
						</div>
					</div>

			</div>
		</div>

		<script src="js/vendor/what-input.js"></script>
		<script src="js/vendor/foundation.js"></script>
		<script src="js/app.js"></script>
	</body>

</html>
