package servlet;

import unioeste.geral.bo.produto.Produto;
import unioeste.geral.produto.manager.UCManterProdutoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@WebServlet("/AlteraPesquisaProdutoServlet")
public class AlteraPesquisaProdutoServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		request.setCharacterEncoding("UTF-8"); // recebe caracteres UTF-8 do JSP
		String id = request.getParameter("id");
		String codigoBarras = request.getParameter("codigoDeBarras");
		Produto produto = new Produto();
		String uuid = UUID.randomUUID().toString();

		try {
			// Busca por ID
			if (!id.equals("") && codigoBarras.equals("")) {
				produto.setIdProduto(Integer.parseInt(id));
				produto = new UCManterProdutoImpl().obterProduto(produto);
			}

			requestDispatcher = request.getRequestDispatcher("AlteraProduto.jsp");
			if (produto != null) {
				request.setAttribute("produto", produto);
				request.getSession().setAttribute(uuid, produto);
				request.setAttribute("uuid", uuid);
			}
		} catch (Exception e) {
			requestDispatcher = request.getRequestDispatcher("Falha.jsp");
			request.setAttribute("e", e);
		}
		request.setAttribute("pagina", "AlteraPesquisaProduto.jsp");
		requestDispatcher.forward(request, response);
	}
}
