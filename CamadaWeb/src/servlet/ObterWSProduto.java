package servlet;

import unioeste.geral.produto.manager.UCManterProduto;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class ObterWSProduto {

	public UCManterProduto obterWS(String classe) {
		URL url = null;
		try {
			// JBoss:
			url = new URL("http://localhost:8080/CamadaWeb/UCManterProdutoImpl?wsdl");
			// GlassFish:
//			url = new URL("http://localhost:8080/CamadaWeb/UCManterProdutoImplService?wsdl");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		QName qName = new QName("http://manager.produto.geral.unioeste/", "UCManterProdutoImplService");
		Service service = Service.create(url, qName);
		return service.getPort(UCManterProduto.class);
	}

}
