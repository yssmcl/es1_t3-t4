package servlet;

import unioeste.geral.bo.produto.Produto;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ExcluiProdutoServlet")
public class ExcluiProdutoServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uuid = request.getParameter("uuid");
        Produto produto = (Produto) request.getSession().getAttribute(uuid);
        RequestDispatcher requestDispatcher;
        request.setAttribute("pagina", "ExcluiProduto.jsp");
        try {
            new ObterWSProduto().obterWS("UCManterProdutoImpl").excluirProduto(produto);
            requestDispatcher = request.getRequestDispatcher("Sucesso.jsp");
        } catch(Exception e) {
            requestDispatcher = request.getRequestDispatcher("Falha.jsp");
        }
        requestDispatcher.forward(request, response);
    }
}

