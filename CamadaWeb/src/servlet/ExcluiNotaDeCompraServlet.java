package servlet;

import unioeste.geral.bo.nota.NotaCompra;
import unioeste.geral.nota.manager.UCManterNotaCompra;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ExcluiNotaDeCompraServlet")
public class ExcluiNotaDeCompraServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uuid = request.getParameter("uuid");
		NotaCompra notaCompra = (NotaCompra) request.getSession().getAttribute(uuid);
		RequestDispatcher requestDispatcher;
		request.setAttribute("pagina", "ExcluiNotaDeCompra.jsp");
		try {
			new UCManterNotaCompra().excluirNotaCompra(notaCompra);
			requestDispatcher = request.getRequestDispatcher("Sucesso.jsp");
		} catch(Exception e) {
			requestDispatcher = request.getRequestDispatcher("Falha.jsp");
		}
		requestDispatcher.forward(request, response);
	}

}
