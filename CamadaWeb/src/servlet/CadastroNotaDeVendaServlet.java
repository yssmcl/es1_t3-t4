package servlet;

import unioeste.geral.bo.nota.FormaPagamento;
import unioeste.geral.bo.nota.ItemNotaVenda;
import unioeste.geral.bo.nota.NotaVenda;
import unioeste.geral.bo.pessoa.Cliente;
import unioeste.geral.bo.produto.Produto;
import unioeste.geral.nota.manager.FormaPagamentoControle;
import unioeste.geral.nota.manager.UCManterNotaVenda;
import unioeste.geral.pessoa.manager.UCManterCliente;
import unioeste.geral.produto.manager.UCManterProdutoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/CadastroNotaDeVendaServlet")
public class CadastroNotaDeVendaServlet extends HttpServlet {

	private ItemNotaVenda criarItemNotaVenda(double precoUnitario, int qtdItemNota, Produto produto) {
		ItemNotaVenda itemNotaVenda = new ItemNotaVenda();
		itemNotaVenda.setPrecoUnitario(precoUnitario);
		itemNotaVenda.setQtdItemNota(qtdItemNota);
		itemNotaVenda.setProduto(produto);
		return itemNotaVenda;
	}

	public List<ItemNotaVenda> criarListaItemNotaVenda(int[] idProduto, int[] qtdProduto) {
		List<ItemNotaVenda> lista = new ArrayList<>();
		for (int i = 0; i < idProduto.length; i++) {
			Produto produto = new Produto();
			produto.setIdProduto(idProduto[i]);
			try {
				produto = new UCManterProdutoImpl().obterProduto(produto);
			} catch (Exception e) {
				e.printStackTrace();
			}
			lista.add(criarItemNotaVenda(produto.getPrecoCustoProduto(), qtdProduto[i], produto));
		}
		return lista;
	}

	private NotaVenda criarNotaVenda(Date dataNota, int idCliente, int[] idProdutos, int[] qtdProduto, double totalNota,
									 double descontoTotal, int idFormaPagamento) {
		Cliente cliente = null;
		FormaPagamento formaPagamento = null;
		try {
			cliente = new UCManterCliente().obterCliente(new Cliente() {{
				setIdCliente(idCliente);
			}});

			formaPagamento = new FormaPagamentoControle().recuperarFormaPagamentoPorPK(new FormaPagamento() {{
				setIdFormaPagamento(idFormaPagamento);
			}});
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<ItemNotaVenda> listaItemNotaVenda = criarListaItemNotaVenda(idProdutos, qtdProduto);

		for (ItemNotaVenda i : listaItemNotaVenda) {
			totalNota += i.getPrecoUnitario();
			i.setTotalItemNota(i.getQtdItemNota() * i.getPrecoUnitario());
		}
		double valorLiquido = totalNota - descontoTotal;

		NotaVenda notaVenda = new NotaVenda(
				dataNota,
				totalNota,
				descontoTotal,
				valorLiquido,
				cliente,
				listaItemNotaVenda,
				formaPagamento);

		for (ItemNotaVenda i : listaItemNotaVenda) {
			i.setNota(notaVenda);
		}

		return notaVenda;
	}

	private Date converterData(String dataNota) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date utilDate = null;
		try {
			utilDate = sdf.parse(dataNota);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new Date(utilDate != null ? utilDate.getTime() : 0);
	}

	private int[] strArray2IntArray(String[] strArray) {
		int intArray[] = new int[strArray.length];
		for (int i = 0; i < strArray.length; i++) {
			intArray[i] = Integer.parseInt(strArray[i]);
		}
		return intArray;
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		request.setAttribute("pagina", "CadastroNotaDeVendaServlet.jsp");
		request.setCharacterEncoding("UTF-8"); // pega caracteres UTF-8 do JSP

		String dataNota = request.getParameter("dataNota");
		String idCliente = request.getParameter("idCliente");
		String idProduto[] = request.getParameterValues("idProduto");
		String qtdProduto[] = request.getParameterValues("qtdProduto");
		String totalNota = request.getParameter("totalNota");
		String descontoTotal = request.getParameter("descontoTotal");
		String idFormaPagamento = request.getParameter("idFormaPagamento");

		NotaVenda notaVenda = criarNotaVenda(
				converterData(dataNota),
				Integer.parseInt(idCliente),
				strArray2IntArray(idProduto),
				strArray2IntArray(qtdProduto),
				Double.parseDouble(totalNota),
				Double.parseDouble(descontoTotal),
				Integer.parseInt(idFormaPagamento));

		try {
			new UCManterNotaVenda().cadastrarNotaVenda(notaVenda);
			requestDispatcher = request.getRequestDispatcher("Sucesso.jsp");
		} catch (Exception e) {
			request.setAttribute("e", e);
			requestDispatcher = request.getRequestDispatcher("Falha.jsp");
		}
		requestDispatcher.forward(request, response);
	}

}
