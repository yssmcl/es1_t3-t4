package servlet;

import unioeste.geral.bo.endereco.Endereco;
import unioeste.geral.bo.pessoa.CNPJ;
import unioeste.geral.bo.pessoa.EnderecoEspecifico;
import unioeste.geral.bo.pessoa.Fornecedor;
import unioeste.geral.endereco.exception.EnderecoException;
import unioeste.geral.endereco.manager.UCEnderecoGeralServicos;
import unioeste.geral.pessoa.manager.UCManterFornecedorBean;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/CadastroFornecedorServlet")
public class CadastroFornecedorServlet extends HttpServlet {

	private Fornecedor criarFornecedor(String nomeFantasia, String nomeRazaoSocial, String cnpj, String nroEndereco,
			String complementoEndereco, String cep) {
		// Busca endereço
		Endereco endereco = new Endereco();
		try {
			endereco = new UCEnderecoGeralServicos().obterEnderecoPorCEP(cep);
		} catch (EnderecoException e) {
			e.printStackTrace();
		}
		EnderecoEspecifico enderecoEspecifico = new EnderecoEspecifico(nroEndereco, complementoEndereco, endereco);
		CNPJ objCNPJ = new CNPJ(cnpj);
		return new Fornecedor(nomeFantasia, nomeRazaoSocial, objCNPJ, enderecoEspecifico);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		request.setAttribute("pagina", "CadastroFornecedor.jsp");
		request.setCharacterEncoding("UTF-8"); // pega caracteres UTF-8 do JSP

		String nomeFantasia =  request.getParameter("nomeFantasia");
		String nomeRazaoSocial = request.getParameter("nomeRazaoSocial");
		String cnpj = request.getParameter("cnpj");
		String cep = request.getParameter("cep");
		String nroEndereco = String.valueOf(request.getParameter("nroEndereco"));
		String complemento = request.getParameter("complemento");

		Fornecedor fornecedor = criarFornecedor(
				nomeFantasia, nomeRazaoSocial, cnpj, nroEndereco, complemento, cep);

		try {
			// TODO: EJB
//			InitialContext initialContext = new InitialContext();
//			UCManterFornecedorBean ucManterFornecedorBean =
//					(UCManterFornecedorBean) initialContext.lookup("UCManterFornecedorEJB");
//			ucManterFornecedorBean.cadastrarFornecedor(fornecedor);
		    new UCManterFornecedorBean().cadastrarFornecedor(fornecedor);
			requestDispatcher = request.getRequestDispatcher("Sucesso.jsp");
		} catch (Exception e) {
			request.setAttribute("e", e);
			requestDispatcher = request.getRequestDispatcher("Falha.jsp");
		}
		requestDispatcher.forward(request, response);
	}

}
