package servlet;

import unioeste.geral.bo.pessoa.Fornecedor;
import unioeste.geral.pessoa.manager.UCManterFornecedorBean;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ExcluiFornecedorServlet")
public class ExcluiFornecedorServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uuid = request.getParameter("uuid");
        Fornecedor fornecedor = (Fornecedor) request.getSession().getAttribute(uuid);
        RequestDispatcher requestDispatcher;
        request.setAttribute("pagina", "ExcluiFornecedor.jsp");
        try {
            new UCManterFornecedorBean().excluirFornecedor(fornecedor);
            requestDispatcher = request.getRequestDispatcher("Sucesso.jsp");
        } catch(Exception e) {
            requestDispatcher = request.getRequestDispatcher("Falha.jsp");
        }
        requestDispatcher.forward(request, response);
    }

}