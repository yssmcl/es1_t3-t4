package servlet;

import unioeste.geral.bo.pessoa.Cliente;
import unioeste.geral.pessoa.manager.UCManterCliente;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@WebServlet("/ExcluiPesquisaClienteServlet")
public class ExcluiPesquisaClienteServlet extends HttpServlet {

	public List<String> obterListaAtributos(Cliente cliente) {
		List<String> listaAtributos = new ArrayList<>();
		listaAtributos.add(String.valueOf(cliente.getIdCliente()));
		if (cliente.isPessoaFisica()) {
			listaAtributos.add(cliente.getNomePessoa().getNomeCompleto());
//            listaAtributos.add(String.valueOf(cliente.getCpf().getNumeroCPF()));
			listaAtributos.add(cliente.getCpf().getNumeroCPFFormatado());
			listaAtributos.add(String.valueOf(cliente.getSexo().getSiglaSexo()));
//            listaAtributos.add(cliente.getSexo().getNomeSexo());
		} else {
			listaAtributos.add(cliente.getNomeFantasia());
			listaAtributos.add(cliente.getNomeRazaoSocial());
//            listaAtributos.add(String.valueOf(cliente.getCnpj().getNumeroCNPJ()));
			listaAtributos.add(cliente.getCnpj().getNumeroCNPJFormatado());
		}
//            listaAtributos.add(String.valueOf(cliente.isPessoaJuridica()));
//            listaAtributos.add(String.valueOf(cliente.isPessoaFisica()));
		listaAtributos.add(cliente.getEnderecoPrincipal().getNroEndereco());
		listaAtributos.add(cliente.getEnderecoPrincipal().getComplementoEndereco());
		listaAtributos.add(cliente.getEnderecoPrincipal().getEndereco().getCep());
		return listaAtributos;
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8"); // recebe caracteres UTF-8 do JSP
		String id = request.getParameter("id");
		String cnpj = request.getParameter("cnpj");
		RequestDispatcher requestDispatcher;
		Cliente cliente = new Cliente();
		String uuid = UUID.randomUUID().toString();

		try {
			// Busca por ID
			if (!id.equals("") && cnpj.equals("")) {
				cliente.setIdCliente(Integer.parseInt(id));
				cliente = new UCManterCliente().obterCliente(cliente);
			}

			requestDispatcher = request.getRequestDispatcher("ExibeClienteExcluir.jsp");
			if (cliente != null) {
				List<String> listaAtributos = obterListaAtributos(cliente);
				request.setAttribute("atributos", listaAtributos);
				request.getSession().setAttribute(uuid, cliente);
				request.setAttribute("uuid", uuid);
			}
		} catch (Exception e) {
			requestDispatcher = request.getRequestDispatcher("Falha.jsp");
			request.setAttribute("e", e);
		}
		request.setAttribute("pagina", "ExcluiPesquisaCliente.jsp");
		requestDispatcher.forward(request, response);
	}

}
