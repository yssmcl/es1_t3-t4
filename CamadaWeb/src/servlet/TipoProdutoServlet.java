package servlet;

import unioeste.geral.bo.produto.TipoProduto;
import unioeste.geral.produto.manager.TipoProdutoControle;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/TipoProdutoServlet")
public class TipoProdutoServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            TipoProdutoControle controle= new TipoProdutoControle();
            List<TipoProduto> valores = controle.recuperarTodosTipoProduto();

            out.print("[");
            for(int i=0;i<valores.size();i++){
                out.print("{\"atributo\":\"".concat(valores.get(i).getNomeTipoProduto()).concat("\"}"));
                out.print(",{\"id\":\"".concat(String.valueOf(valores.get(i).getIdTipoProduto())).concat("\"}"));
                if((i != valores.size()-1)&&(valores.size()!=1)){
                    out.print(",");
                }
            }
            out.print("]\n");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
