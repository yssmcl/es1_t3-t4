package servlet;

import unioeste.geral.bo.pessoa.Fornecedor;
import unioeste.geral.pessoa.manager.UCManterFornecedorBean;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@WebServlet("/ExcluiPesquisaFornecedorServlet")
public class ExcluiPesquisaFornecedorServlet extends HttpServlet {

	public List<String> obterListaAtributos(Fornecedor fornecedor) {
		List<String> listaAtributos = new ArrayList<>();
		listaAtributos.add(String.valueOf(fornecedor.getIdFornecedor()));
		listaAtributos.add(fornecedor.getNomeFantasia());
		listaAtributos.add(fornecedor.getNomeRazaoSocial());
		listaAtributos.add(String.valueOf(fornecedor.getCnpj().getNumeroCNPJ()));
		listaAtributos.add(fornecedor.getCnpj().getNumeroCNPJFormatado());
		listaAtributos.add(fornecedor.getEnderecoPrincipal().getNroEndereco());
		listaAtributos.add(fornecedor.getEnderecoPrincipal().getComplementoEndereco());
		listaAtributos.add(fornecedor.getEnderecoPrincipal().getEndereco().getCep());
		return listaAtributos;
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8"); // recebe caracteres UTF-8 do JSP
		String id = request.getParameter("id");
		String cnpj = request.getParameter("cnpj");
		RequestDispatcher requestDispatcher;
		Fornecedor fornecedor = new Fornecedor();
		String uuid = UUID.randomUUID().toString();

		try {
			// Busca por ID
			if (!id.equals("") && cnpj.equals("")) {
				fornecedor.setIdFornecedor(Integer.parseInt(id));
				fornecedor = new UCManterFornecedorBean().obterFornecedor(fornecedor);
			}

			requestDispatcher = request.getRequestDispatcher("ExibeFornecedorExcluir.jsp");
			if (fornecedor != null) {
				List<String> listaAtributos = obterListaAtributos(fornecedor);
				request.setAttribute("atributos", listaAtributos);
				request.getSession().setAttribute(uuid, fornecedor);
				request.setAttribute("uuid", uuid);
			}
		} catch (Exception e) {
			requestDispatcher = request.getRequestDispatcher("Falha.jsp");
			request.setAttribute("e", e);
		}
		request.setAttribute("pagina", "ExcluiPesquisaFornecedor.jsp");
		requestDispatcher.forward(request, response);
	}

}
