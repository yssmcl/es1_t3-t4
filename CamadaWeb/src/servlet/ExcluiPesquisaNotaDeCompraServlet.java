package servlet;

import unioeste.geral.bo.nota.ItemNotaCompra;
import unioeste.geral.bo.nota.NotaCompra;
import unioeste.geral.nota.manager.UCManterNotaCompra;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@WebServlet("/ExcluiPesquisaNotaDeCompraServlet")
public class ExcluiPesquisaNotaDeCompraServlet extends HttpServlet {

	public List<String> obterListaAtributos(NotaCompra notaCompra) {
		List<String> listaAtributos = new ArrayList<>();
		listaAtributos.add(String.valueOf(notaCompra.getNroNota()));
		listaAtributos.add(String.valueOf(notaCompra.getDataEmissaoNota()));
		listaAtributos.add(String.valueOf(notaCompra.getTotalBrutoNota()));
		listaAtributos.add(String.valueOf(notaCompra.getDescontoTotalNota()));
		listaAtributos.add(String.valueOf(notaCompra.getValorLiquidoNota()));
		listaAtributos.add(String.valueOf(notaCompra.getFornecedor().getNomeFantasia()));

		String itens = new String();
		for (ItemNotaCompra itemNotaCompra : notaCompra.getListaItensCompra()) {
			itens += String.valueOf(itemNotaCompra.getProduto().getNomeProduto());
			itens += ", ";
		}
		itens = itens.substring(0, itens.length()-2); // pra tirar o ", " do final
		listaAtributos.add(itens);

		return listaAtributos;
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		request.setCharacterEncoding("UTF-8"); // recebe caracteres UTF-8 do JSP
		String nroNota = request.getParameter("nroNota");
		NotaCompra notaCompra = new NotaCompra();
		String uuid = UUID.randomUUID().toString();

		try {
			notaCompra.setNroNota(Integer.parseInt(nroNota));
			notaCompra = new UCManterNotaCompra().obterNotaCompra(notaCompra);

			requestDispatcher = request.getRequestDispatcher("ExibeNotaDeCompraExcluir.jsp");
			if (notaCompra != null) {
				List<String> listaAtributos = obterListaAtributos(notaCompra);
				request.setAttribute("atributos", listaAtributos);
				request.getSession().setAttribute(uuid, notaCompra);
				request.setAttribute("uuid", uuid);
			}
		} catch (Exception e) {
			requestDispatcher = request.getRequestDispatcher("Falha.jsp");
			request.setAttribute("e", e);
		}
		request.setAttribute("pagina", "ExcluiPesquisaNotaDeCompra.jsp");
		requestDispatcher.forward(request, response);
	}

}

