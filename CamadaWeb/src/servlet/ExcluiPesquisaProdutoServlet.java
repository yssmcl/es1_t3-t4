package servlet;

import unioeste.geral.bo.produto.Produto;
import unioeste.geral.produto.manager.UCManterProdutoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@WebServlet("/ExcluiPesquisaProdutoServlet")
public class ExcluiPesquisaProdutoServlet extends HttpServlet {

	public List<String> obterListaAtributos(Produto produto) {
		List<String> listaAtributos = new ArrayList<>();
		listaAtributos.add(String.valueOf(produto.getIdProduto()));
		listaAtributos.add(String.valueOf(produto.getCodigoBarrasProduto()));
		listaAtributos.add(produto.getNomeProduto());
		listaAtributos.add(String.valueOf(produto.getPrecoCustoProduto()));
		listaAtributos.add(String.valueOf(produto.getPrecoVendaProduto()));
		listaAtributos.add(String.valueOf(produto.getQtdProduto()));
		listaAtributos.add(produto.getTipoProduto().getNomeTipoProduto());
		listaAtributos.add(produto.getFornecedor().getNomeFantasia());
		return listaAtributos;
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		request.setCharacterEncoding("UTF-8"); // recebe caracteres UTF-8 do JSP
		String id = request.getParameter("id");
		String codigoBarras = request.getParameter("codigoDeBarras");
		Produto produto = new Produto();
		String uuid = UUID.randomUUID().toString();

		try {
			// Busca por ID
			if (!id.equals("") && codigoBarras.equals("")) {
				produto.setIdProduto(Integer.parseInt(id));
				produto = new UCManterProdutoImpl().obterProduto(produto);
			}

			requestDispatcher = request.getRequestDispatcher("ExibeProdutoExcluir.jsp");
			if (produto != null) {
				List<String> listaAtributos = obterListaAtributos(produto);
				request.setAttribute("atributos", listaAtributos);
				request.getSession().setAttribute(uuid, produto);
				request.setAttribute("uuid", uuid);
			}
		} catch (Exception e) {
			requestDispatcher = request.getRequestDispatcher("Falha.jsp");
			request.setAttribute("e", e);
		}
		request.setAttribute("pagina", "ExcluiPesquisaProduto.jsp");
		requestDispatcher.forward(request, response);
	}

}
