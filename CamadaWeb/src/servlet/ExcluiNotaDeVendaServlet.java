package servlet;

import unioeste.geral.bo.nota.NotaVenda;
import unioeste.geral.nota.manager.UCManterNotaVenda;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ExcluiNotaDeVendaServlet")
public class ExcluiNotaDeVendaServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uuid = request.getParameter("uuid");
		NotaVenda notaVenda = (NotaVenda) request.getSession().getAttribute(uuid);
		RequestDispatcher requestDispatcher;
		request.setAttribute("pagina", "ExcluiNotaDeVenda.jsp");
		try {
			new UCManterNotaVenda().excluirNotaVenda(notaVenda);
			requestDispatcher = request.getRequestDispatcher("Sucesso.jsp");
		} catch(Exception e) {
			requestDispatcher = request.getRequestDispatcher("Falha.jsp");
		}
		requestDispatcher.forward(request, response);
	}

}