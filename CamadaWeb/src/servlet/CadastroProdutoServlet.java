package servlet;

import unioeste.geral.bo.pessoa.Fornecedor;
import unioeste.geral.bo.produto.Produto;
import unioeste.geral.bo.produto.TipoProduto;
import unioeste.geral.pessoa.manager.UCManterFornecedorBean;
import unioeste.geral.produto.manager.TipoProdutoControle;
import unioeste.geral.produto.manager.UCManterProduto;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

@WebServlet("/CadastroProdutoServlet")
public class CadastroProdutoServlet extends HttpServlet {

    private Produto criarProduto(long codigoBarrasProduto, String nomeProduto, int idTipoProduto,
								 int idFornecedor, double precoCustoProduto,
                                 double precoVendaProduto, int qtdProduto) {
        TipoProduto tipoProduto = new TipoProduto();
        Fornecedor fornecedor = new Fornecedor();
        try {
			tipoProduto = new TipoProdutoControle().recuperarTipoProdutoPorPK(new TipoProduto() {{
				setIdTipoProduto(idTipoProduto);
			}});
			fornecedor = new UCManterFornecedorBean().obterFornecedor(new Fornecedor() {{
				setIdFornecedor(idFornecedor);
			}});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Produto(codigoBarrasProduto, nomeProduto, precoCustoProduto, precoVendaProduto, qtdProduto,
                tipoProduto, fornecedor);
    }

	private UCManterProduto obterRecurso(String recurso) {
		URL url = null;
		try {
			url = new URL("http://localhost:8080/CamadaWeb/" + recurso + "Service?wsdl");
//			url = new URL("http://localhost:8080/CamadaWeb/" + recurso + "?wsdl");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		QName qName = new QName("http://manager.produto.geral.unioeste/", recurso + "Service");
		Service service = Service.create(url, qName);
		return service.getPort(UCManterProduto.class);
	}

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		request.setAttribute("pagina", "CadastroProduto.jsp");
		request.setCharacterEncoding("UTF-8"); // pega caracteres UTF-8 do JSP

		String codigoDeBarras = request.getParameter("codigoDeBarras");
		String nomeProduto = request.getParameter("nomeProduto");
		String idTipoProduto = request.getParameter("tipoProduto");
		String idFornecedor = request.getParameter("fornecedor");
		String precoCusto = request.getParameter("precoCusto");
		String precoVenda = request.getParameter("precoVenda");
		String qtd = request.getParameter("qtd");

		Produto produto = criarProduto(
				Long.parseLong(codigoDeBarras),
				nomeProduto,
				Integer.parseInt(idTipoProduto),
				Integer.parseInt(idFornecedor),
				Double.parseDouble(precoCusto),
				Double.parseDouble(precoVenda),
				Integer.parseInt(qtd));

		try {
			UCManterProduto ucManterProduto = obterRecurso("UCManterProdutoImpl");
			ucManterProduto.cadastrarProduto(produto);
			requestDispatcher = request.getRequestDispatcher("Sucesso.jsp");
		} catch (Exception e) {
			request.setAttribute("e", e);
			requestDispatcher = request.getRequestDispatcher("Falha.jsp");
		}
		requestDispatcher.forward(request, response);

	}
}
