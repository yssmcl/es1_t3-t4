package servlet;

import unioeste.geral.bo.pessoa.Fornecedor;
import unioeste.geral.bo.produto.Produto;
import unioeste.geral.bo.produto.TipoProduto;
import unioeste.geral.pessoa.manager.FornecedorControle;
import unioeste.geral.produto.manager.TipoProdutoControle;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/AlteraProdutoServlet")
public class AlteraProdutoServlet extends HttpServlet {

	private Produto criarProduto(int id, long codigoBarrasProduto, String nomeProduto, String nomeTipoProduto,
								 String nomeFantasiaFornecedor, double precoCustoProduto,
								 double precoVendaProduto, int qtdProduto) {
		TipoProduto tipoProduto = new TipoProduto();
		Fornecedor fornecedor = new Fornecedor();
		try {
			tipoProduto = new TipoProdutoControle().recuperarTipoProdutoPorAtributo("nomeTipoProduto",
					nomeTipoProduto);
			fornecedor = new FornecedorControle().recuperarFornecedorPorAtributo("nomeFantasiaFornecedor",
					nomeFantasiaFornecedor);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new Produto(id, codigoBarrasProduto, nomeProduto, precoCustoProduto, precoVendaProduto, qtdProduto,
				tipoProduto, fornecedor);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		request.setAttribute("pagina", "AlteraProduto.jsp");
		request.setCharacterEncoding("UTF-8"); // pega caracteres UTF-8 do JSP

		String id = request.getParameter("id");
		String codigoDeBarras = request.getParameter("codigoDeBarras");
		String nomeProduto = request.getParameter("nomeProduto");
		String nomeTipoProduto = request.getParameter("tipoProduto");
		String nomeFantasiaFornecedor = request.getParameter("fornecedor");
		String precoCusto = request.getParameter("precoCusto");
		String precoVenda = request.getParameter("precoVenda");
		String qtd = request.getParameter("qtd");

		System.out.println(id);
		System.out.println(codigoDeBarras);
		System.out.println(nomeProduto);
		System.out.println(nomeTipoProduto);
		System.out.println(nomeFantasiaFornecedor);
		System.out.println(precoCusto);
		System.out.println(precoVenda);
		System.out.println(qtd);

		Produto produto = criarProduto(
				Integer.parseInt(id),
				Long.parseLong(codigoDeBarras),
				nomeProduto,
				nomeTipoProduto,
				nomeFantasiaFornecedor,
				Double.parseDouble(precoCusto),
				Double.parseDouble(precoVenda),
				Integer.parseInt(qtd));

		try {
			new ObterWSProduto().obterWS("UCManterProdutoImpl").alterarProduto(produto);
			requestDispatcher = request.getRequestDispatcher("Sucesso.jsp");
		} catch (Exception e) {
			request.setAttribute("e", e);
			requestDispatcher = request.getRequestDispatcher("Falha.jsp");
		}
		requestDispatcher.forward(request, response);
	}

}