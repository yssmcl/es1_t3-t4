package servlet;

import unioeste.geral.bo.nota.ItemNotaVenda;
import unioeste.geral.bo.nota.NotaVenda;
import unioeste.geral.nota.manager.UCManterNotaVenda;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@WebServlet("/ExcluiPesquisaNotaDeVendaServlet")
public class ExcluiPesquisaNotaDeVendaServlet extends HttpServlet {

	public List<String> obterListaAtributos(NotaVenda notaVenda) {
		List<String> listaAtributos = new ArrayList<>();
		listaAtributos.add(String.valueOf(notaVenda.getNroNota()));
		listaAtributos.add(String.valueOf(notaVenda.getDataEmissaoNota()));
		listaAtributos.add(String.valueOf(notaVenda.getTotalBrutoNota()));
		listaAtributos.add(String.valueOf(notaVenda.getDescontoTotalNota()));
		listaAtributos.add(String.valueOf(notaVenda.getValorLiquidoNota()));
		if (notaVenda.getCliente().isPessoaJuridica()) {
			listaAtributos.add(String.valueOf(notaVenda.getCliente().getNomeFantasia()));
		} else {
			listaAtributos.add(String.valueOf(notaVenda.getCliente().getNomePessoa().getNomeCompleto()));
		}

		String itens = new String();
		for (ItemNotaVenda itemNotaVenda : notaVenda.getListaItensVenda()) {
			itens += String.valueOf(itemNotaVenda.getProduto().getNomeProduto());
			itens += ", ";
		}
		itens = itens.substring(0, itens.length()-2); // pra tirar o ", " do final
		listaAtributos.add(itens);

		return listaAtributos;
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		request.setCharacterEncoding("UTF-8"); // recebe caracteres UTF-8 do JSP
		String nroNota = request.getParameter("nroNota");
		NotaVenda notaVenda = new NotaVenda();
		String uuid = UUID.randomUUID().toString();

		try {
			notaVenda.setNroNota(Integer.parseInt(nroNota));
			notaVenda = new UCManterNotaVenda().obterNotaVenda(notaVenda);

			requestDispatcher = request.getRequestDispatcher("ExibeNotaDeVendaExcluir.jsp");
			if (notaVenda != null) {
				List<String> listaAtributos = obterListaAtributos(notaVenda);
				request.setAttribute("atributos", listaAtributos);
				request.getSession().setAttribute(uuid, notaVenda);
				request.setAttribute("uuid", uuid);
			}
		} catch (Exception e) {
			requestDispatcher = request.getRequestDispatcher("Falha.jsp");
			request.setAttribute("e", e);
		}
		request.setAttribute("pagina", "ExcluiPesquisaNotaDeVenda.jsp");
		requestDispatcher.forward(request, response);
	}

}
