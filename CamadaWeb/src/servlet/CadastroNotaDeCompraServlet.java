package servlet;

import unioeste.geral.bo.nota.ItemNotaCompra;
import unioeste.geral.bo.nota.NotaCompra;
import unioeste.geral.bo.pessoa.Fornecedor;
import unioeste.geral.bo.produto.Produto;
import unioeste.geral.nota.manager.UCManterNotaCompra;
import unioeste.geral.pessoa.manager.UCManterFornecedorBean;
import unioeste.geral.produto.manager.UCManterProdutoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/CadastroNotaDeCompraServlet")
public class CadastroNotaDeCompraServlet extends HttpServlet {

	private ItemNotaCompra criarItemNotaCompra(double precoUnitario, int qtdItemNota, Produto produto) {
		ItemNotaCompra itemNotaCompra = new ItemNotaCompra();
		itemNotaCompra.setPrecoUnitario(precoUnitario);
		itemNotaCompra.setQtdItemNota(qtdItemNota);
		itemNotaCompra.setProduto(produto);
		return itemNotaCompra;
	}

	public List<ItemNotaCompra> criarListaItemNotaCompra(int[] idProduto, int[] qtdProduto) {
		List<ItemNotaCompra> lista = new ArrayList<>();
		for (int i = 0; i < idProduto.length; i++) {
			Produto produto = new Produto();
			produto.setIdProduto(idProduto[i]);
			try {
				produto = new UCManterProdutoImpl().obterProduto(produto);
			} catch (Exception e) {
				e.printStackTrace();
			}
			lista.add(criarItemNotaCompra(produto.getPrecoCustoProduto(), qtdProduto[i], produto));
		}
		return lista;
	}

	private NotaCompra criarNotaCompra(Date dataNota, int idFornecedor, int[] idProdutos, int[] qtdProduto, double totalNota,
									   double descontoTotal) {
		Fornecedor fornecedor = null;
		try {
			fornecedor = new UCManterFornecedorBean().obterFornecedor(new Fornecedor() {{
				setIdFornecedor(idFornecedor);
			}});

		} catch (Exception e) {
			e.printStackTrace();
		}

		List<ItemNotaCompra> listaItemNotaCompra = criarListaItemNotaCompra(idProdutos, qtdProduto);

		for (ItemNotaCompra i : listaItemNotaCompra) {
			totalNota += i.getPrecoUnitario();
			i.setTotalItemNota(i.getQtdItemNota() * i.getPrecoUnitario());
		}
		double valorLiquido = totalNota - descontoTotal;

		NotaCompra notaCompra = new NotaCompra(
				dataNota,
				totalNota,
				descontoTotal,
				valorLiquido,
				fornecedor,
				listaItemNotaCompra);

		for (ItemNotaCompra i : listaItemNotaCompra) {
			i.setNota(notaCompra);
		}

		return notaCompra;
	}

	private Date converterData(String dataNota) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date utilDate = null;
		try {
			utilDate = sdf.parse(dataNota);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new Date(utilDate != null ? utilDate.getTime() : 0);
	}

	private int[] strArray2IntArray(String[] strArray) {
		int intArray[] = new int[strArray.length];
		for (int i = 0; i < strArray.length; i++) {
			intArray[i] = Integer.parseInt(strArray[i]);
		}
		return intArray;
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		request.setAttribute("pagina", "CadastroNotaDeCompraServlet.jsp");
		request.setCharacterEncoding("UTF-8"); // pega caracteres UTF-8 do JSP

		String dataNota = request.getParameter("dataNota");
		String idFornecedor = request.getParameter("idFornecedor");
		String idProduto[] = request.getParameterValues("idProduto");
		String qtdProduto[] = request.getParameterValues("qtdProduto");
		String totalNota = request.getParameter("totalNota");
		String descontoTotal = request.getParameter("descontoTotal");

		NotaCompra notaCompra = criarNotaCompra(
				converterData(dataNota),
				Integer.parseInt(idFornecedor),
				strArray2IntArray(idProduto),
				strArray2IntArray(qtdProduto),
				Double.parseDouble(totalNota),
				Double.parseDouble(descontoTotal));

		try {
			new UCManterNotaCompra().cadastrarNotaCompra(notaCompra);
			requestDispatcher = request.getRequestDispatcher("Sucesso.jsp");
		} catch (Exception e) {
			request.setAttribute("e", e);
			requestDispatcher = request.getRequestDispatcher("Falha.jsp");
		}
		requestDispatcher.forward(request, response);
	}
}
