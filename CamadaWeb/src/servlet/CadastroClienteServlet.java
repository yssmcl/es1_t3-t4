package servlet;

import unioeste.geral.bo.endereco.Endereco;
import unioeste.geral.bo.pessoa.*;
import unioeste.geral.endereco.exception.EnderecoException;
import unioeste.geral.endereco.manager.UCEnderecoGeralServicos;
import unioeste.geral.pessoa.manager.UCManterCliente;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@WebServlet("/CadastroClienteServlet")
public class CadastroClienteServlet extends HttpServlet {

    private List<Telefone> criarListaTelefone(Telefone... telefone) {
        List<Telefone> listaTelefones = new ArrayList<>();
        Collections.addAll(listaTelefones, telefone);
        return listaTelefones;
    }

    private Cliente criarClientePJ(String nroTelefone, String nroEndereco, String complementoEndereco, String cep,
                                   String nomeFantasia, String nomeRazaoSocial, String cnpj) {
        Endereco endereco = new Endereco();
        try {
            endereco = new UCEnderecoGeralServicos().obterEnderecoPorCEP(cep);
        } catch (EnderecoException e) {
            e.printStackTrace();
        }
        nroTelefone = nroTelefone.replaceAll("[\\s\\-()]", "");
        DDD dddPR = new DDD(Integer.parseInt(nroTelefone.substring(0, 2)));
        TipoTelefone tipoResidencial = new TipoTelefone(2, "residencial");
        Telefone telefone = new Telefone(Integer.parseInt(nroTelefone.substring(2, nroTelefone.length())), dddPR, tipoResidencial);
        List<Telefone> listaTelefones = criarListaTelefone(telefone);
        List<Email> listaEmails = new ArrayList<>();
        EnderecoEspecifico enderecoEspecifico = new EnderecoEspecifico(nroEndereco, complementoEndereco, endereco);
        CNPJ objCNPJ = new CNPJ(cnpj);
        return new Cliente(listaEmails, listaTelefones, enderecoEspecifico, nomeFantasia, nomeRazaoSocial, objCNPJ);
    }

    private Cliente criarClientePF(String nroTelefone, String nroEndereco, String complementoEndereco, String cep,
                                   String sexo, String cpf, String nomeCompleto) {
        Endereco endereco = new Endereco();
        try {
            endereco = new UCEnderecoGeralServicos().obterEnderecoPorCEP(cep);
        } catch (EnderecoException e) {
            e.printStackTrace();
        }
        nroTelefone = nroTelefone.replaceAll("[\\s\\-()]", "");
        DDD dddPR = new DDD(Integer.parseInt(nroTelefone.substring(0, 2)));
        TipoTelefone tipoResidencial = new TipoTelefone(2, "residencial");
        Telefone telefone = new Telefone(Integer.parseInt(nroTelefone.substring(2, nroTelefone.length())), dddPR, tipoResidencial);
        List<Telefone> listaTelefones = criarListaTelefone(telefone);
        EnderecoEspecifico enderecoEspecifico = new EnderecoEspecifico(nroEndereco, complementoEndereco, endereco);
        Sexo objSexo = new Sexo(sexo);
        CPF objCPF = new CPF(cpf);
        NomePessoa nomePessoa = new NomePessoa(nomeCompleto);
		List<Email> listaEmails = new ArrayList<>();
        return new Cliente(listaEmails, listaTelefones, enderecoEspecifico, objSexo, objCPF, nomePessoa);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher;
        request.setAttribute("pagina", "CadastroCliente.jsp");
        request.setCharacterEncoding("UTF-8"); // pega caracteres UTF-8 do JSP

		String primeiroNome = request.getParameter("primeiroNome");
		String nomeDoMeio = request.getParameter("nomeDoMeio");
		String ultimoNome = request.getParameter("ultimoNome");
		String cpf = request.getParameter("cpf");
		String nomeFantasia = request.getParameter("nomeFantasia");
		String nomeRazaoSocial = request.getParameter("nomeRazaoSocial");
		String cnpj = request.getParameter("cnpj");
        String telefone = request.getParameter("telefone");
        String sexo = request.getParameter("sexo");
        String cep = request.getParameter("cep");
        String nroEndereco = String.valueOf(request.getParameter("nroEndereco"));
        String complemento = request.getParameter("complemento");
        String nomeCompleto = primeiroNome + " " + nomeDoMeio + " " + ultimoNome;

		Cliente cliente;
		if (request.getParameter("tipoCadastro").equals("pessoaFisica")) {
            cliente = criarClientePF(telefone, nroEndereco, complemento, cep, sexo, cpf, nomeCompleto);
		} else {
            cliente = criarClientePJ(telefone, nroEndereco, complemento, cep, nomeFantasia, nomeRazaoSocial, cnpj);
        }

        try {
            if (cliente != null) new UCManterCliente().cadastrarCliente(cliente);
            requestDispatcher = request.getRequestDispatcher("Sucesso.jsp");
        } catch (Exception e) {
            request.setAttribute("e", e);
            requestDispatcher = request.getRequestDispatcher("Falha.jsp");
        }
        requestDispatcher.forward(request, response);
    }

}
