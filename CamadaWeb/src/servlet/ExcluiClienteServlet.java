package servlet;

import unioeste.geral.bo.pessoa.Cliente;
import unioeste.geral.pessoa.manager.UCManterCliente;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ExcluiClienteServlet")
public class ExcluiClienteServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uuid = request.getParameter("uuid");
        Cliente cliente = (Cliente) request.getSession().getAttribute(uuid);
        RequestDispatcher requestDispatcher;
        request.setAttribute("pagina", "ExcluiCliente.jsp");
        try {
            new UCManterCliente().excluirCliente(cliente);
            requestDispatcher = request.getRequestDispatcher("Sucesso.jsp");
        } catch(Exception e) {
            requestDispatcher = request.getRequestDispatcher("Falha.jsp");
        }
        requestDispatcher.forward(request, response);
    }

}