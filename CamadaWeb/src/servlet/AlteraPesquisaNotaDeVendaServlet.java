
package servlet;

import unioeste.geral.bo.nota.NotaVenda;
import unioeste.geral.nota.manager.UCManterNotaVenda;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@WebServlet("/AlteraPesquisaNotaDeVendaServlet")
public class AlteraPesquisaNotaDeVendaServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		request.setCharacterEncoding("UTF-8"); // recebe caracteres UTF-8 do JSP
		String id = request.getParameter("nroNota");
		NotaVenda notaVenda = new NotaVenda();
		String uuid = UUID.randomUUID().toString();

		try {
			notaVenda.setNroNota(Integer.parseInt(id));
			notaVenda = new UCManterNotaVenda().obterNotaVenda(notaVenda);

			requestDispatcher = request.getRequestDispatcher("AlteraNotaDeVenda.jsp");
			if (notaVenda != null) {
				request.setAttribute("notaVenda", notaVenda);
				request.getSession().setAttribute(uuid, notaVenda);
				request.setAttribute("uuid", uuid);
			}
		} catch (Exception e) {
			requestDispatcher = request.getRequestDispatcher("Falha.jsp");
			request.setAttribute("e", e);
		}
		request.setAttribute("pagina", "AlteraPesquisaNotaDeVenda.jsp");
		requestDispatcher.forward(request, response);
	}
}
