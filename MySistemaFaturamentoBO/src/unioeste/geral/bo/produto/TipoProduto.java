package unioeste.geral.bo.produto;

import java.io.Serializable;

public class TipoProduto implements Serializable {
	private static final long serialVersionUID = 5499723315544100781L;
	private int idTipoProduto;
	private String nomeTipoProduto;

    public TipoProduto() {
    }

    public TipoProduto(int idTipoProduto, String nomeTipoProduto) {
        this.idTipoProduto = idTipoProduto;
		this.nomeTipoProduto = nomeTipoProduto;
    }

	public TipoProduto(String nomeTipoProduto) {
		this.nomeTipoProduto = nomeTipoProduto;
	}

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getIdTipoProduto() {
        return idTipoProduto;
    }

    public void setIdTipoProduto(int idTipoProduto) {
        this.idTipoProduto = idTipoProduto;
    }

	public String getNomeTipoProduto() {
		return nomeTipoProduto;
	}

	public void setNomeTipoProduto(String nomeTipoProduto) {
		this.nomeTipoProduto = nomeTipoProduto;
	}

}
