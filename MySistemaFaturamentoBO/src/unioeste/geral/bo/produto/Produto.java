package unioeste.geral.bo.produto;

import unioeste.geral.bo.pessoa.Fornecedor;

import java.io.Serializable;

public class Produto implements Serializable {
	private static final long serialVersionUID = -7297468868484857735L;
	private int idProduto;
	private long codigoBarrasProduto;
	private String nomeProduto;
	private double precoCustoProduto;
	private double precoVendaProduto;
	private int qtdProduto;
	private TipoProduto tipoProduto;
	private Fornecedor fornecedor;

    public Produto() {
    }

	public Produto(long codigoBarrasProduto, String nomeProduto, double precoCustoProduto,
				   double precoVendaProduto, int qtdProduto, TipoProduto tipoProduto, Fornecedor fornecedor) {
		this.codigoBarrasProduto = codigoBarrasProduto;
		this.nomeProduto = nomeProduto;
		this.precoCustoProduto = precoCustoProduto;
		this.precoVendaProduto = precoVendaProduto;
		this.qtdProduto = qtdProduto;
		this.tipoProduto = tipoProduto;
		this.fornecedor = fornecedor;
	}

    public Produto(int idProduto, long codigoBarrasProduto, String nomeProduto, double precoCustoProduto,
				   double precoVendaProduto, int qtdProduto, TipoProduto tipoProduto, Fornecedor fornecedor) {
        this.idProduto = idProduto;
        this.codigoBarrasProduto = codigoBarrasProduto;
        this.nomeProduto = nomeProduto;
        this.precoCustoProduto = precoCustoProduto;
        this.precoVendaProduto = precoVendaProduto;
        this.qtdProduto = qtdProduto;
        this.tipoProduto = tipoProduto;
        this.fornecedor = fornecedor;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public long getCodigoBarrasProduto() {
        return codigoBarrasProduto;
    }

    public void setCodigoBarrasProduto(long codigoBarrasProduto) {
        this.codigoBarrasProduto = codigoBarrasProduto;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public double getPrecoCustoProduto() {
        return precoCustoProduto;
    }

    public void setPrecoCustoProduto(double precoCustoProduto) {
        this.precoCustoProduto = precoCustoProduto;
    }

    public double getPrecoVendaProduto() {
        return precoVendaProduto;
    }

    public void setPrecoVendaProduto(double precoVendaProduto) {
        this.precoVendaProduto = precoVendaProduto;
    }

    public int getQtdProduto() {
        return qtdProduto;
    }

    public void setQtdProduto(int qtdProduto) {
        this.qtdProduto = qtdProduto;
    }

    public TipoProduto getTipoProduto() {
        return tipoProduto;
    }

    public void setTipoProduto(TipoProduto tipoProduto) {
        this.tipoProduto = tipoProduto;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }
}
