package unioeste.geral.bo.nota;

import unioeste.geral.bo.produto.Produto;

import java.io.Serializable;

public abstract class ItemNota implements Serializable {
	private static final long serialVersionUID = 6384103101245341744L;
	private int idItemNota;
	private double precoUnitario;
	private int qtdItemNota;
	private double totalItemNota;
	private Produto produto;
    private Nota nota;

    public ItemNota() {
    }

    public ItemNota(int idItemNota, double precoUnitario, int qtdItemNota, double totalItemNota, Produto produto, Nota nota) {
        this.idItemNota = idItemNota;
        this.precoUnitario = precoUnitario;
        this.qtdItemNota = qtdItemNota;
        this.totalItemNota = totalItemNota;
        this.produto = produto;
        this.nota = nota;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getIdItemNota() {
        return idItemNota;
    }

    public void setIdItemNota(int idItemNota) {
        this.idItemNota = idItemNota;
    }

    public double getPrecoUnitario() {
        return precoUnitario;
    }

    public void setPrecoUnitario(double precoUnitario) {
        this.precoUnitario = precoUnitario;
    }

    public int getQtdItemNota() {
        return qtdItemNota;
    }

    public void setQtdItemNota(int qtdItemNota) {
        this.qtdItemNota = qtdItemNota;
    }

    public double getTotalItemNota() {
        return totalItemNota;
    }

    public void setTotalItemNota(double totalItemNota) {
        this.totalItemNota = totalItemNota;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

	public Nota getNota() {
		return nota;
	}

	public void setNota(Nota nota) {
		this.nota = nota;
	}
}
