package unioeste.geral.bo.nota;

import unioeste.geral.bo.pessoa.Cliente;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

public class NotaVenda extends Nota implements Serializable {
	private static final long serialVersionUID = 2621604105768477080L;
	private Cliente cliente;
    private List<ItemNotaVenda> listaItensVenda;
    private FormaPagamento formaPagamento;

    public NotaVenda() {
    }

    public NotaVenda(int nroNota, Date dataEmissaoNota, double totalBrutoNota, double descontoTotalNota,
                     double valorLiquidoNota, Cliente cliente, List<ItemNotaVenda> listaItensVenda, FormaPagamento formaPagamento) {
        super(nroNota, dataEmissaoNota, totalBrutoNota, descontoTotalNota, valorLiquidoNota);
        this.cliente = cliente;
        this.listaItensVenda = listaItensVenda;
		this.formaPagamento =  formaPagamento;
    }

    public NotaVenda(Date dataEmissaoNota, double totalBrutoNota, double descontoTotalNota, double valorLiquidoNota,
                     Cliente cliente, List<ItemNotaVenda> listaItensVenda, FormaPagamento formaPagamento) {
        super(dataEmissaoNota, totalBrutoNota, descontoTotalNota, valorLiquidoNota);
        this.cliente = cliente;
        this.listaItensVenda = listaItensVenda;
        this.formaPagamento = formaPagamento;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<ItemNotaVenda> getListaItensVenda() {
        return listaItensVenda;
    }

    public void setListaItensVenda(List<ItemNotaVenda> listaItensVenda) {
        this.listaItensVenda = listaItensVenda;
    }

    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
    }
}
