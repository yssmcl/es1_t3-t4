package unioeste.geral.bo.nota;

import unioeste.geral.bo.produto.Produto;

import java.io.Serializable;

public class ItemNotaCompra extends ItemNota implements Serializable {
	private static final long serialVersionUID = 5084331006150783242L;

    public ItemNotaCompra() {
    }

    public ItemNotaCompra(int idItemNota, double precoUnitario, int qtdItemNota, double totalItemNota, Produto produto,
                          NotaCompra notaCompra) {
        super(idItemNota, precoUnitario, qtdItemNota, totalItemNota, produto, notaCompra);
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
