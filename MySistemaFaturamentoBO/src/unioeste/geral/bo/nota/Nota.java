package unioeste.geral.bo.nota;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

public abstract class Nota implements Serializable {
	private static final long serialVersionUID = -6301623623599450399L;
	private int nroNota;
	private Date dataEmissaoNota;
	private double totalBrutoNota;
	private double descontoTotalNota;
	private double valorLiquidoNota;
	private List<ItemNota> listaItens;

    public Nota() {
    }

    public Nota(int nroNota, Date dataEmissaoNota, double totalBrutoNota, double descontoTotalNota, double valorLiquidoNota) {
        this.nroNota = nroNota;
        this.dataEmissaoNota = dataEmissaoNota;
        this.totalBrutoNota = totalBrutoNota;
        this.descontoTotalNota = descontoTotalNota;
        this.valorLiquidoNota = valorLiquidoNota;
    }

    public Nota(Date dataEmissaoNota, double totalBrutoNota, double descontoTotalNota, double valorLiquidoNota) {
        this.dataEmissaoNota = dataEmissaoNota;
        this.totalBrutoNota = totalBrutoNota;
        this.descontoTotalNota = descontoTotalNota;
        this.valorLiquidoNota = valorLiquidoNota;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getNroNota() {
        return nroNota;
    }

    public void setNroNota(int nroNota) {
        this.nroNota = nroNota;
    }

    public Date getDataEmissaoNota() {
        return dataEmissaoNota;
    }

    public void setDataEmissaoNota(Date dataEmissaoNota) {
        this.dataEmissaoNota = dataEmissaoNota;
    }

    public double getTotalBrutoNota() {
        return totalBrutoNota;
    }

    public void setTotalBrutoNota(double totalBrutoNota) {
        this.totalBrutoNota = totalBrutoNota;
    }

    public double getDescontoTotalNota() {
        return descontoTotalNota;
    }

    public void setDescontoTotalNota(double descontoTotalNota) {
        this.descontoTotalNota = descontoTotalNota;
    }

    public double getValorLiquidoNota() {
        return valorLiquidoNota;
    }

    public void setValorLiquidoNota(double valorLiquidoNota) {
        this.valorLiquidoNota = valorLiquidoNota;
    }

    public List<ItemNota> getListaItens() {
        return listaItens;
    }

    public void setListaItens(List<ItemNota> listaItens) {
        this.listaItens = listaItens;
    }
}
