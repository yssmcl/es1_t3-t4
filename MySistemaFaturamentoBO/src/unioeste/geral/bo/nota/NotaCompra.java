package unioeste.geral.bo.nota;

import unioeste.geral.bo.pessoa.Fornecedor;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

public class NotaCompra extends Nota implements Serializable {
	private static final long serialVersionUID = -3322671827790792203L;
	private Fornecedor fornecedor;
    private List<ItemNotaCompra> listaItensCompra;

    public NotaCompra() {
    }

    public NotaCompra(int nroNota, Date dataEmissaoNota, double totalBrutoNota, double descontoTotalNota,
                      double valorLiquidoNota, Fornecedor fornecedor, List<ItemNotaCompra> listaItensCompra) {
        super(nroNota, dataEmissaoNota, totalBrutoNota, descontoTotalNota, valorLiquidoNota);
        this.fornecedor = fornecedor;
        this.listaItensCompra = listaItensCompra;
    }

    public NotaCompra(Date dataEmissaoNota, double totalBrutoNota, double descontoTotalNota, double valorLiquidoNota,
                      Fornecedor fornecedor, List<ItemNotaCompra> listaItensCompra) {
        super(dataEmissaoNota, totalBrutoNota, descontoTotalNota, valorLiquidoNota);
        this.fornecedor = fornecedor;
        this.listaItensCompra = listaItensCompra;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public List<ItemNotaCompra> getListaItensCompra() {
        return listaItensCompra;
    }

    public void setListaItensCompra(List<ItemNotaCompra> listaItensCompra) {
        this.listaItensCompra = listaItensCompra;
    }
}
