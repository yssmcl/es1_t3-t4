package unioeste.geral.bo.nota;

import java.io.Serializable;

public class FormaPagamento implements Serializable {
	private static final long serialVersionUID = 3713746589817379366L;
	private String nomeFormaPagamento;
	private int idFormaPagamento;

    public FormaPagamento() {
    }

    public FormaPagamento(int idFormaPagamento, String nomeFormaPagamento) {
        this.nomeFormaPagamento = nomeFormaPagamento;
        this.idFormaPagamento = idFormaPagamento;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getNomeFormaPagamento() {
        return nomeFormaPagamento;
    }

    public void setNomeFormaPagamento(String nomeFormaPagamento) {
        this.nomeFormaPagamento = nomeFormaPagamento;
    }

    public int getIdFormaPagamento() {
        return idFormaPagamento;
    }

    public void setIdFormaPagamento(int idFormaPagamento) {
        this.idFormaPagamento = idFormaPagamento;
    }
}
