package unioeste.geral.bo.nota;

import unioeste.geral.bo.produto.Produto;

import java.io.Serializable;

public class ItemNotaVenda extends ItemNota implements Serializable {
	private static final long serialVersionUID = -8002852108750519471L;

    public ItemNotaVenda() {
    }

    public ItemNotaVenda(int idItemNota, double precoUnitario, int qtdItemNota, double totalItemNota, Produto produto,
                         NotaVenda notaVenda) {
        super(idItemNota, precoUnitario, qtdItemNota, totalItemNota, produto, notaVenda);
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
