USE endereco;

-- exclusão nessa ordem
DELETE FROM Endereco;
DELETE FROM Logradouro;
DELETE FROM TipoLogradouro;
DELETE FROM Bairro;
DELETE FROM Cidade;
DELETE FROM UnidadeFederativa;
DELETE FROM Pais;

-- inserção nessa ordem
INSERT INTO Pais              VALUES (1, "Brasil", "BR");
INSERT INTO UnidadeFederativa VALUES (1, "Paraná", "PR", 1);
INSERT INTO Cidade            VALUES (1, "Foz do Iguaçu", 1, 1);
INSERT INTO Bairro            VALUES (1, "Conjunto B");
INSERT INTO TipoLogradouro    VALUES (1, "Rua");
INSERT INTO TipoLogradouro    VALUES (2, "Avenida");
INSERT INTO TipoLogradouro    VALUES (3, "Alameda");
INSERT INTO Logradouro        VALUES (1, "Tancredo Neves", 2);
INSERT INTO Endereco          VALUES (1, 85867000, 1, 1, 1, 1, 1);
