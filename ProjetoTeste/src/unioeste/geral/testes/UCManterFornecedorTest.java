package unioeste.geral.testes;

import org.junit.Assert;
import org.junit.Test;
import unioeste.geral.bo.endereco.Endereco;
import unioeste.geral.bo.pessoa.CNPJ;
import unioeste.geral.bo.pessoa.EnderecoEspecifico;
import unioeste.geral.bo.pessoa.Fornecedor;
import unioeste.geral.endereco.exception.EnderecoException;
import unioeste.geral.endereco.manager.UCEnderecoGeralServicos;
import unioeste.geral.pessoa.manager.UCManterFornecedorBean;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class UCManterFornecedorTest {
	// Atributos do Fornecedor
	private final String nomeFantasia = (String) UUID.randomUUID().toString().subSequence(0, 8);
	private final String nomeRazaoSocial = (String) UUID.randomUUID().toString().subSequence(0, 8);
	private final long cnpj = ThreadLocalRandom.current().nextLong(90000000000000L, 100000000000000L);
	private final String nroEndereco = "123";
	private final String complementoEndereco = "complemento";
	private final String cep = "85867000";

	private final UCManterFornecedorBean ucManterFornecedorBean = new UCManterFornecedorBean();

	private void printarAtributos(Fornecedor fornecedor) {
		System.out.println("ID:             " + fornecedor.getIdFornecedor());
		System.out.println("Nome fantasia:  " + fornecedor.getNomeFantasia());
		System.out.println("Razão social:   " + fornecedor.getNomeRazaoSocial());
		System.out.println("CNPJ:           " + fornecedor.getCnpj().getNumeroCNPJ());
		System.out.println("CNPJ formatado: " + fornecedor.getCnpj().getNumeroCNPJFormatado());
		System.out.println("Nro. endereço:  " + fornecedor.getEnderecoPrincipal().getNroEndereco());
		System.out.println("Complemento:    " + fornecedor.getEnderecoPrincipal().getComplementoEndereco());
		System.out.println("CEP:            " + fornecedor.getEnderecoPrincipal().getEndereco().getCep());
	}

	public void assertFornecedor(Fornecedor fornecedor) {
		Fornecedor fornecedorObtido = new Fornecedor();
		try {
			fornecedorObtido = ucManterFornecedorBean.obterFornecedor(fornecedor);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Assert.assertEquals(fornecedor.getIdFornecedor(),
				fornecedorObtido.getIdFornecedor());
		Assert.assertEquals(fornecedor.getNomeFantasia(),
				fornecedorObtido.getNomeFantasia());
		Assert.assertEquals(fornecedor.getNomeRazaoSocial(),
				fornecedorObtido.getNomeRazaoSocial());
		Assert.assertEquals(fornecedor.getCnpj().getNumeroCNPJ(),
				fornecedorObtido.getCnpj().getNumeroCNPJ());
		Assert.assertEquals(fornecedor.getCnpj().getNumeroCNPJFormatado(),
				fornecedorObtido.getCnpj().getNumeroCNPJFormatado());
		Assert.assertEquals(fornecedor.getEnderecoPrincipal().getNroEndereco(),
				fornecedorObtido.getEnderecoPrincipal().getNroEndereco());
		Assert.assertEquals(fornecedor.getEnderecoPrincipal().getComplementoEndereco(),
				fornecedorObtido.getEnderecoPrincipal().getComplementoEndereco());
		Assert.assertEquals(fornecedor.getEnderecoPrincipal().getEndereco().getIdEndereco(),
				fornecedorObtido.getEnderecoPrincipal().getEndereco().getIdEndereco());
	}

	private Fornecedor criarFornecedor(String nomeFantasia, String nomeRazaoSocial, String cnpj, String nroEndereco,
									  String complementoEndereco, String cep) {
		// Busca endereço
		Endereco endereco = new Endereco();
		try {
			endereco = new UCEnderecoGeralServicos().obterEnderecoPorCEP(cep);
		} catch (EnderecoException e) {
			e.printStackTrace();
		}
		EnderecoEspecifico enderecoEspecifico = new EnderecoEspecifico(nroEndereco, complementoEndereco, endereco);
		CNPJ objCNPJ = new CNPJ(cnpj);
		return new Fornecedor(nomeFantasia, nomeRazaoSocial, objCNPJ, enderecoEspecifico);
	}

	@Test
	public void cadastrarFornecedorTest() throws Exception {
		// Cria objeto fornecedor
		Fornecedor fornecedor = criarFornecedor(nomeFantasia, nomeRazaoSocial, String.valueOf(cnpj),
				nroEndereco, complementoEndereco, cep);

		// Chama serviço de cadastrar fornecedor
		ucManterFornecedorBean.cadastrarFornecedor(fornecedor);
		assertFornecedor(fornecedor);
		printarAtributos(fornecedor);
	}

	@Test
	public void alterarFornecedorTest() throws Exception {
		// Obtém fornecedor
		Fornecedor fornecedor = new Fornecedor();
		fornecedor.setIdFornecedor(1);
		fornecedor = ucManterFornecedorBean.obterFornecedor(fornecedor);
		printarAtributos(fornecedor);

		// Altera fornecedor
		fornecedor.setNomeFantasia("nome fantasia alterado");
		fornecedor.setNomeRazaoSocial("nome razão social alterado");
		ucManterFornecedorBean.alterarFornecedor(fornecedor);
		assertFornecedor(fornecedor);
		System.out.println("\nAtributos alterados: ");
		printarAtributos(fornecedor);
	}

	@Test
	public void obterFornecedorTest() throws Exception {
		Fornecedor fornecedorObtido = new Fornecedor();
		fornecedorObtido.setIdFornecedor(1);
        fornecedorObtido = ucManterFornecedorBean.obterFornecedor(fornecedorObtido);
        printarAtributos(fornecedorObtido);
	}

	@Test
	public void obterTodosFornecedoresTest() throws Exception {
		List<Fornecedor> listaFornecedores = ucManterFornecedorBean.obterTodosFornecedores();
		for (Fornecedor f : listaFornecedores) {
			printarAtributos(f);
			System.out.println("----------------");
		}
	}

	@Test
	public void excluirFornecedorTest() throws Exception {
		// Obtém fornecedor
		Fornecedor fornecedor = new Fornecedor();
		fornecedor.setIdFornecedor(2);
		fornecedor = ucManterFornecedorBean.obterFornecedor(fornecedor);
		printarAtributos(fornecedor);

		ucManterFornecedorBean.excluirFornecedor(fornecedor);
	}

}
