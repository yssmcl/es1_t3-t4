package unioeste.geral.testes;

import org.junit.Assert;
import org.junit.Test;
import unioeste.geral.bo.nota.ItemNotaCompra;
import unioeste.geral.bo.nota.NotaCompra;
import unioeste.geral.bo.pessoa.Fornecedor;
import unioeste.geral.bo.produto.Produto;
import unioeste.geral.nota.manager.UCManterNotaCompra;
import unioeste.geral.pessoa.manager.UCManterFornecedorBean;
import unioeste.geral.produto.manager.UCManterProdutoImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class UCManterNotaCompraTest {
	private final UCManterNotaCompra ucManterNotaCompra = new UCManterNotaCompra();

	// Atributos da nota de compra
	private final Date dataEmissao = new Date(new java.util.Date().getTime());
	private double totalBruto = 0;
	private final double descontoTotal = new Random().nextFloat()*10;
	private final int idFornecedor = 1;

	// Funções auxiliares
	private void printarAtributos(NotaCompra notaCompra) {
		System.out.println("Nro.:            " + notaCompra.getNroNota());
		System.out.println("Data de emissão: " + notaCompra.getDataEmissaoNota());
		System.out.println("Total bruto:     " + notaCompra.getTotalBrutoNota());
		System.out.println("Desconto total:  " + notaCompra.getDescontoTotalNota());
		System.out.println("Valor líquido:   " + notaCompra.getValorLiquidoNota());
		System.out.println("Fornecedor:      " + notaCompra.getFornecedor().getCnpj().getNumeroCNPJFormatado());
		// TODO: printar lista de itens
	}

	private void assertNotaCompra(NotaCompra notaCompra) {
		UCManterFornecedorTest ucManterFornecedorTest = new UCManterFornecedorTest();
		NotaCompra notaCompraObtida = new NotaCompra();

		try {
            notaCompraObtida = ucManterNotaCompra.obterNotaCompra(notaCompra);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Assert.assertEquals(notaCompra.getNroNota(),
				notaCompraObtida.getNroNota());
		Assert.assertTrue(notaCompra.getDataEmissaoNota().toString()
				.equals(notaCompraObtida.getDataEmissaoNota().toString()));
		Assert.assertEquals(notaCompra.getTotalBrutoNota(),
				notaCompraObtida.getTotalBrutoNota(), 0.001);
		Assert.assertEquals(notaCompra.getDescontoTotalNota(),
				notaCompraObtida.getDescontoTotalNota(), 0.001);
		Assert.assertEquals(notaCompra.getValorLiquidoNota(),
				notaCompraObtida.getValorLiquidoNota(), 0.001);

		ucManterFornecedorTest.assertFornecedor(notaCompra.getFornecedor());
		ucManterFornecedorTest.assertFornecedor(notaCompraObtida.getFornecedor());

		Assert.assertEquals(notaCompra.getListaItensCompra().size(),
				notaCompraObtida.getListaItensCompra().size());
		for (int i = 0; i < notaCompra.getListaItensCompra().size(); i++) {
			Assert.assertEquals(notaCompra.getNroNota(),
					notaCompraObtida.getNroNota());
			Assert.assertTrue(notaCompra.getDataEmissaoNota().toString()
					.equals(notaCompraObtida.getDataEmissaoNota().toString()));
			Assert.assertEquals(notaCompra.getTotalBrutoNota(),
					notaCompraObtida.getTotalBrutoNota(), 0.001);
			Assert.assertEquals(notaCompra.getDescontoTotalNota(),
					notaCompraObtida.getDescontoTotalNota(), 0.001);
			Assert.assertEquals(notaCompra.getValorLiquidoNota(),
					notaCompraObtida.getValorLiquidoNota(), 0.001);

			Assert.assertEquals(notaCompra.getListaItensCompra().get(i).getIdItemNota(),
					notaCompraObtida.getListaItensCompra().get(i).getIdItemNota());
			Assert.assertEquals(notaCompra.getListaItensCompra().get(i).getPrecoUnitario(),
					notaCompraObtida.getListaItensCompra().get(i).getPrecoUnitario(), 0.001);
			Assert.assertEquals(notaCompra.getListaItensCompra().get(i).getQtdItemNota(),
					notaCompraObtida.getListaItensCompra().get(i).getQtdItemNota());
			Assert.assertEquals(notaCompra.getListaItensCompra().get(i).getTotalItemNota(),
					notaCompraObtida.getListaItensCompra().get(i).getTotalItemNota(), 0.001);
			Assert.assertEquals(notaCompra.getListaItensCompra().get(i).getTotalItemNota(),
					notaCompraObtida.getListaItensCompra().get(i).getTotalItemNota(), 0.001);

			Assert.assertEquals(notaCompra.getListaItensCompra().get(i).getProduto().getIdProduto(),
					notaCompraObtida.getListaItensCompra().get(i).getProduto().getIdProduto());
		}
	}

	private ItemNotaCompra criarItemNotaCompra(double precoUnitario, int qtdItemNota, int idProduto) throws Exception {
		Produto produto = new UCManterProdutoImpl().obterProduto(new Produto() {{
			setIdProduto(idProduto);
		}});

		ItemNotaCompra itemNotaCompra = new ItemNotaCompra();
		itemNotaCompra.setPrecoUnitario(precoUnitario);
		itemNotaCompra.setQtdItemNota(qtdItemNota);
		itemNotaCompra.setProduto(produto);

		return itemNotaCompra;
	}

	private List<ItemNotaCompra> criarListaItemNotaCompra(ItemNotaCompra... itemNotaCompra) {
		List<ItemNotaCompra> lista = new ArrayList<>();
		Collections.addAll(lista, itemNotaCompra);
		return lista;
	}

	// Funções de teste
	@Test
	public void cadastrarNotaCompraTest() throws Exception {
		// Cria objeto NotaCompra
		Fornecedor fornecedor = new UCManterFornecedorBean().obterFornecedor(new Fornecedor() {{
			setIdFornecedor(idFornecedor);
		}});

		// Cria itens da nota de compra
		ItemNotaCompra itemNotaCompra1 = criarItemNotaCompra(5.0, 2, 1);
		ItemNotaCompra itemNotaCompra2 = criarItemNotaCompra(15.0, 4, 2);

		List<ItemNotaCompra> lista = criarListaItemNotaCompra(itemNotaCompra1, itemNotaCompra2);

		for (ItemNotaCompra i : lista) {
			totalBruto += i.getPrecoUnitario();
			i.setTotalItemNota(i.getQtdItemNota() * i.getPrecoUnitario());
		}
		double valorLiquido = totalBruto - descontoTotal;

		NotaCompra notaCompra = new NotaCompra(dataEmissao, totalBruto, descontoTotal, valorLiquido, fornecedor, lista);

		for (ItemNotaCompra i : lista) {
			i.setNota(notaCompra);
		}

		// Chama serviço de cadastrar nota de compra
        ucManterNotaCompra.cadastrarNotaCompra(notaCompra);

        assertNotaCompra(notaCompra);
        printarAtributos(notaCompra);
	}


	@Test
	public void alterarNotaCompraTest() throws Exception {
		NotaCompra notaCompra = ucManterNotaCompra.obterNotaCompra(new NotaCompra() {{
			setNroNota(1);
		}});
		printarAtributos(notaCompra);

		// Altera produto
		notaCompra.setTotalBrutoNota(50);
		notaCompra = ucManterNotaCompra.alterarNotaCompra(notaCompra);
		System.out.println("\nAtributos alterados: ");
		printarAtributos(notaCompra);
		assertNotaCompra(notaCompra);
	}

	@Test
	public void obterNotaCompraTest() throws Exception {
		NotaCompra notaCompraObtida = ucManterNotaCompra.obterNotaCompra(new NotaCompra() {{
			setNroNota(1);
		}});
		assertNotaCompra(notaCompraObtida);
		printarAtributos(notaCompraObtida);
	}

	@Test
	public void obterTodosNotaCompraTest() throws Exception {
		List<NotaCompra> lista = ucManterNotaCompra.obterTodosNotaCompra();
		for (NotaCompra notaCompra : lista) {
			printarAtributos(notaCompra);
			System.out.println("----------------");
		}
	}

	@Test
	public void excluirNotaCompraTest() throws Exception {
		int nroNota = 1;

		NotaCompra notaCompra = ucManterNotaCompra.obterNotaCompra(new NotaCompra() {{
			setNroNota(nroNota);
		}});

		ucManterNotaCompra.excluirNotaCompra(notaCompra);

		NotaCompra notaCompraObtida = ucManterNotaCompra.obterNotaCompra(new NotaCompra() {{
			setNroNota(nroNota);
		}});
		printarAtributos(notaCompraObtida);
	}

}
