package unioeste.geral.testes;

import org.junit.Assert;
import org.junit.Test;
import unioeste.geral.bo.endereco.Endereco;
import unioeste.geral.bo.pessoa.*;
import unioeste.geral.endereco.exception.EnderecoException;
import unioeste.geral.endereco.manager.UCEnderecoGeralServicos;
import unioeste.geral.pessoa.manager.UCManterCliente;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class UCManterClienteTest {
	// Atributos do cliente
	private final String nomeCompleto = (String) UUID.randomUUID().toString().replaceAll("[\\d-]", "");
	private final long cpf = ThreadLocalRandom.current().nextLong(90000000000L, 100000000000L);
	private final String nomeSexo = "MASCULINO";
	private final String nomeFantasia = (String) UUID.randomUUID().toString().subSequence(0, 8);
	private final String nomeRazaoSocial = (String) UUID.randomUUID().toString().subSequence(0, 8);
	private final long cnpj = ThreadLocalRandom.current().nextLong(90000000000000L, 100000000000000L);
	private final String nroEndereco = "123";
	private final String complementoEndereco = "complemento";
	private final String idEndereco = "1";

	private final UCManterCliente ucManterCliente = new UCManterCliente();

	private List<Email> criarListaEmail(String... enderecoEmail) {
		List<Email> listaEmails = new ArrayList<>();
		for (String endereco : enderecoEmail){
			Email email = new Email();
			email.setEnderecoEmail(endereco);
			listaEmails.add(email);
		}
		return listaEmails;
	}

//	private List<Telefone> criarListaTelefone(int... numeroTelefone) {
//		List<Telefone> listaTelefones = new ArrayList<>();
//		for (int numero : numeroTelefone){
//			Telefone telefone = new Telefone();
//			telefone.setNumeroTelefone(numero);
//			listaTelefones.add(telefone);
//		}
//		return listaTelefones;
//	}

	private List<Telefone> criarListaTelefone(Telefone... telefone) {
		List<Telefone> listaTelefones = new ArrayList<>();
		Collections.addAll(listaTelefones, telefone);
		return listaTelefones;
	}

	// E-mails
	private final List<Email> listaEmails = criarListaEmail(
			UUID.randomUUID().toString().subSequence(0, 4) + "@" + UUID.randomUUID().toString().subSequence(4, 8) + ".com",
			UUID.randomUUID().toString().subSequence(0, 4) + "@" + UUID.randomUUID().toString().subSequence(4, 8) + ".com");

	// Telefones
	private final TipoTelefone tipoCelular = new TipoTelefone(1, "celular");
	private final TipoTelefone tipoResidencial = new TipoTelefone(2, "residencial");
	private final TipoTelefone tipoComercial = new TipoTelefone(3, "celular");
	private final DDD dddPR = new DDD(new Random().nextInt(100));
	private final Telefone telefoneCelular = new Telefone(new Random().nextInt(90000000)+100000000, dddPR, tipoCelular);
	private final Telefone telefoneResidencial = new Telefone(new Random().nextInt(90000000)+10000000, dddPR, tipoResidencial);
	private final Telefone telefoneComercial = new Telefone(new Random().nextInt(90000000)+10000000, dddPR, tipoComercial);
	private final List<Telefone> listaTelefones = criarListaTelefone(telefoneCelular, telefoneResidencial, telefoneComercial);

	private void printarAtributos(Cliente cliente) {
		System.out.println("ID:              " + cliente.getIdCliente());
        if (cliente.isPessoaFisica()) {
            System.out.println("Nome completo:   " + cliente.getNomePessoa().getNomeCompleto());
            System.out.println("CPF:             " + cliente.getCpf().getNumeroCPF());
            System.out.println("CPF formatado:   " + cliente.getCpf().getNumeroCPFFormatado());
            System.out.println("Sigla do sexo:   " + cliente.getSexo().getSiglaSexo());
            System.out.println("Nome do sexo:    " + cliente.getSexo().getNomeSexo());
		} else {
			System.out.println("Nome fantasia:   " + cliente.getNomeFantasia());
			System.out.println("Razão social:    " + cliente.getNomeRazaoSocial());
			System.out.println("CNPJ:            " + cliente.getCnpj().getNumeroCNPJ());
			System.out.println("CNPJ formatado:  " + cliente.getCnpj().getNumeroCNPJFormatado());
			System.out.println("Pessoa jurídica: " + cliente.isPessoaJuridica());
			System.out.println("Pessoa física:   " + cliente.isPessoaFisica());
			System.out.println("Nro. endereço:   " + cliente.getEnderecoPrincipal().getNroEndereco());
			System.out.println("Complemento:     " + cliente.getEnderecoPrincipal().getComplementoEndereco());
			System.out.println("CEP:             " + cliente.getEnderecoPrincipal().getEndereco().getCep());
		}

		System.out.print("E-mails:         ");
		for (Email e : cliente.getListaEmails()) {
			System.out.print(e.getEnderecoEmail() + "; ");
		}
		System.out.println();

		System.out.print("Telefones:       ");
		for (Telefone t : cliente.getListaTelefones()) {
			System.out.print(t.getNumeroTelefone() + "; ");
		}
		System.out.println();
	}

	public void assertCliente(Cliente cliente) {
		Cliente clienteObtido = new Cliente();
		try {
			clienteObtido = ucManterCliente.obterCliente(cliente);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (cliente.isPessoaFisica()) {
			Assert.assertEquals(cliente.getIdCliente(),
					clienteObtido.getIdCliente());
			Assert.assertEquals(cliente.getNomePessoa().getNomeCompleto(),
					clienteObtido.getNomePessoa().getNomeCompleto());
			Assert.assertEquals(cliente.getCpf().getNumeroCPF(),
					clienteObtido.getCpf().getNumeroCPF());
			Assert.assertEquals(cliente.getCpf().getNumeroCPFFormatado(),
					clienteObtido.getCpf().getNumeroCPFFormatado());
			Assert.assertEquals(cliente.getSexo().getSiglaSexo(),
					clienteObtido.getSexo().getSiglaSexo());
			Assert.assertEquals(cliente.getSexo().getNomeSexo(),
					clienteObtido.getSexo().getNomeSexo());
		} else {
			Assert.assertEquals(cliente.getNomeFantasia(),
					clienteObtido.getNomeFantasia());
			Assert.assertEquals(cliente.getNomeRazaoSocial(),
					clienteObtido.getNomeRazaoSocial());
			Assert.assertEquals(cliente.getCnpj().getNumeroCNPJ(),
					clienteObtido.getCnpj().getNumeroCNPJ());
			Assert.assertEquals(cliente.getCnpj().getNumeroCNPJFormatado(),
					clienteObtido.getCnpj().getNumeroCNPJFormatado());
		}
		Assert.assertEquals(cliente.getEnderecoPrincipal().getNroEndereco(),
				clienteObtido.getEnderecoPrincipal().getNroEndereco());
		Assert.assertEquals(cliente.getEnderecoPrincipal().getComplementoEndereco(),
				clienteObtido.getEnderecoPrincipal().getComplementoEndereco());
		Assert.assertEquals(cliente.getEnderecoPrincipal().getEndereco().getIdEndereco(),
				clienteObtido.getEnderecoPrincipal().getEndereco().getIdEndereco());
	}

	private Cliente criarClientePJ(List<Email> listaEmails, List<Telefone> listaTelefones, String nroEndereco,
								  String complementoEndereco, String idEndereco, String nomeFantasia,
								  String nomeRazaoSocial, String cnpj) {
		// Busca endereço
		Endereco endereco = new Endereco();
		endereco.setIdEndereco(Integer.parseInt(idEndereco));
		try {
			endereco = new UCEnderecoGeralServicos().obterEnderecoPorID(endereco);
		} catch (EnderecoException e) {
			e.printStackTrace();
		}
		EnderecoEspecifico enderecoEspecifico = new EnderecoEspecifico(nroEndereco, complementoEndereco, endereco);
		CNPJ objCNPJ = new CNPJ(cnpj);
		return new Cliente(listaEmails, listaTelefones, enderecoEspecifico, nomeFantasia, nomeRazaoSocial, objCNPJ);
	}

	private Cliente criarClientePF(List<Email> listaEmails, List<Telefone> listaTelefones, String nroEndereco,
								  String complementoEndereco, String idEndereco, String sexo, String cpf,
								  String nomeCompleto) {
		// Busca endereço
		Endereco endereco = new Endereco();
		endereco.setIdEndereco(Integer.parseInt(idEndereco));
		try {
			endereco = new UCEnderecoGeralServicos().obterEnderecoPorID(endereco);
		} catch (EnderecoException e) {
			e.printStackTrace();
		}
		EnderecoEspecifico enderecoEspecifico = new EnderecoEspecifico(nroEndereco, complementoEndereco, endereco);
		Sexo objSexo = new Sexo(sexo);
		CPF objCPF = new CPF(cpf);
		NomePessoa nomePessoa = new NomePessoa(nomeCompleto);
		return new Cliente(listaEmails, listaTelefones, enderecoEspecifico, objSexo, objCPF, nomePessoa);
	}

	@Test
	public void cadastrarClientePJTest() throws Exception {
		// Cria objeto cliente
		Cliente cliente = criarClientePJ(listaEmails, listaTelefones, nroEndereco, complementoEndereco,
				idEndereco, nomeFantasia, nomeRazaoSocial, String.valueOf(cnpj));

		// Chama serviço de cadastrar cliente
		ucManterCliente.cadastrarCliente(cliente);
		assertCliente(cliente);
		printarAtributos(cliente);
	}

	@Test
	public void cadastrarClientePFTest() throws Exception {
		// Cria objeto cliente
		Cliente cliente = criarClientePF(listaEmails, listaTelefones, "123", complementoEndereco,
				"1", nomeSexo, String.valueOf(cpf), nomeCompleto);

		// Chama serviço de cadastrar cliente
		ucManterCliente.cadastrarCliente(cliente);
		assertCliente(cliente);
		printarAtributos(cliente);
	}

	@Test
	public void alterarClienteTest() throws Exception {
		// Obtém cliente
		Cliente cliente = new Cliente();
		cliente.setIdCliente(1);
		cliente = ucManterCliente.obterCliente(cliente);
		printarAtributos(cliente);

		// Altera fornecedor
		NomePessoa nomePessoa = new NomePessoa("nome completo alterado");
		cliente.setNomePessoa(nomePessoa);
		ucManterCliente.alterarCliente(cliente);
		assertCliente(cliente);
		System.out.println("\nAtributos alterados: ");
		printarAtributos(cliente);
	}

	@Test
	public void obterClienteTest() throws Exception {
		Cliente clienteObtido = new Cliente();
		clienteObtido.setIdCliente(1);
        clienteObtido = ucManterCliente.obterCliente(clienteObtido);
        printarAtributos(clienteObtido);
	}

	@Test
	public void obterTodosClientesTest() throws Exception {
		List<Cliente> listaClientes = ucManterCliente.obterTodosClientes();
		for (Cliente c : listaClientes) {
			printarAtributos(c);
			System.out.println("----------------");
		}
	}

	@Test
	public void excluirClienteTest() throws Exception {
		// Obtém cliente
		Cliente cliente = new Cliente();
		cliente.setIdCliente(2);
		cliente = ucManterCliente.obterCliente(cliente);
		printarAtributos(cliente);

		ucManterCliente.excluirCliente(cliente);
	}

}
