package unioeste.geral.testes;

import org.junit.Assert;
import org.junit.Test;
import unioeste.geral.bo.pessoa.Fornecedor;
import unioeste.geral.bo.produto.Produto;
import unioeste.geral.bo.produto.TipoProduto;
import unioeste.geral.pessoa.manager.FornecedorControle;
import unioeste.geral.produto.manager.TipoProdutoControle;
import unioeste.geral.produto.manager.UCManterProduto;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class UCManterProdutoTest {
	// Atributos do Produto
	private final long codigoBarras = new Random().nextInt(900000000)+100000000;
	private final String nomeProduto = (String) UUID.randomUUID().toString().subSequence(0, 8);
	private final double precoCusto = new Random().nextFloat()*1000;
	private final double precoVenda = new Random().nextFloat()*1000;
	private final int qtd = new Random().nextInt(100);
	private final int idTipoProduto = 1;
	private final int idFornecedor = 1;


	private void printarAtributos(Produto produto) {
		System.out.println("ID:               " + produto.getIdProduto());
		System.out.println("Código de barras: " + produto.getCodigoBarrasProduto());
		System.out.println("Nome do produto:  " + produto.getNomeProduto());
		System.out.println("Preço de custo:   " + produto.getPrecoCustoProduto());
		System.out.println("Preço de venda:   " + produto.getPrecoVendaProduto());
		System.out.println("Qtd. do produto:  " + produto.getQtdProduto());
		System.out.println("Tipo do produto:  " + produto.getTipoProduto().getNomeTipoProduto());
		System.out.println("Fornecedor:       " + produto.getFornecedor().getNomeFantasia());
	}

	public void assertProduto(Produto produto) {
		UCManterProduto ucManterProduto = obterRecurso();
		Produto produtoObtido = new Produto();
		try {
			produtoObtido = ucManterProduto.obterProduto(produto);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Assert.assertEquals(produto.getIdProduto(),
				produtoObtido.getIdProduto());
		Assert.assertEquals(produto.getCodigoBarrasProduto(),
				produtoObtido.getCodigoBarrasProduto());
		Assert.assertEquals(produto.getNomeProduto(),
				produtoObtido.getNomeProduto());
		Assert.assertEquals(produto.getPrecoCustoProduto(),
				produtoObtido.getPrecoCustoProduto(), 0.001);
		Assert.assertEquals(produto.getPrecoVendaProduto(),
				produtoObtido.getPrecoVendaProduto(), 0.001);
		Assert.assertEquals(produto.getQtdProduto(),
				produtoObtido.getQtdProduto());
		Assert.assertEquals(produto.getTipoProduto().getNomeTipoProduto(),
				produtoObtido.getTipoProduto().getNomeTipoProduto());
		new UCManterFornecedorTest().assertFornecedor(produto.getFornecedor());
	}

	private Produto criarProduto(long codigoBarrasProduto, String nomeProduto, double precoCustoProduto,
								 double precoVendaProduto, int qtdProduto, int idTipoProduto, int idFornecedor) {
		TipoProduto tipoProduto = new TipoProduto();
		tipoProduto.setIdTipoProduto(idTipoProduto);
		Fornecedor fornecedor = new Fornecedor();
		fornecedor.setIdFornecedor(idFornecedor);
		try {
			tipoProduto = new TipoProdutoControle().recuperarTipoProdutoPorPK(tipoProduto);
			fornecedor = new FornecedorControle().recuperarFornecedorPorPK(fornecedor);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new Produto(codigoBarrasProduto, nomeProduto, precoCustoProduto, precoVendaProduto, qtdProduto,
				tipoProduto, fornecedor);
	}

	private UCManterProduto obterRecurso() {
		URL url = null;
		try {
			// JBoss:
			url = new URL("http://localhost:8080/CamadaWeb/UCManterProdutoImpl?wsdl");
			// GlassFish:
//			url = new URL("http://localhost:8080/CamadaWeb/UCManterProdutoImplService?wsdl");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		QName qName = new QName("http://manager.produto.geral.unioeste/", "UCManterProdutoImplService");
		Service service = Service.create(url, qName);
		return service.getPort(UCManterProduto.class);
	}

	@Test
	public void cadastrarProdutoTest() throws Exception {
		UCManterProduto ucManterProduto = obterRecurso();
		// Cria objeto produto
		Produto produto = criarProduto(codigoBarras, nomeProduto, precoCusto, precoVenda, qtd, idTipoProduto, idFornecedor);

		// Chama serviço de cadastrar produto
		produto = ucManterProduto.cadastrarProduto(produto);
		assertProduto(produto);
		printarAtributos(produto);
	}

	@Test
	public void alterarProdutoTest() throws Exception {
		UCManterProduto ucManterProduto = obterRecurso();
		// Obtém produto
        Produto produto = new Produto();
		produto.setIdProduto(1);
		produto = ucManterProduto.obterProduto(produto);
        printarAtributos(produto);

		// Altera produto
		produto.setCodigoBarrasProduto(123456789);
		produto.setNomeProduto("nome do produto alterado");
		ucManterProduto.alterarProduto(produto);
		assertProduto(produto);
		System.out.println("\nAtributos alterados: ");
		printarAtributos(produto);
	}

	@Test
	public void obterProdutoTest() throws Exception {
		UCManterProduto ucManterProduto = obterRecurso();
		Produto produtoObtido = new Produto();
		produtoObtido.setIdProduto(2);
        produtoObtido = ucManterProduto.obterProduto(produtoObtido);
        printarAtributos(produtoObtido);
	}

	@Test
	public void obterTodosProdutosTest() throws Exception {
		UCManterProduto ucManterProduto = obterRecurso();
		List<Produto> listaProdutoes = ucManterProduto.obterTodosProdutos();
		for (Produto f : listaProdutoes) {
			printarAtributos(f);
			System.out.println("----------------");
		}
	}

	@Test
	public void excluirProdutoTest() throws Exception {
		UCManterProduto ucManterProduto = obterRecurso();
		// Obtém produto
		Produto produto = new Produto();
		produto.setIdProduto(2);
		produto = ucManterProduto.obterProduto(produto);
		printarAtributos(produto);

		ucManterProduto.excluirProduto(produto);
	}

}
