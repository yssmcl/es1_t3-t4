package unioeste.geral.testes;

import org.junit.Assert;
import org.junit.Test;
import unioeste.geral.bo.nota.FormaPagamento;
import unioeste.geral.bo.nota.ItemNotaVenda;
import unioeste.geral.bo.nota.NotaVenda;
import unioeste.geral.bo.pessoa.Cliente;
import unioeste.geral.bo.produto.Produto;
import unioeste.geral.nota.manager.FormaPagamentoControle;
import unioeste.geral.nota.manager.UCManterNotaVenda;
import unioeste.geral.pessoa.manager.UCManterCliente;
import unioeste.geral.produto.manager.UCManterProdutoImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class UCManterNotaVendaTest {
	private final UCManterNotaVenda ucManterNotaVenda = new UCManterNotaVenda();

	// Atributos da nota de compra
	private final Date dataEmissao = new Date(new java.util.Date().getTime());
	private double totalBruto = 0;
	private final double descontoTotal = new Random().nextFloat()*10;
	private final int idCliente = 1;

	// Funções auxiliares
	private void printarAtributos(NotaVenda notaVenda) {
		System.out.println("Nro.:            " + notaVenda.getNroNota());
		System.out.println("Data de emissão: " + notaVenda.getDataEmissaoNota());
		System.out.println("Total bruto:     " + notaVenda.getTotalBrutoNota());
		System.out.println("Desconto total:  " + notaVenda.getDescontoTotalNota());
		System.out.println("Valor líquido:   " + notaVenda.getValorLiquidoNota());
		if (notaVenda.getCliente().isPessoaFisica()) {
			System.out.println("Cliente:         " + notaVenda.getCliente().getCpf().getNumeroCPFFormatado());
		} else {
			System.out.println("Cliente:         " + notaVenda.getCliente().getCnpj().getNumeroCNPJFormatado());
		}
		// TODO: printar lista de itens
	}

	private void assertNotaVenda(NotaVenda notaVenda) {
		UCManterClienteTest ucManterClienteTest = new UCManterClienteTest();
		NotaVenda notaVendaObtida = new NotaVenda();

		try {
            notaVendaObtida = ucManterNotaVenda.obterNotaVenda(notaVenda);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Assert.assertEquals(notaVenda.getNroNota(),
				notaVendaObtida.getNroNota());
		Assert.assertTrue(notaVenda.getDataEmissaoNota().toString()
				.equals(notaVendaObtida.getDataEmissaoNota().toString()));
		Assert.assertEquals(notaVenda.getTotalBrutoNota(),
				notaVendaObtida.getTotalBrutoNota(), 0.001);
		Assert.assertEquals(notaVenda.getDescontoTotalNota(),
				notaVendaObtida.getDescontoTotalNota(), 0.001);
		Assert.assertEquals(notaVenda.getValorLiquidoNota(),
				notaVendaObtida.getValorLiquidoNota(), 0.001);

		ucManterClienteTest.assertCliente(notaVenda.getCliente());
		ucManterClienteTest.assertCliente(notaVendaObtida.getCliente());

		Assert.assertEquals(notaVenda.getListaItensVenda().size(),
				notaVendaObtida.getListaItensVenda().size());
		for (int i = 0; i < notaVenda.getListaItensVenda().size(); i++) {
			Assert.assertEquals(notaVenda.getNroNota(),
					notaVendaObtida.getNroNota());
			Assert.assertTrue(notaVenda.getDataEmissaoNota().toString()
					.equals(notaVendaObtida.getDataEmissaoNota().toString()));
			Assert.assertEquals(notaVenda.getTotalBrutoNota(),
					notaVendaObtida.getTotalBrutoNota(), 0.001);
			Assert.assertEquals(notaVenda.getDescontoTotalNota(),
					notaVendaObtida.getDescontoTotalNota(), 0.001);
			Assert.assertEquals(notaVenda.getValorLiquidoNota(),
					notaVendaObtida.getValorLiquidoNota(), 0.001);

			Assert.assertEquals(notaVenda.getListaItensVenda().get(i).getIdItemNota(),
					notaVendaObtida.getListaItensVenda().get(i).getIdItemNota());
			Assert.assertEquals(notaVenda.getListaItensVenda().get(i).getPrecoUnitario(),
					notaVendaObtida.getListaItensVenda().get(i).getPrecoUnitario(), 0.001);
			Assert.assertEquals(notaVenda.getListaItensVenda().get(i).getQtdItemNota(),
					notaVendaObtida.getListaItensVenda().get(i).getQtdItemNota());
			Assert.assertEquals(notaVenda.getListaItensVenda().get(i).getTotalItemNota(),
					notaVendaObtida.getListaItensVenda().get(i).getTotalItemNota(), 0.001);
			Assert.assertEquals(notaVenda.getListaItensVenda().get(i).getTotalItemNota(),
					notaVendaObtida.getListaItensVenda().get(i).getTotalItemNota(), 0.001);

			Assert.assertEquals(notaVenda.getListaItensVenda().get(i).getProduto().getIdProduto(),
					notaVendaObtida.getListaItensVenda().get(i).getProduto().getIdProduto());
		}
	}

	private ItemNotaVenda criarItemNotaVenda(double precoUnitario, int qtdItemNota, int idProduto) throws Exception {
		Produto produto = new UCManterProdutoImpl().obterProduto(new Produto() {{
			setIdProduto(idProduto);
		}});

		ItemNotaVenda itemNotaVenda = new ItemNotaVenda();
		itemNotaVenda.setPrecoUnitario(precoUnitario);
		itemNotaVenda.setQtdItemNota(qtdItemNota);
		itemNotaVenda.setProduto(produto);

		return itemNotaVenda;
	}

	public List<ItemNotaVenda> criarListaItemNotaVenda(ItemNotaVenda... itemNotaVenda) {
		List<ItemNotaVenda> lista = new ArrayList<>();
		Collections.addAll(lista, itemNotaVenda);
		return lista;
	}

	// Funções de teste
	@Test
	public void cadastrarNotaVendaTest() throws Exception {
		// Cria objeto NotaVenda
		Cliente cliente = new UCManterCliente().obterCliente(new Cliente() {{
			setIdCliente(idCliente);
		}});
		FormaPagamento formaPagamento = new FormaPagamentoControle().recuperarFormaPagamentoPorPK(new FormaPagamento() {{
			setIdFormaPagamento(1);
		}});

		// Cria itens da nota de compra
		ItemNotaVenda itemNotaVenda1 = criarItemNotaVenda(5.0, 2, 1);
		ItemNotaVenda itemNotaVenda2 = criarItemNotaVenda(15.0, 4, 2);

		List<ItemNotaVenda> lista = criarListaItemNotaVenda(itemNotaVenda1, itemNotaVenda2);

		for (ItemNotaVenda i : lista) {
			totalBruto += i.getPrecoUnitario();
			i.setTotalItemNota(i.getQtdItemNota() * i.getPrecoUnitario());
		}
		double valorLiquido = totalBruto - descontoTotal;

		NotaVenda notaVenda = new NotaVenda(dataEmissao, totalBruto, descontoTotal, valorLiquido, cliente, lista, formaPagamento);

		for (ItemNotaVenda i : lista) {
			i.setNota(notaVenda);
		}

		// Chama serviço de cadastrar nota de compra
        ucManterNotaVenda.cadastrarNotaVenda(notaVenda);

        assertNotaVenda(notaVenda);
        printarAtributos(notaVenda);
	}

	@Test
	public void alterarNotaVendaTest() throws Exception {
		NotaVenda notaVenda = ucManterNotaVenda.obterNotaVenda(new NotaVenda() {{
			setNroNota(1);
		}});
		printarAtributos(notaVenda);

		// Altera produto
		notaVenda.setTotalBrutoNota(50);
		notaVenda = ucManterNotaVenda.alterarNotaVenda(notaVenda);
		System.out.println("\nAtributos alterados: ");
		printarAtributos(notaVenda);
		assertNotaVenda(notaVenda);
	}

	@Test
	public void obterNotaVendaTest() throws Exception {
		NotaVenda notaVendaObtida = ucManterNotaVenda.obterNotaVenda(new NotaVenda() {{
			setNroNota(1);
		}});
		assertNotaVenda(notaVendaObtida);
		printarAtributos(notaVendaObtida);
	}

	@Test
	public void obterTodosNotaVendaTest() throws Exception {
		List<NotaVenda> lista = ucManterNotaVenda.obterTodosNotaVenda();
		for (NotaVenda notaVenda : lista) {
			printarAtributos(notaVenda);
			System.out.println("----------------");
		}
	}

	@Test
	public void excluirNotaVendaTest() throws Exception {
		int nroNota = 1;

		NotaVenda notaVenda = ucManterNotaVenda.obterNotaVenda(new NotaVenda() {{
			setNroNota(nroNota);
		}});

		ucManterNotaVenda.excluirNotaVenda(notaVenda);

		NotaVenda notaVendaObtida = ucManterNotaVenda.obterNotaVenda(new NotaVenda() {{
			setNroNota(nroNota);
		}});
		printarAtributos(notaVendaObtida);
	}

}
