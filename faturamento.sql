-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 27-Nov-2016 às 00:43
-- Versão do servidor: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `faturamento`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `idCliente` int(11) NOT NULL,
  `nomeCliente` varchar(45) DEFAULT NULL,
  `cpfCliente` varchar(15) DEFAULT NULL,
  `sexoCliente` char(1) DEFAULT NULL,
  `nomeFantasiaCliente` varchar(45) DEFAULT NULL,
  `nomeRazaoSocialCliente` varchar(45) DEFAULT NULL,
  `cnpjCliente` varchar(18) DEFAULT NULL,
  `ehPessoaFisica` tinyint(1) NOT NULL,
  `ehPessoaJuridica` tinyint(1) NOT NULL,
  `nroEnderecoCliente` varchar(11) DEFAULT NULL,
  `complementoEnderecoCliente` varchar(45) DEFAULT NULL,
  `Endereco_idEndereco` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`idCliente`, `nomeCliente`, `cpfCliente`, `sexoCliente`, `nomeFantasiaCliente`, `nomeRazaoSocialCliente`, `cnpjCliente`, `ehPessoaFisica`, `ehPessoaJuridica`, `nroEnderecoCliente`, `complementoEnderecoCliente`, `Endereco_idEndereco`) VALUES
(1, 'nome completo', '123.456.789-00', 'M', NULL, NULL, NULL, 1, 0, '123', 'complemento', 1),
(2, NULL, NULL, NULL, 'nome fantasia', 'nome razão social', '12.345.678/1234-56', 0, 1, '123', 'complemento', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `emailcliente`
--

CREATE TABLE IF NOT EXISTS `emailcliente` (
  `enderecoEmailCliente` varchar(45) NOT NULL,
  `Cliente_idCliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `emailcliente`
--

INSERT INTO `emailcliente` (`enderecoEmailCliente`, `Cliente_idCliente`) VALUES
('pf2@email.com', 1),
('pf@email.com', 1),
('pj2@email.com', 2),
('pj@email.com', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `formapagamento`
--

CREATE TABLE IF NOT EXISTS `formapagamento` (
  `idFormaPagamento` int(11) NOT NULL,
  `nomeFormaPagamento` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `formapagamento`
--

INSERT INTO `formapagamento` (`idFormaPagamento`, `nomeFormaPagamento`) VALUES
(1, 'boleto bancário'),
(2, 'cartão de crédito');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fornecedor`
--

CREATE TABLE IF NOT EXISTS `fornecedor` (
  `idFornecedor` int(11) NOT NULL,
  `nomeFantasiaFornecedor` varchar(45) DEFAULT NULL,
  `nomeRazaoSocialFornecedor` varchar(45) DEFAULT NULL,
  `cnpjFornecedor` varchar(18) DEFAULT NULL,
  `nroEnderecoFornecedor` varchar(11) DEFAULT NULL,
  `complementoEnderecoFornecedor` varchar(45) DEFAULT NULL,
  `Endereco_idEndereco` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `fornecedor`
--

INSERT INTO `fornecedor` (`idFornecedor`, `nomeFantasiaFornecedor`, `nomeRazaoSocialFornecedor`, `cnpjFornecedor`, `nroEnderecoFornecedor`, `complementoEnderecoFornecedor`, `Endereco_idEndereco`) VALUES
(1, 'nome fantasia', 'nome razão social', '12.345.678/1234-56', '123', 'complemento', 1),
(2, 'nome fantasia2', 'nome razão social2', '12.345.678/1234-57', '123', 'complemento', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `itemnotacompra`
--

CREATE TABLE IF NOT EXISTS `itemnotacompra` (
  `idItemNotaCompra` int(11) NOT NULL,
  `precoUnitarioItemNotaCompra` double DEFAULT NULL,
  `qtdItemNotaCompra` int(11) DEFAULT NULL,
  `totalItemNotaCompra` double DEFAULT NULL,
  `Produto_idProduto` int(11) NOT NULL,
  `NotaCompra_nroNotaCompra` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `itemnotacompra`
--

INSERT INTO `itemnotacompra` (`idItemNotaCompra`, `precoUnitarioItemNotaCompra`, `qtdItemNotaCompra`, `totalItemNotaCompra`, `Produto_idProduto`, `NotaCompra_nroNotaCompra`) VALUES
(1, 2, 5, 10, 1, 1),
(2, 3, 2, 6, 2, 1),
(3, 10.5, 1, 10.5, 1, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `itemnotavenda`
--

CREATE TABLE IF NOT EXISTS `itemnotavenda` (
  `idItemNotaVenda` int(11) NOT NULL,
  `precoUnitarioItemNotaVenda` double DEFAULT NULL,
  `qtdItemNotaVenda` int(11) DEFAULT NULL,
  `totalItemNotaVenda` double DEFAULT NULL,
  `FormaPagamento_idFormaPagamento` int(11) NOT NULL,
  `Produto_idProduto` int(11) NOT NULL,
  `NotaVenda_nroNotaVenda` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `itemnotavenda`
--

INSERT INTO `itemnotavenda` (`idItemNotaVenda`, `precoUnitarioItemNotaVenda`, `qtdItemNotaVenda`, `totalItemNotaVenda`, `FormaPagamento_idFormaPagamento`, `Produto_idProduto`, `NotaVenda_nroNotaVenda`) VALUES
(1, 2, 5, 10, 2, 1, 1),
(2, 3, 2, 6, 2, 2, 2),
(3, 10.5, 1, 10.5, 1, 1, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `notacompra`
--

CREATE TABLE IF NOT EXISTS `notacompra` (
  `nroNotaCompra` int(11) NOT NULL,
  `dataEmissaoNotaCompra` date DEFAULT NULL,
  `totalBrutoNotaCompra` double DEFAULT NULL,
  `descontoTotalNotaCompra` double DEFAULT NULL,
  `Fornecedor_idFornecedor` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `notacompra`
--

INSERT INTO `notacompra` (`nroNotaCompra`, `dataEmissaoNotaCompra`, `totalBrutoNotaCompra`, `descontoTotalNotaCompra`, `Fornecedor_idFornecedor`) VALUES
(1, '2016-10-10', 16, 5, 1),
(2, '2016-12-12', 10.5, 0, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `notavenda`
--

CREATE TABLE IF NOT EXISTS `notavenda` (
  `nroNotaVenda` int(11) NOT NULL,
  `dataEmissaoNotaVenda` date DEFAULT NULL,
  `totalBrutoNotaVenda` double DEFAULT NULL,
  `descontoTotalNotaVenda` double DEFAULT NULL,
  `Cliente_idCliente` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `notavenda`
--

INSERT INTO `notavenda` (`nroNotaVenda`, `dataEmissaoNotaVenda`, `totalBrutoNotaVenda`, `descontoTotalNotaVenda`, `Cliente_idCliente`) VALUES
(1, '2016-06-06', 20, 2, 1),
(2, '2016-08-08', 15, 1, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE IF NOT EXISTS `produto` (
  `idProduto` int(11) NOT NULL,
  `codigoBarrasProduto` bigint(20) DEFAULT NULL,
  `nomeProduto` varchar(45) DEFAULT NULL,
  `precoCustoProduto` double DEFAULT NULL,
  `precoVendaProduto` double DEFAULT NULL,
  `qtdProduto` int(11) DEFAULT NULL,
  `TipoProduto_idTipoProduto` int(11) NOT NULL,
  `Fornecedor_idFornecedor` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`idProduto`, `codigoBarrasProduto`, `nomeProduto`, `precoCustoProduto`, `precoVendaProduto`, `qtdProduto`, `TipoProduto_idTipoProduto`, `Fornecedor_idFornecedor`) VALUES
(1, 1554565987945, 'nome produto', 15.5, 25.5, 132, 1, 1),
(2, 4564754787876, 'nome produto2', 48.5, 120.5, 240, 2, 1),
(3, 1238724788238, 'nome produto3', 60, 80, 50, 1, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `telefonecliente`
--

CREATE TABLE IF NOT EXISTS `telefonecliente` (
  `numeroTelefoneCliente` int(11) NOT NULL,
  `dddTelefoneCliente` int(11) NOT NULL,
  `TipoTelefone_idTipoTelefone` int(11) NOT NULL,
  `Cliente_idCliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `telefonecliente`
--

INSERT INTO `telefonecliente` (`numeroTelefoneCliente`, `dddTelefoneCliente`, `TipoTelefone_idTipoTelefone`, `Cliente_idCliente`) VALUES
(98754519, 45, 1, 2),
(99574543, 45, 1, 1),
(35264451, 45, 2, 2),
(35751154, 45, 2, 1),
(35240234, 45, 3, 1),
(35774541, 45, 3, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipoproduto`
--

CREATE TABLE IF NOT EXISTS `tipoproduto` (
  `idTipoProduto` int(11) NOT NULL,
  `nomeTipoProduto` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tipoproduto`
--

INSERT INTO `tipoproduto` (`idTipoProduto`, `nomeTipoProduto`) VALUES
(1, 'eletrônico'),
(2, 'eletrodoméstico');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipotelefone`
--

CREATE TABLE IF NOT EXISTS `tipotelefone` (
  `idTipoTelefone` int(11) NOT NULL,
  `nomeTipoTelefone` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tipotelefone`
--

INSERT INTO `tipotelefone` (`idTipoTelefone`, `nomeTipoTelefone`) VALUES
(1, 'celular'),
(2, 'residencial'),
(3, 'comercial');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`), ADD KEY `fk_Cliente_Endereco1` (`Endereco_idEndereco`);

--
-- Indexes for table `emailcliente`
--
ALTER TABLE `emailcliente`
  ADD PRIMARY KEY (`enderecoEmailCliente`,`Cliente_idCliente`), ADD KEY `fk_Email_Cliente1` (`Cliente_idCliente`);

--
-- Indexes for table `formapagamento`
--
ALTER TABLE `formapagamento`
  ADD PRIMARY KEY (`idFormaPagamento`);

--
-- Indexes for table `fornecedor`
--
ALTER TABLE `fornecedor`
  ADD PRIMARY KEY (`idFornecedor`), ADD KEY `fk_Fornecedor_Endereco1` (`Endereco_idEndereco`);

--
-- Indexes for table `itemnotacompra`
--
ALTER TABLE `itemnotacompra`
  ADD PRIMARY KEY (`idItemNotaCompra`,`NotaCompra_nroNotaCompra`), ADD KEY `fk_ItemProdutoCompra_Produto1` (`Produto_idProduto`), ADD KEY `fk_ItemNotaCompra_NotaCompra1` (`NotaCompra_nroNotaCompra`);

--
-- Indexes for table `itemnotavenda`
--
ALTER TABLE `itemnotavenda`
  ADD PRIMARY KEY (`idItemNotaVenda`,`NotaVenda_nroNotaVenda`), ADD KEY `fk_ItemProdutoVenda_FormaPagamento1` (`FormaPagamento_idFormaPagamento`), ADD KEY `fk_ItemProdutoVenda_Produto1` (`Produto_idProduto`), ADD KEY `fk_ItemNotaVenda_NotaVenda1` (`NotaVenda_nroNotaVenda`);

--
-- Indexes for table `notacompra`
--
ALTER TABLE `notacompra`
  ADD PRIMARY KEY (`nroNotaCompra`), ADD KEY `fk_NotaCompra_Fornecedor1` (`Fornecedor_idFornecedor`);

--
-- Indexes for table `notavenda`
--
ALTER TABLE `notavenda`
  ADD PRIMARY KEY (`nroNotaVenda`), ADD KEY `fk_NotaVenda_Cliente1` (`Cliente_idCliente`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`idProduto`), ADD KEY `fk_Produto_TipoProduto1` (`TipoProduto_idTipoProduto`), ADD KEY `fk_Produto_Fornecedor1` (`Fornecedor_idFornecedor`);

--
-- Indexes for table `telefonecliente`
--
ALTER TABLE `telefonecliente`
  ADD PRIMARY KEY (`numeroTelefoneCliente`,`dddTelefoneCliente`,`Cliente_idCliente`), ADD KEY `fk_TelefoneCliente_TipoTelefone1` (`TipoTelefone_idTipoTelefone`), ADD KEY `fk_TelefoneCliente_Cliente1` (`Cliente_idCliente`);

--
-- Indexes for table `tipoproduto`
--
ALTER TABLE `tipoproduto`
  ADD PRIMARY KEY (`idTipoProduto`);

--
-- Indexes for table `tipotelefone`
--
ALTER TABLE `tipotelefone`
  ADD PRIMARY KEY (`idTipoTelefone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `formapagamento`
--
ALTER TABLE `formapagamento`
  MODIFY `idFormaPagamento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `fornecedor`
--
ALTER TABLE `fornecedor`
  MODIFY `idFornecedor` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `itemnotacompra`
--
ALTER TABLE `itemnotacompra`
  MODIFY `idItemNotaCompra` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `itemnotavenda`
--
ALTER TABLE `itemnotavenda`
  MODIFY `idItemNotaVenda` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `notacompra`
--
ALTER TABLE `notacompra`
  MODIFY `nroNotaCompra` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `notavenda`
--
ALTER TABLE `notavenda`
  MODIFY `nroNotaVenda` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `idProduto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tipoproduto`
--
ALTER TABLE `tipoproduto`
  MODIFY `idTipoProduto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tipotelefone`
--
ALTER TABLE `tipotelefone`
  MODIFY `idTipoTelefone` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `cliente`
--
ALTER TABLE `cliente`
ADD CONSTRAINT `fk_Cliente_Endereco1` FOREIGN KEY (`Endereco_idEndereco`) REFERENCES `endereco`.`endereco` (`idEndereco`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `emailcliente`
--
ALTER TABLE `emailcliente`
ADD CONSTRAINT `fk_Email_Cliente1` FOREIGN KEY (`Cliente_idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `fornecedor`
--
ALTER TABLE `fornecedor`
ADD CONSTRAINT `fk_Fornecedor_Endereco1` FOREIGN KEY (`Endereco_idEndereco`) REFERENCES `endereco`.`endereco` (`idEndereco`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `itemnotacompra`
--
ALTER TABLE `itemnotacompra`
ADD CONSTRAINT `fk_ItemNotaCompra_NotaCompra1` FOREIGN KEY (`NotaCompra_nroNotaCompra`) REFERENCES `notacompra` (`nroNotaCompra`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ItemProdutoCompra_Produto1` FOREIGN KEY (`Produto_idProduto`) REFERENCES `produto` (`idProduto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `itemnotavenda`
--
ALTER TABLE `itemnotavenda`
ADD CONSTRAINT `fk_ItemNotaVenda_NotaVenda1` FOREIGN KEY (`NotaVenda_nroNotaVenda`) REFERENCES `notavenda` (`nroNotaVenda`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ItemProdutoVenda_FormaPagamento1` FOREIGN KEY (`FormaPagamento_idFormaPagamento`) REFERENCES `formapagamento` (`idFormaPagamento`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ItemProdutoVenda_Produto1` FOREIGN KEY (`Produto_idProduto`) REFERENCES `produto` (`idProduto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `notacompra`
--
ALTER TABLE `notacompra`
ADD CONSTRAINT `fk_NotaCompra_Fornecedor1` FOREIGN KEY (`Fornecedor_idFornecedor`) REFERENCES `fornecedor` (`idFornecedor`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `notavenda`
--
ALTER TABLE `notavenda`
ADD CONSTRAINT `fk_NotaVenda_Cliente1` FOREIGN KEY (`Cliente_idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `produto`
--
ALTER TABLE `produto`
ADD CONSTRAINT `fk_Produto_Fornecedor1` FOREIGN KEY (`Fornecedor_idFornecedor`) REFERENCES `fornecedor` (`idFornecedor`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_Produto_TipoProduto1` FOREIGN KEY (`TipoProduto_idTipoProduto`) REFERENCES `tipoproduto` (`idTipoProduto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `telefonecliente`
--
ALTER TABLE `telefonecliente`
ADD CONSTRAINT `fk_TelefoneCliente_Cliente1` FOREIGN KEY (`Cliente_idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_TelefoneCliente_TipoTelefone1` FOREIGN KEY (`TipoTelefone_idTipoTelefone`) REFERENCES `tipotelefone` (`idTipoTelefone`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
