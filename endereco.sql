-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 27-Nov-2016 às 00:43
-- Versão do servidor: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `endereco`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `bairro`
--

CREATE TABLE IF NOT EXISTS `bairro` (
  `idBairro` int(11) NOT NULL,
  `nomeBairro` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `bairro`
--

INSERT INTO `bairro` (`idBairro`, `nomeBairro`) VALUES
(1, 'Conjunto B');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cidade`
--

CREATE TABLE IF NOT EXISTS `cidade` (
  `idCidade` int(11) NOT NULL,
  `nomeCidade` varchar(45) DEFAULT NULL,
  `UnidadeFederativa_idUnidadeFederativa` int(11) NOT NULL,
  `UnidadeFederativa_Pais_idPais` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cidade`
--

INSERT INTO `cidade` (`idCidade`, `nomeCidade`, `UnidadeFederativa_idUnidadeFederativa`, `UnidadeFederativa_Pais_idPais`) VALUES
(1, 'Foz do Iguaçu', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `endereco`
--

CREATE TABLE IF NOT EXISTS `endereco` (
  `idEndereco` int(11) NOT NULL,
  `cep` varchar(9) DEFAULT NULL,
  `Logradouro_idLogradouro` int(11) NOT NULL,
  `Bairro_idBairro` int(11) NOT NULL,
  `Cidade_idCidade` int(11) NOT NULL,
  `Cidade_UnidadeFederativa_idUnidadeFederativa` int(11) NOT NULL,
  `Cidade_UnidadeFederativa_Pais_idPais` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `endereco`
--

INSERT INTO `endereco` (`idEndereco`, `cep`, `Logradouro_idLogradouro`, `Bairro_idBairro`, `Cidade_idCidade`, `Cidade_UnidadeFederativa_idUnidadeFederativa`, `Cidade_UnidadeFederativa_Pais_idPais`) VALUES
(1, '85867000', 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `logradouro`
--

CREATE TABLE IF NOT EXISTS `logradouro` (
  `idLogradouro` int(11) NOT NULL,
  `nomeLogradouro` varchar(45) DEFAULT NULL,
  `TipoLogradouro_idTipoLogradouro` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `logradouro`
--

INSERT INTO `logradouro` (`idLogradouro`, `nomeLogradouro`, `TipoLogradouro_idTipoLogradouro`) VALUES
(1, 'Tancredo Neves', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pais`
--

CREATE TABLE IF NOT EXISTS `pais` (
  `idPais` int(11) NOT NULL,
  `nomePais` varchar(45) DEFAULT NULL,
  `siglaPais` varchar(3) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pais`
--

INSERT INTO `pais` (`idPais`, `nomePais`, `siglaPais`) VALUES
(1, 'Brasil', 'BR');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipologradouro`
--

CREATE TABLE IF NOT EXISTS `tipologradouro` (
  `idTipoLogradouro` int(11) NOT NULL,
  `nomeTipoLogradouro` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tipologradouro`
--

INSERT INTO `tipologradouro` (`idTipoLogradouro`, `nomeTipoLogradouro`) VALUES
(1, 'Rua'),
(2, 'Avenida'),
(3, 'Alameda');

-- --------------------------------------------------------

--
-- Estrutura da tabela `unidadefederativa`
--

CREATE TABLE IF NOT EXISTS `unidadefederativa` (
  `idUnidadeFederativa` int(11) NOT NULL,
  `nomeUnidadeFederativa` varchar(45) DEFAULT NULL,
  `siglaUnidadeFederativa` varchar(3) DEFAULT NULL,
  `Pais_idPais` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `unidadefederativa`
--

INSERT INTO `unidadefederativa` (`idUnidadeFederativa`, `nomeUnidadeFederativa`, `siglaUnidadeFederativa`, `Pais_idPais`) VALUES
(1, 'Paraná', 'PR', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bairro`
--
ALTER TABLE `bairro`
  ADD PRIMARY KEY (`idBairro`);

--
-- Indexes for table `cidade`
--
ALTER TABLE `cidade`
  ADD PRIMARY KEY (`idCidade`,`UnidadeFederativa_idUnidadeFederativa`,`UnidadeFederativa_Pais_idPais`), ADD KEY `fk_Cidade_UnidadeFederativa1` (`UnidadeFederativa_idUnidadeFederativa`,`UnidadeFederativa_Pais_idPais`);

--
-- Indexes for table `endereco`
--
ALTER TABLE `endereco`
  ADD PRIMARY KEY (`idEndereco`), ADD KEY `fk_Endereco_Rua1` (`Logradouro_idLogradouro`), ADD KEY `fk_Endereco_Bairro1` (`Bairro_idBairro`), ADD KEY `fk_Endereco_Cidade1` (`Cidade_idCidade`,`Cidade_UnidadeFederativa_idUnidadeFederativa`,`Cidade_UnidadeFederativa_Pais_idPais`);

--
-- Indexes for table `logradouro`
--
ALTER TABLE `logradouro`
  ADD PRIMARY KEY (`idLogradouro`), ADD KEY `fk_Logradouro_TipoLogradouro1` (`TipoLogradouro_idTipoLogradouro`);

--
-- Indexes for table `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`idPais`);

--
-- Indexes for table `tipologradouro`
--
ALTER TABLE `tipologradouro`
  ADD PRIMARY KEY (`idTipoLogradouro`);

--
-- Indexes for table `unidadefederativa`
--
ALTER TABLE `unidadefederativa`
  ADD PRIMARY KEY (`idUnidadeFederativa`,`Pais_idPais`), ADD KEY `fk_Estado_Pais` (`Pais_idPais`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bairro`
--
ALTER TABLE `bairro`
  MODIFY `idBairro` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cidade`
--
ALTER TABLE `cidade`
  MODIFY `idCidade` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `endereco`
--
ALTER TABLE `endereco`
  MODIFY `idEndereco` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `logradouro`
--
ALTER TABLE `logradouro`
  MODIFY `idLogradouro` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pais`
--
ALTER TABLE `pais`
  MODIFY `idPais` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tipologradouro`
--
ALTER TABLE `tipologradouro`
  MODIFY `idTipoLogradouro` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `unidadefederativa`
--
ALTER TABLE `unidadefederativa`
  MODIFY `idUnidadeFederativa` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `cidade`
--
ALTER TABLE `cidade`
ADD CONSTRAINT `fk_Cidade_UnidadeFederativa1` FOREIGN KEY (`UnidadeFederativa_idUnidadeFederativa`, `UnidadeFederativa_Pais_idPais`) REFERENCES `unidadefederativa` (`idUnidadeFederativa`, `Pais_idPais`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `endereco`
--
ALTER TABLE `endereco`
ADD CONSTRAINT `fk_Endereco_Bairro1` FOREIGN KEY (`Bairro_idBairro`) REFERENCES `bairro` (`idBairro`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_Endereco_Cidade1` FOREIGN KEY (`Cidade_idCidade`, `Cidade_UnidadeFederativa_idUnidadeFederativa`, `Cidade_UnidadeFederativa_Pais_idPais`) REFERENCES `cidade` (`idCidade`, `UnidadeFederativa_idUnidadeFederativa`, `UnidadeFederativa_Pais_idPais`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_Endereco_Rua1` FOREIGN KEY (`Logradouro_idLogradouro`) REFERENCES `logradouro` (`idLogradouro`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `logradouro`
--
ALTER TABLE `logradouro`
ADD CONSTRAINT `fk_Logradouro_TipoLogradouro1` FOREIGN KEY (`TipoLogradouro_idTipoLogradouro`) REFERENCES `tipologradouro` (`idTipoLogradouro`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `unidadefederativa`
--
ALTER TABLE `unidadefederativa`
ADD CONSTRAINT `fk_Estado_Pais` FOREIGN KEY (`Pais_idPais`) REFERENCES `pais` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
