package unioeste.geral.bo.pessoa;

import java.io.Serializable;

public class Email implements Serializable {
	private static final long serialVersionUID = 4869761205565746375L;
	private String enderecoEmail;
	private Cliente cliente;

    public Email() {
    }

    public Email(String enderecoEmail, Cliente cliente) {
        this.enderecoEmail = enderecoEmail;
		this.cliente = cliente;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getEnderecoEmail() {
        return enderecoEmail;
    }

    public void setEnderecoEmail(String enderecoEmail) {
        this.enderecoEmail = enderecoEmail;
    }

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
