package unioeste.geral.bo.pessoa;

import java.io.Serializable;
import java.util.List;

public abstract class PessoaFisica extends Pessoa implements Serializable {
	private static final long serialVersionUID = 9141369442101178846L;
	private Sexo sexo;
	private CPF cpf;
	private NomePessoa nomePessoa;

    public PessoaFisica() {
    }

    public PessoaFisica(Sexo sexo, CPF cpf, NomePessoa nomePessoa) {
        this.sexo = sexo;
        this.cpf = cpf;
        this.nomePessoa = nomePessoa;
    }

    public PessoaFisica(EnderecoEspecifico enderecoPrincipal, List<Email> listaEmails, List<Telefone> listaTelefones,
                        Sexo sexo, CPF cpf, NomePessoa nomePessoa) {
        super(enderecoPrincipal, listaEmails, listaTelefones);
        this.sexo = sexo;
        this.cpf = cpf;
        this.nomePessoa = nomePessoa;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public CPF getCpf() {
        return cpf;
    }

    public void setCpf(CPF cpf) {
        this.cpf = cpf;
    }

    public NomePessoa getNomePessoa() {
        return nomePessoa;
    }

    public void setNomePessoa(NomePessoa nomePessoa) {
        this.nomePessoa = nomePessoa;
    }
}
