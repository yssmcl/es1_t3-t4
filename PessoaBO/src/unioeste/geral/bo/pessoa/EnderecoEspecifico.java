package unioeste.geral.bo.pessoa;

import unioeste.geral.bo.endereco.Endereco;

import java.io.Serializable;

public class EnderecoEspecifico implements Serializable {
    private static final long serialVersionUID = -7611592599333383343L;
    private String nroEndereco;
    private String complementoEndereco;
    private Endereco endereco;

    public EnderecoEspecifico() {
    }

    public EnderecoEspecifico(String nroEndereco, String complementoEndereco, Endereco endereco) {
        this.nroEndereco = nroEndereco;
        this.complementoEndereco = complementoEndereco;
        this.endereco = endereco;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getNroEndereco() {
        return nroEndereco;
    }

    public void setNroEndereco(String nroEndereco) {
        this.nroEndereco = nroEndereco;
    }

    public String getComplementoEndereco() {
        return complementoEndereco;
    }

    public void setComplementoEndereco(String complementoEndereco) {
        this.complementoEndereco = complementoEndereco;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
}
