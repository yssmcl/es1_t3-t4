package unioeste.geral.bo.pessoa;

import java.io.Serializable;
import java.util.List;

public class Cliente implements Serializable {
	private static final long serialVersionUID = 106805765424485092L;
    private int idCliente;
	private Pessoa dadosCliente;

    public Cliente() {
    }

    // Construtores para PessoaFisica
    public Cliente(List<Email> listaEmails, List<Telefone> listaTelefones, EnderecoEspecifico enderecoPrincipal,
				   Sexo sexo, CPF cpf, NomePessoa nomePessoa) {
		this.dadosCliente = new PessoaFisica() {};
		setListaEmails(listaEmails);
		setListaTelefones(listaTelefones);
		setEnderecoPrincipal(enderecoPrincipal);
        setSexo(sexo);
        setCpf(cpf);
        setNomePessoa(nomePessoa);
    }
	public Cliente(int idCliente, List<Email> listaEmails, List<Telefone> listaTelefones,
				   EnderecoEspecifico enderecoPrincipal, Sexo sexo, CPF cpf, NomePessoa nomePessoa) {
		this.dadosCliente = new PessoaFisica() {};
		this.idCliente = idCliente;
		setListaEmails(listaEmails);
		setListaTelefones(listaTelefones);
		setEnderecoPrincipal(enderecoPrincipal);
		setSexo(sexo);
		setCpf(cpf);
		setNomePessoa(nomePessoa);
	}

    // Construtores para PessoaJurídica
    public Cliente(List<Email> listaEmails, List<Telefone> listaTelefones, EnderecoEspecifico enderecoPrincipal,
				   String nomeFantasia, String nomeRazaoSocial, CNPJ cnpj) {
		this.dadosCliente = new PessoaJuridica() {};
		setListaEmails(listaEmails);
		setListaTelefones(listaTelefones);
		setEnderecoPrincipal(enderecoPrincipal);
        setNomeFantasia(nomeFantasia);
        setNomeRazaoSocial(nomeRazaoSocial);
        setCnpj(cnpj);
    }
	public Cliente(int idCliente, List<Email> listaEmails, List<Telefone> listaTelefones,
				   EnderecoEspecifico enderecoPrincipal, String nomeFantasia, String nomeRazaoSocial, CNPJ cnpj) {
		this.dadosCliente = new PessoaJuridica() {};
		this.idCliente = idCliente;
		setListaEmails(listaEmails);
		setListaTelefones(listaTelefones);
		setEnderecoPrincipal(enderecoPrincipal);
		setNomeFantasia(nomeFantasia);
		setNomeFantasia(nomeFantasia);
		setNomeRazaoSocial(nomeRazaoSocial);
		setCnpj(cnpj);
	}

    // Dados Pessoa
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public Pessoa getDadosCliente() {
        return dadosCliente;
    }

    public void setDadosCliente(Pessoa dadosCliente) {
        this.dadosCliente = dadosCliente;
    }

    public List<Email> getListaEmails() {
        return dadosCliente.getListaEmails();
    }

    public void setListaEmails(List<Email> listaEmails) {
        dadosCliente.setListaEmails(listaEmails);
    }

    public List<Telefone> getListaTelefones() {
        return dadosCliente.getListaTelefones();
    }

    public void setListaTelefones(List<Telefone> listaTelefones) {
        dadosCliente.setListaTelefones(listaTelefones);
    }

    public EnderecoEspecifico getEnderecoPrincipal() {
		if (dadosCliente == null) System.out.printf("alskdjasldk");
		return dadosCliente.getEnderecoPrincipal();
    }

    public void setEnderecoPrincipal(EnderecoEspecifico enderecoPrincipal) {
		if (dadosCliente == null) System.out.printf("alsdjalksdjk");
		dadosCliente.setEnderecoPrincipal(enderecoPrincipal);
    }

    // Dados PessoaFisica
    public Sexo getSexo() {
        PessoaFisica pessoaFisica = (PessoaFisica)dadosCliente;
        if (pessoaFisica == null) {
            return null;
        }
        return pessoaFisica.getSexo();
    }

    public void setSexo(Sexo sexo) {
        ((PessoaFisica)dadosCliente).setSexo(sexo);
    }

    public CPF getCpf() {
        PessoaFisica pessoaFisica = (PessoaFisica)dadosCliente;
        if (pessoaFisica == null) {
            return null;
        }
        return pessoaFisica.getCpf();
    }

    public void setCpf(CPF cpf) {
        ((PessoaFisica)dadosCliente).setCpf(cpf);
    }

    public NomePessoa getNomePessoa() {
        PessoaFisica pessoaFisica = (PessoaFisica)dadosCliente;
        if (pessoaFisica == null) {
            return null;
        }
        return pessoaFisica.getNomePessoa();
    }

    public void setNomePessoa(NomePessoa nomePessoa) {
        ((PessoaFisica)dadosCliente).setNomePessoa(nomePessoa);
    }

    // Dados PessoaJuridica
    public String getNomeFantasia() {
        PessoaJuridica pessoaJuridica = (PessoaJuridica)dadosCliente;
        if (pessoaJuridica == null) {
            return null;
        }
        return getPessoaJuridica().getNomeFantasia();
    }

    public void setNomeFantasia(String nomeFantasia) {
        ((PessoaJuridica)dadosCliente).setNomeFantasia(nomeFantasia);
    }

    public String getNomeRazaoSocial() {
        PessoaJuridica pessoaJuridica = (PessoaJuridica)dadosCliente;
        if (pessoaJuridica == null) {
            return null;
        }
        return getPessoaJuridica().getNomeRazaoSocial();
    }

    public void setNomeRazaoSocial(String nomeRazaoSocial) {
        ((PessoaJuridica)dadosCliente).setNomeRazaoSocial(nomeRazaoSocial);
    }

    public CNPJ getCnpj() {
        PessoaJuridica pessoaJuridica = (PessoaJuridica)dadosCliente;
        if (pessoaJuridica == null) {
            return null;
        }
        return getPessoaJuridica().getCnpj();
    }

    public void setCnpj(CNPJ cnpj) {
        ((PessoaJuridica)dadosCliente).setCnpj(cnpj);
    }

    // Funções auxiliares
    public PessoaFisica getPessoaFisica() {
    	return (PessoaFisica)dadosCliente;
    }

    public PessoaJuridica getPessoaJuridica() {
        return (PessoaJuridica)dadosCliente;
    }

    public boolean isPessoaFisica() {
        return (dadosCliente instanceof PessoaFisica);
    }

    public boolean isPessoaJuridica() {
        return (dadosCliente instanceof PessoaJuridica);
    }

}