package unioeste.geral.bo.pessoa;

import java.io.Serializable;

public class Fornecedor extends PessoaJuridica implements Serializable {
	private static final long serialVersionUID = -5049763644025186687L;
    private int idFornecedor;

    public Fornecedor() {
    }

    public Fornecedor(int idFornecedor, String nomeFantasia, String nomeRazaoSocial, CNPJ cnpj, EnderecoEspecifico
            enderecoPrincipal) {
        super(nomeFantasia, nomeRazaoSocial, cnpj);
        this.idFornecedor = idFornecedor;
        this.setEnderecoPrincipal(enderecoPrincipal);
    }

    public Fornecedor(String nomeFantasia, String nomeRazaoSocial, CNPJ cnpj, EnderecoEspecifico
            enderecoPrincipal) {
        super(nomeFantasia, nomeRazaoSocial, cnpj);
        this.setEnderecoPrincipal(enderecoPrincipal);
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(int idFornecedor) {
        this.idFornecedor = idFornecedor;
    }
}
