package unioeste.geral.bo.pessoa;

import java.io.Serializable;

public class Telefone implements Serializable {
	private static final long serialVersionUID = 3408406047780076605L;
	private int numeroTelefone;
	private DDD ddd;
	private TipoTelefone tipoTelefone;
	private Cliente cliente;

    public Telefone() {
    }

	public Telefone(int numeroTelefone, DDD ddd, TipoTelefone tipoTelefone, Cliente cliente) {
		this.numeroTelefone = numeroTelefone;
		this.ddd = ddd;
		this.tipoTelefone = tipoTelefone;
		this.cliente = cliente;
	}

	public Telefone(int numeroTelefone, DDD ddd, TipoTelefone tipoTelefone) {
		this.numeroTelefone = numeroTelefone;
		this.ddd = ddd;
		this.tipoTelefone = tipoTelefone;
	}

	public static long getSerialVersionUID() {
        return serialVersionUID;
    }

	public int getNumeroTelefone() {
		return numeroTelefone;
	}

	public void setNumeroTelefone(int numeroTelefone) {
		this.numeroTelefone = numeroTelefone;
	}

	public DDD getDdd() {
		return ddd;
	}

	public void setDdd(DDD ddd) {
		this.ddd = ddd;
	}

	public TipoTelefone getTipoTelefone() {
		return tipoTelefone;
	}

	public void setTipoTelefone(TipoTelefone tipoTelefone) {
		this.tipoTelefone = tipoTelefone;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

}
