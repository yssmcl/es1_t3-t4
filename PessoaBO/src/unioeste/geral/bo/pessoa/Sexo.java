package unioeste.geral.bo.pessoa;

import java.io.Serializable;

public class Sexo implements Serializable {
	private static final long serialVersionUID = 2649771981860043441L;
	private String nomeSexo;
	private char siglaSexo;

    public Sexo() {
    }

    public Sexo(String nomeSexo, char siglaSexo) {
        this.nomeSexo = nomeSexo;
        this.siglaSexo = siglaSexo;
    }

	public Sexo(String nomeSexo) {
        if (nomeSexo.toUpperCase().equals("MASCULINO") || nomeSexo.equals("m") || nomeSexo.equals("M")) {
           this.nomeSexo = "MASCULINO";
           this.siglaSexo = 'M';
        } else if (nomeSexo.toUpperCase().equals("FEMININO") || nomeSexo.charAt(0) == 'f' || nomeSexo.charAt(0) == 'F') {
            this.nomeSexo = "FEMININO";
            this.siglaSexo = 'F';
        }
	}

	public Sexo(char siglaSexo) {
        if (siglaSexo == 'm' || siglaSexo == 'M') {
            this.nomeSexo = "MASCULINO";
            this.siglaSexo = 'M';
        } else if (siglaSexo == 'f' || siglaSexo == 'F') {
            this.nomeSexo = "FEMININO";
            this.siglaSexo = 'F';
        }
	}

	public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getNomeSexo() {
        return nomeSexo;
    }

    public void setNomeSexo(String nomeSexo) {
        this.nomeSexo = nomeSexo;
    }

    public char getSiglaSexo() {
        return siglaSexo;
    }

    public void setSiglaSexo(char siglaSexo) {
        this.siglaSexo = siglaSexo;
    }
}
