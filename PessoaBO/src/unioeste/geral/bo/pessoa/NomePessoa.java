package unioeste.geral.bo.pessoa;

import java.io.Serializable;

public class NomePessoa implements Serializable {
	private static final long serialVersionUID = 8962777884993590345L;
	private String primeiroNome;
	private String nomeDoMeio;
	private String ultimoNome;
	private String nomeCompleto;

    public NomePessoa() {
    }

    public NomePessoa(String primeiroNome, String nomeDoMeio, String ultimoNome, String nomeCompleto) {
        this.primeiroNome = primeiroNome;
        this.nomeDoMeio = nomeDoMeio;
        this.ultimoNome = ultimoNome;
        this.nomeCompleto = nomeCompleto;
    }

	public NomePessoa(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getPrimeiroNome() {
        return primeiroNome;
    }

    public void setPrimeiroNome(String primeiroNome) {
        this.primeiroNome = primeiroNome;
    }

    public String getNomeDoMeio() {
        return nomeDoMeio;
    }

    public void setNomeDoMeio(String nomeDoMeio) {
        this.nomeDoMeio = nomeDoMeio;
    }

    public String getUltimoNome() {
        return ultimoNome;
    }

    public void setUltimoNome(String ultimoNome) {
        this.ultimoNome = ultimoNome;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }
}
