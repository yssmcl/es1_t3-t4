package unioeste.geral.bo.pessoa;

import java.io.Serializable;

public class CNPJ implements Serializable {
	private static final long serialVersionUID = 7671827598412098264L;
	private long numeroCNPJ;
	private String numeroCNPJFormatado;

	public CNPJ() {
	}

	public CNPJ(long numeroCNPJ) {
		this.numeroCNPJ = numeroCNPJ;
		this.numeroCNPJFormatado = formatarCNPJ(numeroCNPJ);
	}

	public CNPJ(String numeroCNPJFormatado) {
		if (numeroCNPJFormatado.contains(".") || numeroCNPJFormatado.contains("/")) {
			this.numeroCNPJ = Long.parseLong(numeroCNPJFormatado.replaceAll("[^0-9]", ""));
			this.numeroCNPJFormatado = numeroCNPJFormatado;
		} else {
			this.numeroCNPJ = Long.parseLong(numeroCNPJFormatado);
			this.numeroCNPJFormatado = formatarCNPJ(Long.parseLong(numeroCNPJFormatado));
		}
	}

	public CNPJ(long numeroCNPJ, String numeroCNPJFormatado) {
		this.numeroCNPJ = numeroCNPJ;
		this.numeroCNPJFormatado = numeroCNPJFormatado;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public long getNumeroCNPJ() {
		return numeroCNPJ;
	}

	public void setNumeroCNPJ(long numeroCNPJ) {
		this.numeroCNPJ = numeroCNPJ;
	}

	public String getNumeroCNPJFormatado() {
		return numeroCNPJFormatado;
	}

	public void setNumeroCNPJFormatado(String numeroCNPJFormatado) {
		this.numeroCNPJFormatado = numeroCNPJFormatado;
	}

	// Funções auxiliares
	public String formatarCNPJ(long numeroCNPJ) {
		String numeroCNPJFormatado = Long.toString(numeroCNPJ);
		numeroCNPJFormatado = numeroCNPJFormatado.substring(0, 2) +
			"." + numeroCNPJFormatado.substring(2, 5) +
			"." + numeroCNPJFormatado.substring(5, 8) +
			"/" + numeroCNPJFormatado.substring(8, 12) +
			"-" + numeroCNPJFormatado.substring(12, numeroCNPJFormatado.length());

		return numeroCNPJFormatado;
	}
}
