package unioeste.geral.bo.pessoa;

import java.io.Serializable;

public class TipoTelefone implements Serializable {
	private static final long serialVersionUID = -3290465431277390233L;
	private int idTipoTelefone;
	private String nomeTipoTelefone;

    public TipoTelefone() {
    }

    public TipoTelefone(int idTipoTelefone, String nomeTipoTelefone) {
        this.idTipoTelefone = idTipoTelefone;
		this.nomeTipoTelefone = nomeTipoTelefone;
    }

	public TipoTelefone(String nomeTipoTelefone) {
		this.nomeTipoTelefone = nomeTipoTelefone;
	}

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getNomeTipoTelefone() {
        return nomeTipoTelefone;
    }

    public void setNomeTipoTelefone(String nomeTipoTelefone) {
        this.nomeTipoTelefone = nomeTipoTelefone;
    }

    public int getIdTipoTelefone() {
        return idTipoTelefone;
    }

    public void setIdTipoTelefone(int idTipoTelefone) {
        this.idTipoTelefone = idTipoTelefone;
    }
}
