package unioeste.geral.bo.pessoa;

import java.io.Serializable;

public class CPF implements Serializable{
	private static final long serialVersionUID = -4046298226928194458L;
	private long numeroCPF;
	private String numeroCPFFormatado;

	public CPF() {
	}

	public CPF(long numeroCPF, String numeroCPFFormatado) {
		this.numeroCPF = numeroCPF;
		this.numeroCPFFormatado = numeroCPFFormatado;
	}

	public CPF(long numeroCPF) {
		this.numeroCPF = numeroCPF;
		this.numeroCPFFormatado = this.formatarCPF(numeroCPF);
	}

	public CPF(String numeroCPFFormatado) {
		if (numeroCPFFormatado.contains(".") || numeroCPFFormatado.contains("/")) {
			this.numeroCPF = Long.parseLong(numeroCPFFormatado.replaceAll("[^0-9]", ""));
			this.numeroCPFFormatado = numeroCPFFormatado;
		} else {
			this.numeroCPF = Long.parseLong(numeroCPFFormatado);
			this.numeroCPFFormatado  = formatarCPF(Long.parseLong(numeroCPFFormatado));
		}
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public long getNumeroCPF() {
		return numeroCPF;
	}

	public void setNumeroCPF(long numeroCPF) {
		this.numeroCPF = numeroCPF;
	}

	public String getNumeroCPFFormatado() {
		return numeroCPFFormatado;
	}

	public void setNumeroCPFFormatado(String numeroCPFFormatado) {
		this.numeroCPFFormatado = numeroCPFFormatado;
	}

	// Funções auxiliares
	public String formatarCPF(long numeroCPF) {
		String numeroCPFFormatado = Long.toString(numeroCPF);
		numeroCPFFormatado = numeroCPFFormatado.substring(0, 3) +
			"." + numeroCPFFormatado.substring(3, 6) +
			"." + numeroCPFFormatado.substring(6, 9) +
			"-" + numeroCPFFormatado.substring(9, numeroCPFFormatado.length());

		return numeroCPFFormatado;
	}
}
