package unioeste.geral.bo.pessoa;

import java.io.Serializable;

public class DDD implements Serializable{
    private static final long serialVersionUID = -5724391802786591134L;
    private int numeroDDD;

    public DDD() {
    }

    public DDD(int numeroDDD) {
        this.numeroDDD = numeroDDD;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getNumeroDDD() {
        return numeroDDD;
    }

    public void setNumeroDDD(int numeroDDD) {
        this.numeroDDD = numeroDDD;
    }
}
