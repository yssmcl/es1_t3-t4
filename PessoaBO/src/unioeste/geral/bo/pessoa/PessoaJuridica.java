package unioeste.geral.bo.pessoa;

import java.io.Serializable;
import java.util.List;

public abstract class PessoaJuridica extends Pessoa implements Serializable {
	private static final long serialVersionUID = -7268043962587879190L;
	private String nomeFantasia;
	private String nomeRazaoSocial;
	private CNPJ cnpj;

    public PessoaJuridica() {
    }

    public PessoaJuridica(String nomeFantasia, String nomeRazaoSocial, CNPJ cnpj) {
        this.nomeFantasia = nomeFantasia;
        this.nomeRazaoSocial = nomeRazaoSocial;
        this.cnpj = cnpj;
    }

    public PessoaJuridica(List<Email> listaEmails, List<Telefone> listaTelefones, EnderecoEspecifico enderecoPrincipal,
                          String nomeFantasia, String nomeRazaoSocial, CNPJ cnpj) {
        super(enderecoPrincipal, listaEmails, listaTelefones);
        this.nomeFantasia = nomeFantasia;
        this.nomeRazaoSocial = nomeRazaoSocial;
        this.cnpj = cnpj;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getNomeRazaoSocial() {
        return nomeRazaoSocial;
    }

    public void setNomeRazaoSocial(String nomeRazaoSocial) {
        this.nomeRazaoSocial = nomeRazaoSocial;
    }

    public CNPJ getCnpj() {
        return cnpj;
    }

    public void setCnpj(CNPJ cnpj) {
        this.cnpj = cnpj;
    }
}
