package unioeste.geral.bo.pessoa;

import java.io.Serializable;
import java.util.List;

public abstract class Pessoa implements Serializable {
	private static final long serialVersionUID = -7611532556333383343L;
	private EnderecoEspecifico enderecoPrincipal;
    private List<Email> listaEmails;
    private List<Telefone> listaTelefones;

    public Pessoa() {
    }

    public Pessoa(EnderecoEspecifico enderecoPrincipal, List<Email> listaEmails, List<Telefone> listaTelefones) {
        this.enderecoPrincipal = enderecoPrincipal;
        this.listaEmails = listaEmails;
        this.listaTelefones = listaTelefones;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<Email> getListaEmails() {
        return listaEmails;
    }

    public void setListaEmails(List<Email> listaEmails) {
        this.listaEmails = listaEmails;
    }

    public List<Telefone> getListaTelefones() {
        return listaTelefones;
    }

    public void setListaTelefones(List<Telefone> listaTelefones) {
        this.listaTelefones = listaTelefones;
    }

    public EnderecoEspecifico getEnderecoPrincipal() {
        return enderecoPrincipal;
    }

    public void setEnderecoPrincipal(EnderecoEspecifico enderecoPrincipal) {
        this.enderecoPrincipal = enderecoPrincipal;
    }
}
