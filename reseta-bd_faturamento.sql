USE faturamento;

-- Exclusões
DELETE FROM ItemNotaVenda;
DELETE FROM NotaVenda;
DELETE FROM FormaPagamento;
DELETE FROM ItemNotaCompra;
DELETE FROM NotaCompra;
DELETE FROM Produto;
DELETE FROM TipoProduto;
DELETE FROM TelefoneCliente;
DELETE FROM TipoTelefone;
DELETE FROM EmailCliente;
DELETE FROM Cliente;
DELETE FROM Fornecedor;

-- Inserções
INSERT INTO Fornecedor   VALUES (1, "nome fantasia", "nome razão social", "12.345.678/1234-56", "123", "complemento", 1);
INSERT INTO Fornecedor   VALUES (2, "nome fantasia2", "nome razão social2", "12.345.678/1234-57", "123", "complemento", 1);

INSERT INTO Cliente   VALUES (1, "nome cliente pf", "123.456.789-00", "M", NULL, NULL, NULL, 1, 0, 123, "complemento", 1);
INSERT INTO Cliente   VALUES (2, NULL, NULL, NULL, "nome fantasia cliente pj", "nome razão social cliente pj", "12.345.678/1234-56", 0, 1, 123, "complemento", 1);

INSERT INTO EmailCliente VALUES ("pf@email.com", 1);
INSERT INTO EmailCliente VALUES ("pf2@email.com", 1);
INSERT INTO EmailCliente VALUES ("pj@email.com", 2);
INSERT INTO EmailCliente VALUES ("pj2@email.com", 2);

INSERT INTO TipoTelefone VALUES (1, "celular");
INSERT INTO TipoTelefone VALUES (2, "residencial");
INSERT INTO TipoTelefone VALUES (3, "comercial");

INSERT INTO TelefoneCliente VALUES (99574543, 45, 1, 1);
INSERT INTO TelefoneCliente VALUES (35751154, 45, 2, 1);
INSERT INTO TelefoneCliente VALUES (35240234, 45, 3, 1);
INSERT INTO TelefoneCliente VALUES (98754519, 45, 1, 2);
INSERT INTO TelefoneCliente VALUES (35264451, 45, 2, 2);
INSERT INTO TelefoneCliente VALUES (35774541, 45, 3, 2);

INSERT INTO TipoProduto VALUES (1, "Eletrônico");
INSERT INTO TipoProduto VALUES (2, "Eletrodoméstico");

INSERT INTO Produto VALUES (1, 1554565987945, "nome produto", 15.50, 25.50, 132, 1, 1);
INSERT INTO Produto VALUES (2, 4564754787876, "nome produto2", 48.50, 120.50, 240, 2, 1);
INSERT INTO Produto VALUES (3, 1238724788238, "nome produto3", 60.0, 80.0, 50, 1, 2);

INSERT INTO NotaCompra VALUES (1, '2016-10-10', 16.0, 5.00, 1);
INSERT INTO NotaCompra VALUES (2, '2016-12-12', 10.50, 0.00, 2);

INSERT INTO ItemNotaCompra VALUES (1, 2.00, 5, 10.0, 1, 1);
INSERT INTO ItemNotaCompra VALUES (2, 3.00, 2, 6.0, 2, 1);
INSERT INTO ItemNotaCompra VALUES (3, 10.50, 1, 10.50, 1, 2);

INSERT INTO FormaPagamento VALUES (1, "Boleto bancário");
INSERT INTO FormaPagamento VALUES (2, "Cartão de crédito");

INSERT INTO NotaVenda VALUES (1, '2016-06-06', 20.0, 2.00, 1, 2);
INSERT INTO NotaVenda VALUES (2, '2016-08-08', 15.0, 1.00, 2, 1);

INSERT INTO ItemNotaVenda VALUES (1, 2.00, 5, 10.0, 2, 1);
INSERT INTO ItemNotaVenda VALUES (2, 3.00, 2, 6.0, 2, 2);
INSERT INTO ItemNotaVenda VALUES (3, 10.50, 1, 10.50, 1, 1);
