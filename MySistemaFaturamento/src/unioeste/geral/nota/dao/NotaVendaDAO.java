package unioeste.geral.nota.dao;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.nota.NotaVenda;

import java.sql.*;

public class NotaVendaDAO {
    public int inserirNotaVenda(NotaVenda objeto, Connection connection) {
        String sql =
                "INSERT INTO NotaVenda (" +
                    "dataEmissaoNotaVenda," +
                    "totalBrutoNotaVenda," +
                    "descontoTotalNotaVenda," +
                    "Cliente_idCliente," +
					"FormaPagamento_idFormaPagamento" +
                ") VALUES (?,?,?,?,?)";

        int updateCount = 0;
		try (PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			stmt.setDate(++i, objeto.getDataEmissaoNota());
			stmt.setDouble(++i, objeto.getTotalBrutoNota());
			stmt.setDouble(++i, objeto.getDescontoTotalNota());
			stmt.setInt(++i, objeto.getCliente().getIdCliente());
			stmt.setInt(++i, objeto.getFormaPagamento().getIdFormaPagamento());

            updateCount = stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            objeto.setNroNota(rs.getInt(1));
		} catch (SQLException e) {
            e.printStackTrace();
        }
        return updateCount;
    }

    public ResultSet buscarNotaVendaPorPK(int idNotaVenda, Connection connection) {
        //language=MySQL
        String sql =
                "SELECT * " +
                "FROM NotaVenda " +
                "WHERE nroNotaVenda = ?";
        ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection, idNotaVenda);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public ResultSet buscarTodosNotaVenda(Connection connection) {
        //language=MySQL
        String sql =
                "SELECT * " +
                "FROM NotaVenda";
        ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public int atualizarNotaVenda(NotaVenda objeto, Connection connection) {
        String sql =
                "UPDATE NotaVenda " +
                "SET " +
                    "dataEmissaoNotaVenda = ?," +
                    "totalBrutoNotaVenda = ?," +
                    "descontoTotalNotaVenda = ?," +
					"Cliente_idCliente = ?," +
                    "FormaPagamento_idFormaPagamento = ? " +
                "WHERE nroNotaVenda = ?";

        int updateCount = 0;
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            int i = 0;
            stmt.setDate(++i, objeto.getDataEmissaoNota());
            stmt.setDouble(++i, objeto.getTotalBrutoNota());
            stmt.setDouble(++i, objeto.getDescontoTotalNota());
            stmt.setInt(++i, objeto.getCliente().getIdCliente());
			stmt.setInt(++i, objeto.getFormaPagamento().getIdFormaPagamento());
            stmt.setInt(++i, objeto.getNroNota());

            updateCount = stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return updateCount;
    }

    public int deletarNotaVenda(NotaVenda objeto, Connection connection) {
        String sql =
                "DELETE FROM NotaVenda " +
                "WHERE nroNotaVenda = ?";

        int updateCount = 0;
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			stmt.setInt(1, objeto.getNroNota());
            updateCount = stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return updateCount;
    }
}
