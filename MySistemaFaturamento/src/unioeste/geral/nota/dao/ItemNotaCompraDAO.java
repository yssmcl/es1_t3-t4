package unioeste.geral.nota.dao;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.nota.ItemNotaCompra;
import unioeste.geral.bo.nota.NotaCompra;

import java.sql.*;

public class ItemNotaCompraDAO {
    public int inserirItemNotaCompra(ItemNotaCompra objeto, Connection connection) {
        String sql =
                "INSERT INTO ItemNotaCompra (" +
                    "precoUnitarioItemNotaCompra," +
                    "qtdItemNotaCompra," +
                    "totalItemNotaCompra," +
                    "Produto_idProduto, " +
					"NotaCompra_nroNotaCompra" +
                ") VALUES (?,?,?,?,?)";

        int updateCount = 0;
		try (PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			stmt.setDouble(++i, objeto.getPrecoUnitario());
			stmt.setInt(++i, objeto.getQtdItemNota());
			stmt.setDouble(++i, objeto.getTotalItemNota());
			stmt.setInt(++i, objeto.getProduto().getIdProduto());
            stmt.setInt(++i, objeto.getNota().getNroNota());

			updateCount = stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            objeto.setIdItemNota(rs.getInt(1));
		} catch (SQLException e) {
            e.printStackTrace();
        }
        return updateCount;
    }

    public ResultSet buscarItemNotaCompraPorPK(int idItemNotaCompra, Connection connection) {
        //language=MySQL
        String sql =
                "SELECT * " +
                "FROM ItemNotaCompra " +
                "WHERE idItemNotaCompra = ?";

        ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection, idItemNotaCompra);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public ResultSet buscarItemNotaCompraPorNota(NotaCompra notaCompra, Connection connection) {
        //language=MySQL
        String sql =
                "SELECT * " +
                "FROM ItemNotaCompra " +
                "WHERE NotaCompra_nroNotaCompra = ?";
        ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection, notaCompra.getNroNota());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public ResultSet buscarTodosItemNotaCompra(Connection connection) {
        //language=MySQL
        String sql =
                "SELECT * " +
                "FROM ItemNotaCompra";
        ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public int atualizarItemNotaCompra(ItemNotaCompra objeto, Connection connection) {
        String sql =
                "UPDATE ItemNotaCompra " +
                "SET " +
                    "precoUnitarioItemNotaCompra = ?," +
                    "qtdItemNotaCompra = ?," +
                    "totalItemNotaCompra = ?," +
                    "Produto_idProduto = ? " +
                "WHERE idItemNotaCompra = ?";

        int updateCount = 0;
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            int i = 0;
            stmt.setDouble(++i, objeto.getPrecoUnitario());
            stmt.setInt(++i, objeto.getQtdItemNota());
            stmt.setDouble(++i, objeto.getTotalItemNota());
            stmt.setInt(++i, objeto.getProduto().getIdProduto());
            stmt.setInt(++i, objeto.getIdItemNota());

            updateCount = stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return updateCount;
    }

    public int deletarItemNotaCompra(ItemNotaCompra objeto, Connection connection) {
        String sql =
                "DELETE FROM ItemNotaCompra " +
                "WHERE idItemNotaCompra = ?";

        int updateCount = 0;
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, objeto.getIdItemNota());
            updateCount = stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
		return updateCount;
    }
}