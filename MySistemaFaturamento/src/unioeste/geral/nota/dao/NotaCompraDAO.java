package unioeste.geral.nota.dao;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.nota.NotaCompra;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class NotaCompraDAO {
    public int inserirNotaCompra(NotaCompra objeto, Connection connection) {
        String sql =
                "INSERT INTO NotaCompra (" +
                    "dataEmissaoNotaCompra," +
                    "totalBrutoNotaCompra," +
                    "descontoTotalNotaCompra," +
                    "Fornecedor_idFornecedor" +
                ") VALUES (?,?,?,?)";

		int updateCount = 0;
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			int i = 0;
			stmt.setDate(++i, objeto.getDataEmissaoNota());
			stmt.setDouble(++i, objeto.getTotalBrutoNota());
			stmt.setDouble(++i, objeto.getDescontoTotalNota());
			stmt.setInt(++i, objeto.getFornecedor().getIdFornecedor());

            updateCount = stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            objeto.setNroNota(rs.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return updateCount;
    }

    public ResultSet buscarNotaCompraPorPK(int nroNotaCompra, Connection connection) {
        //language=MySQL
        String sql =
                "SELECT * " +
                "FROM NotaCompra " +
                "WHERE nroNotaCompra = ?";
        ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection, nroNotaCompra);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public ResultSet buscarTodosNotaCompra(Connection connection) {
        //language=MySQL
        String sql =
                "SELECT * " +
                "FROM NotaCompra";
        ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public int atualizarNotaCompra(NotaCompra objeto, Connection connection) {
        String sql =
                "UPDATE NotaCompra " +
                "SET " +
                    "dataEmissaoNotaCompra = ?," +
                    "totalBrutoNotaCompra = ?," +
                    "descontoTotalNotaCompra = ?," +
                    "Fornecedor_idFornecedor = ? " +
                "WHERE nroNotaCompra = ?";

        int updateCount = 0;
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            int i = 0;
            stmt.setDate(++i, objeto.getDataEmissaoNota());
            stmt.setDouble(++i, objeto.getTotalBrutoNota());
            stmt.setDouble(++i, objeto.getDescontoTotalNota());
            stmt.setInt(++i, objeto.getFornecedor().getIdFornecedor());
            stmt.setInt(++i, objeto.getNroNota());

            updateCount = stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return updateCount;
    }

    public int deletarNotaCompra(NotaCompra objeto, Connection connection) {
        String sql =
                "DELETE FROM NotaCompra " +
                "WHERE nroNotaCompra = ?";

        int updateCount = 0;
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			stmt.setInt(1, objeto.getNroNota());
            updateCount = stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return updateCount;
    }
}
