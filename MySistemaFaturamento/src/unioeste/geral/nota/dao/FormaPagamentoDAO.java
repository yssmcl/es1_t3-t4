package unioeste.geral.nota.dao;

import unioeste.apoio.bd.Conector;

import java.sql.Connection;
import java.sql.ResultSet;

public class FormaPagamentoDAO {
    public ResultSet buscarFormaPagamentoPorPK(int idFormaPagamento, Connection connection) {
        //language=MySQL
        String sql =
                "SELECT * " +
                "FROM FormaPagamento " +
                "WHERE idFormaPagamento = ?";
        ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection, idFormaPagamento);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public ResultSet buscarTodosFormaPagamento(Connection connection) {
        //language=MySQL
        String sql =
                "SELECT * " +
				"FROM FormaPagamento";
        ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }
}