package unioeste.geral.nota.dao;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.nota.ItemNotaVenda;
import unioeste.geral.bo.nota.NotaVenda;

import java.sql.*;

public class ItemNotaVendaDAO {
    public int inserirItemNotaVenda(ItemNotaVenda objeto, Connection connection) {
        String sql =
                "INSERT INTO ItemNotaVenda (" +
                    "precoUnitarioItemNotaVenda," +
                    "qtdItemNotaVenda," +
                    "totalItemNotaVenda," +
                    "Produto_idProduto," +
					"NotaVenda_nroNotaVenda" +
                ") VALUES (?,?,?,?,?)";

        int updateCount = 0;
		try (PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			stmt.setDouble(++i, objeto.getPrecoUnitario());
			stmt.setInt(++i, objeto.getQtdItemNota());
			stmt.setDouble(++i, objeto.getTotalItemNota());
			stmt.setInt(++i, objeto.getProduto().getIdProduto());
            stmt.setInt(++i, objeto.getNota().getNroNota());

            updateCount = stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            objeto.setIdItemNota(rs.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return updateCount;
    }

    public ResultSet buscarItemNotaVendaPorPK(int idItemNotaVenda, Connection connection) {
        //language=MySQL
        String sql =
                "SELECT * " +
                "FROM ItemNotaVenda " +
                "WHERE idItemNotaVenda = ?";

        ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection, idItemNotaVenda);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public ResultSet buscarItemNotaVendaPorNota(NotaVenda notaVenda, Connection connection) {
        //language=MySQL
        String sql =
                "SELECT * " +
                "FROM ItemNotaVenda " +
                "WHERE NotaVenda_nroNotaVenda = ?";
        ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection, notaVenda.getNroNota());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public ResultSet buscarTodosItemNotaVenda(Connection connection) {
        //language=MySQL
        String sql =
                "SELECT * " +
                "FROM ItemNotaVenda";
        ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public int atualizarItemNotaVenda(ItemNotaVenda objeto, Connection connection) {
        String sql =
                "UPDATE ItemNotaVenda " +
                "SET " +
                    "precoUnitarioItemNotaVenda = ?," +
                    "qtdItemNotaVenda = ?," +
                    "totalItemNotaVenda = ?," +
                    "Produto_idProduto = ? " +
                "WHERE idItemNotaVenda = ?";

        int updateCount = 0;
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            int i = 0;
            stmt.setDouble(++i, objeto.getPrecoUnitario());
            stmt.setInt(++i, objeto.getQtdItemNota());
            stmt.setDouble(++i, objeto.getTotalItemNota());
            stmt.setInt(++i, objeto.getProduto().getIdProduto());
            stmt.setInt(++i, objeto.getIdItemNota());

            updateCount = stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return updateCount;
    }

    public int deletarItemNotaVenda(ItemNotaVenda objeto, Connection connection) {
        String sql =
                "DELETE FROM ItemNotaVenda " +
                "WHERE idItemNotaVenda = ?";

        int updateCount = 0;
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			stmt.setInt(1, objeto.getIdItemNota());
            updateCount = stmt.executeUpdate();
		} catch (SQLException e) {
            e.printStackTrace();
        }
		return updateCount;
    }
}
