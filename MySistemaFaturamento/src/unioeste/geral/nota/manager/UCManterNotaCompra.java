package unioeste.geral.nota.manager;

import unioeste.geral.bo.nota.NotaCompra;

import java.util.List;

public class UCManterNotaCompra {

    public void cadastrarNotaCompra(NotaCompra notaCompra) throws Exception {
		new NotaCompraControle().salvarNotaCompra(notaCompra);
    }

    public NotaCompra obterNotaCompra(NotaCompra notaCompra) throws Exception {
		NotaCompra notaCompraObtido = new NotaCompraControle().recuperarNotaCompraPorPK(notaCompra);
		if (notaCompraObtido == null) {
			throw new Exception("Nota de compra não encontrada");
		}
		return notaCompraObtido;
    }

    public List<NotaCompra> obterTodosNotaCompra() throws Exception {
		List<NotaCompra> listaNotaCompra = new NotaCompraControle().recuperarTodosNotaCompra();
		if (listaNotaCompra != null) {
			return listaNotaCompra;
		} else {
			throw new Exception("Nenhuma nota de compra cadastrada");
		}
    }

    public NotaCompra alterarNotaCompra(NotaCompra notaCompra) throws Exception {
		NotaCompra notaCompraAlterada = new NotaCompraControle().modificarNotaCompra(notaCompra);
		if (notaCompraAlterada == null) {
			throw new Exception("Nota de compra não alterada");
		}
		return notaCompraAlterada;
    }

    public void excluirNotaCompra(NotaCompra notaCompra) throws Exception {
		if (new NotaCompraControle().removerNotaCompra(notaCompra) == 0) {
			throw new Exception("Nota de compra não excluída");
		}
    }

}
