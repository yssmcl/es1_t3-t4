package unioeste.geral.nota.manager;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.nota.ItemNotaCompra;
import unioeste.geral.bo.nota.NotaCompra;
import unioeste.geral.bo.pessoa.Fornecedor;
import unioeste.geral.bo.produto.Produto;
import unioeste.geral.nota.dao.NotaCompraDAO;
import unioeste.geral.pessoa.manager.FornecedorControle;
import unioeste.geral.produto.dao.ProdutoDAO;
import unioeste.geral.produto.manager.ProdutoControle;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.ArrayList;
import java.util.List;

public class NotaCompraControle {
//    private final Connection connection;
    private static final String URL_BD_FATURAMENTO = "jdbc:mysql://localhost/faturamento";
    private static final String USUARIO_BD = "root";
    private static final String SENHA_BD = "";

    public NotaCompraControle() {
//        this.connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD);
    }

    public NotaCompra criarNotaCompra(ResultSet rs, Connection connection) {
		NotaCompra notaCompra = new NotaCompra();

		try {
			notaCompra.setNroNota(rs.getInt("nroNotaCompra"));

			Fornecedor fornecedor = new FornecedorControle().recuperarFornecedorPorPK(new Fornecedor() {{
				setIdFornecedor(rs.getInt("Fornecedor_idFornecedor"));
			}});

			List<ItemNotaCompra> lista = new ItemNotaCompraControle().recuperarItensPorNota(notaCompra, connection);

			notaCompra.setDataEmissaoNota(rs.getDate("dataEmissaoNotaCompra"));
			notaCompra.setTotalBrutoNota(rs.getDouble("totalBrutoNotaCompra"));
			notaCompra.setDescontoTotalNota(rs.getDouble("descontoTotalNotaCompra"));
			notaCompra.setValorLiquidoNota(rs.getDouble("totalBrutoNotaCompra") - rs.getDouble("descontoTotalNotaCompra"));
			notaCompra.setFornecedor(fornecedor);
			notaCompra.setListaItensCompra(lista);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return notaCompra;
    }

    public void salvarNotaCompra(NotaCompra notaCompra) throws Exception {
        ProdutoControle produtoControle = new ProdutoControle();
        ItemNotaCompraControle itemNotaCompraControle = new ItemNotaCompraControle();
		int falha = 0;

		try (Connection connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD)) {
			connection.setAutoCommit(false);
			Savepoint savepoint = connection.setSavepoint();

			if (new NotaCompraDAO().inserirNotaCompra(notaCompra, connection) > 0) {
				for (ItemNotaCompra itemNotaCompra : notaCompra.getListaItensCompra()) {
					// Atualiza estoque
					Produto produto = produtoControle.recuperarProdutoPorPK(itemNotaCompra.getProduto());
					int qtdAtualProduto = produto.getQtdProduto();
					int novaQtdProduto = qtdAtualProduto + itemNotaCompra.getQtdItemNota();
					produto.setQtdProduto(novaQtdProduto);

					// Verifica se a operação no banco foi bem sucedida
					if (itemNotaCompraControle.salvarItemNotaCompra(itemNotaCompra, connection) == 0 ||
						new ProdutoDAO().atualizarProduto(produto, connection) == 0) {
//						produtoControle.modificarProduto(produto) == 0) {
						falha++;
						break;
					}
				}
			} else {
				falha++;
			}

			// Fecha transação
			if (falha == 0) {
				connection.commit();
			} else {
				connection.rollback(savepoint);
			}
			connection.releaseSavepoint(savepoint);
			connection.setAutoCommit(true);
			connection.close();
		}
    }

    public NotaCompra recuperarNotaCompraPorPK(NotaCompra notaCompra) throws Exception {
		NotaCompra notaCompraRecuperada = null;
		try (Connection connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD)) {
			ResultSet rs = new NotaCompraDAO().buscarNotaCompraPorPK(notaCompra.getNroNota(), connection);
			if (rs != null && rs.next()) {
				notaCompraRecuperada = criarNotaCompra(rs, connection);
			}
		}
		return notaCompraRecuperada;
    }

    private List<NotaCompra> criarListaNotaCompra(ResultSet rs) {
		List<NotaCompra> listaNotaCompra = new ArrayList<>();
		try (Connection connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD)) {
			while (rs.next()) {
				NotaCompra notaCompra = criarNotaCompra(rs, connection);
				listaNotaCompra.add(notaCompra);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listaNotaCompra;
	}

    public List<NotaCompra> recuperarTodosNotaCompra() throws Exception {
		try (Connection connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD)) {
			ResultSet rs = new NotaCompraDAO().buscarTodosNotaCompra(connection);
			if (rs != null && rs.next()) {
				rs.previous();
				List<NotaCompra> listaNotaCompra = criarListaNotaCompra(rs);
				connection.close();
				return listaNotaCompra;
			} else {
				connection.close();
				return null;
			}
		}
	}

    public NotaCompra modificarNotaCompra(NotaCompra notaCompra) throws Exception {
        NotaCompra novaNotaCompra = new NotaCompra(
        		notaCompra.getNroNota(),
				notaCompra.getDataEmissaoNota(),
				notaCompra.getTotalBrutoNota(),
				notaCompra.getDescontoTotalNota(),
				notaCompra.getTotalBrutoNota() - notaCompra.getDescontoTotalNota(),
				notaCompra.getFornecedor(),
				notaCompra.getListaItensCompra());

		try (Connection connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD)) {
			if (new NotaCompraDAO().atualizarNotaCompra(novaNotaCompra, connection) == 0) {
				return null;
			}
		}
        return novaNotaCompra;
    }

    public int removerNotaCompra(NotaCompra notaCompra) throws SQLException {
		int updateCount;
		try (Connection connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD)) {
			updateCount = new NotaCompraDAO().deletarNotaCompra(notaCompra, connection);
		}
		return updateCount;
    }

}
