package unioeste.geral.nota.manager;

import unioeste.geral.bo.nota.NotaVenda;

import java.util.List;

public class UCManterNotaVenda {

    public void cadastrarNotaVenda(NotaVenda notaVenda) throws Exception {
		new NotaVendaControle().salvarNotaVenda(notaVenda);
    }

    public NotaVenda obterNotaVenda(NotaVenda notaVenda) throws Exception {
		NotaVenda notaVendaObtido = new NotaVendaControle().recuperarNotaVendaPorPK(notaVenda);
		if (notaVendaObtido == null) {
			throw new Exception("Nota de venda não encontrada");
		}
		return notaVendaObtido;
    }

    public List<NotaVenda> obterTodosNotaVenda() throws Exception {
		List<NotaVenda> listaNotaVenda = new NotaVendaControle().recuperarTodosNotaVenda();
		if (listaNotaVenda != null) {
			return listaNotaVenda;
		} else {
			throw new Exception("Nenhuma nota de venda cadastrada");
		}
    }

    public NotaVenda alterarNotaVenda(NotaVenda notaVenda) throws Exception {
		NotaVenda notaVendaAlterada = new NotaVendaControle().modificarNotaVenda(notaVenda);
		if (notaVendaAlterada == null) {
			throw new Exception("Nota de venda não alterada");
		}
		return notaVendaAlterada;
    }

    public void excluirNotaVenda(NotaVenda notaVenda) throws Exception {
		if (new NotaVendaControle().removerNotaVenda(notaVenda) == 0) {
			throw new Exception("Nota de venda não excluída");
		}
    }

}
