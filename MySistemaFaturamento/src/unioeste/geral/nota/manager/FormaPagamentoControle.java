package unioeste.geral.nota.manager;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.nota.FormaPagamento;
import unioeste.geral.nota.dao.FormaPagamentoDAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FormaPagamentoControle {
	private final Connection connection;
	private static final String URL_BD_FATURAMENTO = "jdbc:mysql://localhost/faturamento";
	private static final String USUARIO_BD = "root";
	private static final String SENHA_BD = "";

	public FormaPagamentoControle() {
		this.connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD);
	}

	private FormaPagamento criarFormaPagamento(ResultSet rs) {
		try {
			return new FormaPagamento(rs.getInt("idFormaPagamento"), rs.getString("nomeFormaPagamento"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public FormaPagamento recuperarFormaPagamentoPorPK(FormaPagamento formaPagamento) throws Exception {
		ResultSet rs = new FormaPagamentoDAO().buscarFormaPagamentoPorPK(formaPagamento.getIdFormaPagamento(), connection);
		if (rs != null && rs.next()) {
			FormaPagamento formaPagamentoRecuperada = criarFormaPagamento(rs);
			connection.close();
			return formaPagamentoRecuperada;
		} else {
			connection.close();
			throw new Exception("Forma de pagamento não encontrada");
		}
	}

	private List<FormaPagamento> criarListaFormaPagamento(ResultSet rs) {
		List<FormaPagamento> listaFormaPagamento = new ArrayList<>();
		try {
			while (rs.next()) {
				FormaPagamento formaPagamento = criarFormaPagamento(rs);
				listaFormaPagamento.add(formaPagamento);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listaFormaPagamento;
	}

	public List<FormaPagamento> recuperarTodosFormaPagamento() throws Exception {
		ResultSet rs = new FormaPagamentoDAO().buscarTodosFormaPagamento(connection);
		if (rs != null && rs.next()) {
			rs.previous();
			List<FormaPagamento> listaFormaPagamento = criarListaFormaPagamento(rs);
			connection.close();
			return listaFormaPagamento;
		} else {
			connection.close();
			return null;
		}
	}

}
