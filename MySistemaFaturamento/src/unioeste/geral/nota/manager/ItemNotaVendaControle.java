package unioeste.geral.nota.manager;

import unioeste.geral.bo.nota.ItemNotaVenda;
import unioeste.geral.bo.nota.NotaVenda;
import unioeste.geral.bo.produto.Produto;
import unioeste.geral.nota.dao.ItemNotaVendaDAO;
import unioeste.geral.produto.manager.ProdutoControle;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ItemNotaVendaControle {
    public List<ItemNotaVenda> recuperarItensPorNota(NotaVenda notaVenda, Connection connection) throws SQLException {
        ResultSet rs = new ItemNotaVendaDAO().buscarItemNotaVendaPorNota(notaVenda, connection);
        List<ItemNotaVenda> lista = null;
        if (rs != null && rs.next()) {
            rs.previous();
            lista = criarListaItemNotaVenda(rs, notaVenda);
        }
        return lista;
    }

    private List<ItemNotaVenda> criarListaItemNotaVenda(ResultSet rs, NotaVenda notaVenda) {
        List<ItemNotaVenda> lista = new ArrayList<>();
        try {
            while (rs.next()) {
                Produto produto = new ProdutoControle().recuperarProdutoPorPK(new Produto() {{
                    setIdProduto(rs.getInt("Produto_idProduto"));
                }});

                ItemNotaVenda itemNotaVenda = new ItemNotaVenda(
                		rs.getInt("idItemNotaVenda"),
						rs.getDouble("precoUnitarioItemNotaVenda"),
                        rs.getInt("qtdItemNotaVenda"),
						rs.getDouble("totalItemNotaVenda"),
						produto,
						notaVenda);

                lista.add(itemNotaVenda);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public int salvarItemNotaVenda(ItemNotaVenda itemNotaVenda, Connection connection) {
		return new ItemNotaVendaDAO().inserirItemNotaVenda(itemNotaVenda, connection);
    }
}
