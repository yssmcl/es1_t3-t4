package unioeste.geral.nota.manager;

import unioeste.geral.bo.nota.ItemNotaCompra;
import unioeste.geral.bo.nota.NotaCompra;
import unioeste.geral.bo.produto.Produto;
import unioeste.geral.nota.dao.ItemNotaCompraDAO;
import unioeste.geral.produto.manager.ProdutoControle;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ItemNotaCompraControle {
    public List<ItemNotaCompra> recuperarItensPorNota(NotaCompra notaCompra, Connection connection) throws SQLException {
        ResultSet rs = new ItemNotaCompraDAO().buscarItemNotaCompraPorNota(notaCompra, connection);
        List<ItemNotaCompra> lista = null;
        if (rs != null && rs.next()) {
            rs.previous();
            lista = criarListaItemNotaCompra(rs, notaCompra);
        }
        return lista;
    }

    private List<ItemNotaCompra> criarListaItemNotaCompra(ResultSet rs, NotaCompra notaCompra) {
        List<ItemNotaCompra> lista = new ArrayList<>();
        try {
            while (rs.next()) {
                Produto produto = new ProdutoControle().recuperarProdutoPorPK(new Produto() {{
                    setIdProduto(rs.getInt("Produto_idProduto"));
                }});

                ItemNotaCompra itemNotaCompra = new ItemNotaCompra(
                		rs.getInt("idItemNotaCompra"),
						rs.getDouble("precoUnitarioItemNotaCompra"),
                        rs.getInt("qtdItemNotaCompra"),
						rs.getDouble("totalItemNotaCompra"),
						produto,
						notaCompra);

                lista.add(itemNotaCompra);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lista;
    }

    public int salvarItemNotaCompra(ItemNotaCompra itemNotaCompra, Connection connection) {
		return new ItemNotaCompraDAO().inserirItemNotaCompra(itemNotaCompra, connection);
    }
}
