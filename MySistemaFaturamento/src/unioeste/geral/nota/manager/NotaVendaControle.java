package unioeste.geral.nota.manager;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.nota.ItemNotaVenda;
import unioeste.geral.bo.nota.NotaVenda;
import unioeste.geral.bo.pessoa.Cliente;
import unioeste.geral.bo.produto.Produto;
import unioeste.geral.nota.dao.NotaVendaDAO;
import unioeste.geral.pessoa.manager.ClienteControle;
import unioeste.geral.produto.dao.ProdutoDAO;
import unioeste.geral.produto.manager.ProdutoControle;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.ArrayList;
import java.util.List;

public class NotaVendaControle {
//    private final Connection connection;
    private static final String URL_BD_FATURAMENTO = "jdbc:mysql://localhost/faturamento";
    private static final String USUARIO_BD = "root";
    private static final String SENHA_BD = "";

    public NotaVendaControle() {
//        this.connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD);
    }

    public NotaVenda criarNotaVenda(ResultSet rs, Connection connection) {
		NotaVenda notaVenda = new NotaVenda();

		try {
			notaVenda.setNroNota(rs.getInt("nroNotaVenda"));

			Cliente cliente = new ClienteControle().recuperarClientePorPK(new Cliente() {{
				setIdCliente(rs.getInt("Cliente_idCliente"));
			}});

			List<ItemNotaVenda> lista = new ItemNotaVendaControle().recuperarItensPorNota(notaVenda, connection);

			notaVenda.setDataEmissaoNota(rs.getDate("dataEmissaoNotaVenda"));
			notaVenda.setTotalBrutoNota(rs.getDouble("totalBrutoNotaVenda"));
			notaVenda.setDescontoTotalNota(rs.getDouble("descontoTotalNotaVenda"));
			notaVenda.setValorLiquidoNota(rs.getDouble("totalBrutoNotaVenda") - rs.getDouble("descontoTotalNotaVenda"));
			notaVenda.setCliente(cliente);
			notaVenda.setListaItensVenda(lista);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return notaVenda;
    }

    public void salvarNotaVenda(NotaVenda notaVenda) throws Exception {
        ProdutoControle produtoControle = new ProdutoControle();
        ItemNotaVendaControle itemNotaVendaControle = new ItemNotaVendaControle();
		int falha = 0;

		try (Connection connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD)) {
			connection.setAutoCommit(false);
			Savepoint savepoint = connection.setSavepoint();

			if (new NotaVendaDAO().inserirNotaVenda(notaVenda, connection) > 0) {
				for (ItemNotaVenda itemNotaVenda : notaVenda.getListaItensVenda()) {
					// Atualiza estoque
					Produto produto = produtoControle.recuperarProdutoPorPK(itemNotaVenda.getProduto());
					int qtdAtualProduto = produto.getQtdProduto();
					int novaQtdProduto = qtdAtualProduto - itemNotaVenda.getQtdItemNota();
					produto.setQtdProduto(novaQtdProduto);

					// Verifica se a operação no banco foi bem sucedida
					if (itemNotaVendaControle.salvarItemNotaVenda(itemNotaVenda, connection) == 0 ||
						new ProdutoDAO().atualizarProduto(produto, connection) == 0) {
//						produtoControle.modificarProduto(produto) == 0) {
						falha++;
						break;
					}
				}
			} else {
				falha++;
			}

			// Fecha transação
			if (falha == 0) {
				connection.commit();
			} else {
				connection.rollback(savepoint);
			}
			connection.releaseSavepoint(savepoint);
			connection.setAutoCommit(true);
			connection.close();
		}
    }

    public NotaVenda recuperarNotaVendaPorPK(NotaVenda notaVenda) throws Exception {
		NotaVenda notaVendaRecuperada = null;
		try (Connection connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD)) {
			ResultSet rs = new NotaVendaDAO().buscarNotaVendaPorPK(notaVenda.getNroNota(), connection);
			if (rs != null && rs.next()) {
				notaVendaRecuperada = criarNotaVenda(rs, connection);
			}
		}
		return notaVendaRecuperada;
    }

    private List<NotaVenda> criarListaNotaVenda(ResultSet rs) {
		List<NotaVenda> listaNotaVenda = new ArrayList<>();
		try (Connection connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD)) {
			while (rs.next()) {
				NotaVenda notaVenda = criarNotaVenda(rs, connection);
				listaNotaVenda.add(notaVenda);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listaNotaVenda;
	}

    public List<NotaVenda> recuperarTodosNotaVenda() throws Exception {
		try (Connection connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD)) {
			ResultSet rs = new NotaVendaDAO().buscarTodosNotaVenda(connection);
			if (rs != null && rs.next()) {
				rs.previous();
				List<NotaVenda> listaNotaVenda = criarListaNotaVenda(rs);
				connection.close();
				return listaNotaVenda;
			} else {
				connection.close();
				return null;
			}
		}
	}

    public NotaVenda modificarNotaVenda(NotaVenda notaVenda) throws Exception {
        NotaVenda novaNotaVenda = new NotaVenda(
        		notaVenda.getNroNota(),
				notaVenda.getDataEmissaoNota(),
				notaVenda.getTotalBrutoNota(),
				notaVenda.getDescontoTotalNota(),
				notaVenda.getTotalBrutoNota() - notaVenda.getDescontoTotalNota(),
				notaVenda.getCliente(),
				notaVenda.getListaItensVenda(),
				notaVenda.getFormaPagamento());

		try (Connection connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD)) {
			if (new NotaVendaDAO().atualizarNotaVenda(novaNotaVenda, connection) == 0) {
				return null;
			}
		}
        return novaNotaVenda;
    }

    public int removerNotaVenda(NotaVenda notaVenda) throws SQLException {
		int updateCount;
		try (Connection connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD)) {
			updateCount = new NotaVendaDAO().deletarNotaVenda(notaVenda, connection);
		}
		return updateCount;
    }

}
