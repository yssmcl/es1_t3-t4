package unioeste.geral.produto.dao;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.produto.Produto;

import java.sql.*;

public class ProdutoDAO {
    public void inserirProduto(Produto objeto, Connection connection) {
        String sql =
                "INSERT INTO Produto (" +
                    "nomeProduto," +
                    "codigoBarrasProduto," +
                    "precoCustoProduto," +
                    "precoVendaProduto," +
                    "qtdProduto," +
                    "TipoProduto_idTipoProduto," +
                    "Fornecedor_idFornecedor" +
                ") VALUES (?,?,?,?,?,?,?)";

		try (PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			stmt.setString(++i, objeto.getNomeProduto());
			stmt.setLong(++i, objeto.getCodigoBarrasProduto());
			stmt.setDouble(++i, objeto.getPrecoCustoProduto());
			stmt.setDouble(++i, objeto.getPrecoVendaProduto());
			stmt.setInt(++i, objeto.getQtdProduto());
			stmt.setInt(++i, objeto.getTipoProduto().getIdTipoProduto());
			stmt.setInt(++i, objeto.getFornecedor().getIdFornecedor());

			stmt.execute();

			// seta ID
			ResultSet rs = stmt.getGeneratedKeys();
			rs.next();
			objeto.setIdProduto(rs.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }

    public ResultSet buscarProdutoPorPK(int idProduto, Connection connection) {
		//language=MySQL
        String sql =
                "SELECT * " +
                "FROM Produto " +
                "WHERE idProduto = ?";
		ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection, idProduto);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

	public ResultSet buscarProdutoPorAtributo(String atributo, Object valor, Connection connection) {
		//language=MySQL
		ResultSet rs = null;
		try {
			rs = new Conector().buscarDados("Produto", atributo, valor, connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}

    public ResultSet buscarTodosProdutos(Connection connection) {
		//language=MySQL
        String sql =
                "SELECT * " +
                "FROM Produto";
		ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public int atualizarProduto(Produto objeto, Connection connection) {
		String sql =
				"UPDATE Produto " +
				"SET " +
					"nomeProduto = ?," +
					"codigoBarrasProduto = ?," +
					"precoCustoProduto = ?," +
					"precoVendaProduto = ?," +
					"qtdProduto = ?," +
					"TipoProduto_idTipoProduto = ?," +
					"Fornecedor_idFornecedor = ? " +
				"WHERE idProduto = ?";

		int updateCount = 0;
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			int i = 0;
			stmt.setString(++i, objeto.getNomeProduto());
			stmt.setLong(++i, objeto.getCodigoBarrasProduto());
			stmt.setDouble(++i, objeto.getPrecoCustoProduto());
			stmt.setDouble(++i, objeto.getPrecoVendaProduto());
			stmt.setInt(++i, objeto.getQtdProduto());
			stmt.setInt(++i, objeto.getTipoProduto().getIdTipoProduto());
			stmt.setInt(++i, objeto.getFornecedor().getIdFornecedor());
			stmt.setInt(++i, objeto.getIdProduto());

			updateCount = stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return updateCount;
    }

    public int deletarProduto(Produto objeto, Connection connection) {
        String sql =
                "DELETE FROM Produto " +
                "WHERE idProduto = ?";

		int updateCount = 0;
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			stmt.setInt(1, objeto.getIdProduto());
			updateCount = stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updateCount;
	}
}
