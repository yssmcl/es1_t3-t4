package unioeste.geral.produto.dao;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.produto.TipoProduto;

import java.sql.*;

public class TipoProdutoDAO {
    public void inserirTipoProduto(TipoProduto objeto, Connection connection) {
        String sql =
                "INSERT INTO TipoProduto (" +
                    "nomeTipoProduto" +
                ") VALUES (?)";

		try (PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			stmt.setString(++i, objeto.getNomeTipoProduto());

			stmt.execute();

			ResultSet rs = stmt.getGeneratedKeys();
			rs.next();
			objeto.setIdTipoProduto(rs.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

    public ResultSet buscarTipoProdutoPorPK(int idTipoProduto, Connection connection) {
		//language=MySQL
        String sql =
                "SELECT * " +
                "FROM TipoProduto " +
                "WHERE idTipoProduto = ?";
		ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection, idTipoProduto);
        } catch (Exception e) {
            e.printStackTrace();
        }
		return rs;
    }

	public ResultSet buscarTipoProdutoPorAtributo(String atributo, Object valor, Connection connection) {
		//language=MySQL
		ResultSet rs = null;
		try {
			rs = new Conector().buscarDados("TipoProduto", atributo, valor, connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}

    public ResultSet buscarTodosTipoProdutos(Connection connection) {
		//language=MySQL
        String sql =
                "SELECT * " +
                "FROM TipoProduto";
		ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public int atualizarTipoProduto(TipoProduto objeto, Connection connection) {
		String sql =
				"UPDATE TipoProduto " +
				"SET " +
					"nomeTipoProduto = ? " +
				"WHERE idTipoProduto = ?";

		int updateCount = 0;
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			int i = 0;
			stmt.setString(++i, objeto.getNomeTipoProduto());
			stmt.setInt(++i, objeto.getIdTipoProduto());

			updateCount = stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return updateCount;
    }

    public int deletarTipoProduto(TipoProduto objeto, Connection connection) {
        String sql =
                "DELETE FROM TipoProduto " +
                "WHERE idTipoProduto = ?";

		int updateCount = 0;
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			stmt.setInt(1, objeto.getIdTipoProduto());
			updateCount = stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updateCount;
	}

}
