package unioeste.geral.produto.manager;

import unioeste.geral.bo.produto.Produto;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@LocalBean
@Stateless(name = "UCManterProdutoEJB")
@WebService(endpointInterface = "unioeste.geral.produto.manager.UCManterProduto")
public class UCManterProdutoImpl implements UCManterProduto {

	@Override
	@WebMethod
    public Produto cadastrarProduto(Produto produto) throws Exception {
		return new ProdutoControle().salvarProduto(produto);
    }

	@Override
	@WebMethod
    public Produto obterProduto(Produto produto) throws Exception {
		Produto produtoObtido = new ProdutoControle().recuperarProdutoPorPK(produto);
		if (produtoObtido == null) {
			throw new Exception("Produto não encontrado");
		}
		return produtoObtido;
    }

	@Override
	@WebMethod
    public List<Produto> obterTodosProdutos() throws Exception {
		List<Produto> listaProduto = new ProdutoControle().recuperarTodosProdutos();
		if (listaProduto != null) {
			return listaProduto;
		} else {
			throw new Exception("Nenhum produto cadastrado");
		}
    }

	@Override
	@WebMethod
    public void alterarProduto(Produto produto) throws Exception {
		if (new ProdutoControle().modificarProduto(produto) == 0) {
			throw new Exception("Nenhum produto alterado");
		}
    }

	@Override
	@WebMethod
    public void excluirProduto(Produto produto) throws Exception {
		if (new ProdutoControle().removerProduto(produto) == 0) {
			throw new Exception("Nenhum produto excluído");
		}
    }

}
