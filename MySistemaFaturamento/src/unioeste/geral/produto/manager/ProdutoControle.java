package unioeste.geral.produto.manager;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.pessoa.Fornecedor;
import unioeste.geral.bo.produto.Produto;
import unioeste.geral.bo.produto.TipoProduto;
import unioeste.geral.pessoa.manager.FornecedorControle;
import unioeste.geral.produto.dao.ProdutoDAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProdutoControle {
    private final Connection connection;
    private static final String URL_BD_FATURAMENTO = "jdbc:mysql://localhost/faturamento";
    private static final String URL_BD_ENDERECO = "jdbc:mysql://localhost/endereco";
    private static final String USUARIO_BD = "root";
    private static final String SENHA_BD = "";

    public ProdutoControle() {
        this.connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD);
    }

	public boolean isNomeProdutoValido(Produto produto) {
		return produto.getNomeProduto().matches("[\\p{L}\\w\\s-]+") && produto.getNomeProduto().length() <= 45;
	}

	public void validarProduto(Produto produto) throws Exception {
		if (!isNomeProdutoValido(produto)) {
			throw new Exception("Nome do produto \"" + produto.getNomeProduto() + "\" inválido");
		}
	}

    public Produto criarProduto(ResultSet rs) {
		Produto produto = new Produto();
		try {
			TipoProduto tipoProduto = new TipoProduto();
			tipoProduto.setIdTipoProduto(rs.getInt("TipoProduto_idTipoProduto"));
			Fornecedor fornecedor = new Fornecedor();
			fornecedor.setIdFornecedor(rs.getInt("Fornecedor_idFornecedor"));
			tipoProduto = new TipoProdutoControle().recuperarTipoProdutoPorPK(tipoProduto);
			fornecedor = new FornecedorControle().recuperarFornecedorPorPK(fornecedor);

			produto = new Produto(rs.getInt("idProduto"), rs.getLong("codigoBarrasProduto"), rs.getString("nomeProduto"),
				rs.getDouble("precoCustoProduto"), rs.getDouble("precoVendaProduto"), rs.getInt("qtdProduto"),
				tipoProduto, fornecedor);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return produto;
    }

    public Produto salvarProduto(Produto produto) throws Exception {
		validarProduto(produto);

		new ProdutoDAO().inserirProduto(produto, connection);
		connection.close();
		return produto;
    }

    public Produto recuperarProdutoPorPK(Produto produto) throws Exception {
		try (Connection connection1 = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD)) {
			ResultSet rs = new ProdutoDAO().buscarProdutoPorPK(produto.getIdProduto(), connection1);
			if (rs != null && rs.next()) {
				return criarProduto(rs);
			} else {
				throw new Exception("Produto não encontrado");
			}
		}
    }

	public Produto recuperarProdutoPorAtributo(String atributo, Object valor) throws Exception {
		try (Connection connection1 = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD)) {
			ResultSet rs = new ProdutoDAO().buscarProdutoPorAtributo(atributo, valor, connection1);
			if (rs != null && rs.next()) {
				return criarProduto(rs);
			} else {
				throw new Exception("Produto não encontrado");
			}
		}
	}

    private List<Produto> criarListaProdutos(ResultSet rs) {
		List<Produto> listaProduto = new ArrayList<>();
		try {
			while (rs.next()) {
				Produto produto = criarProduto(rs);
				listaProduto.add(produto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listaProduto;
	}

    public List<Produto> recuperarTodosProdutos() throws Exception {
        ResultSet rs = new ProdutoDAO().buscarTodosProdutos(connection);
		if (rs != null && rs.next()) {
			rs.previous();
			List<Produto> listaProduto = criarListaProdutos(rs);
			connection.close();
			return listaProduto;
		} else {
			connection.close();
			return null;
		}
	}

    public int modificarProduto(Produto produto) throws Exception {
		int updateCount;
		try (Connection connection1 = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD)) {
			System.out.println(produto.getIdProduto());
			Produto novoProduto = new Produto(
					produto.getIdProduto(),
					produto.getCodigoBarrasProduto(),
					produto.getNomeProduto(),
					produto.getPrecoCustoProduto(),
					produto.getPrecoVendaProduto(),
					produto.getQtdProduto(),
					produto.getTipoProduto(),
					produto.getFornecedor());

			validarProduto(produto);
			updateCount = new ProdutoDAO().atualizarProduto(novoProduto, connection1);
		}
		return updateCount;
    }

    public int removerProduto(Produto produto) {
		int updateCount = new ProdutoDAO().deletarProduto(produto, connection);
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updateCount;
    }

}
