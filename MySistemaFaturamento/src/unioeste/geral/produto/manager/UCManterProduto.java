package unioeste.geral.produto.manager;

import unioeste.geral.bo.produto.Produto;

import javax.ejb.Remote;
import javax.jws.WebService;
import java.util.List;

@Remote
@WebService
public interface UCManterProduto {

	public Produto cadastrarProduto(Produto produto) throws Exception;

	public Produto obterProduto(Produto produto) throws Exception;

	public List<Produto> obterTodosProdutos() throws Exception;

	public void alterarProduto(Produto produto) throws Exception;

	public void excluirProduto(Produto produto) throws Exception;

}
