package unioeste.geral.produto.manager;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.produto.TipoProduto;
import unioeste.geral.produto.dao.TipoProdutoDAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TipoProdutoControle {
	private final Connection connection;
	private static final String URL_BD_FATURAMENTO = "jdbc:mysql://localhost/faturamento";
	private static final String USUARIO_BD = "root";
	private static final String SENHA_BD = "";

	public TipoProdutoControle() {
		this.connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD);
	}

	private TipoProduto criarTipoProduto(ResultSet rs) {
		try {
			return new TipoProduto(rs.getInt("idTipoProduto"), rs.getString("nomeTipoProduto"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public TipoProduto recuperarTipoProdutoPorPK(TipoProduto tipoProduto) throws Exception {
		ResultSet rs = new TipoProdutoDAO().buscarTipoProdutoPorPK(tipoProduto.getIdTipoProduto(), connection);
		if (rs != null && rs.next()) {
			TipoProduto tipoProdutoRecuperado = criarTipoProduto(rs);
			connection.close();
			return tipoProdutoRecuperado;
		} else {
			connection.close();
			throw new Exception("Tipo de produto não encontrado");
		}
	}

	public TipoProduto recuperarTipoProdutoPorAtributo(String atributo, Object valor) throws Exception {
		ResultSet rs = new TipoProdutoDAO().buscarTipoProdutoPorAtributo(atributo, valor, connection);
		if (rs != null && rs.next()) {
			TipoProduto tipoProdutoRecuperado = criarTipoProduto(rs);
			connection.close();
			return tipoProdutoRecuperado;
		} else {
			connection.close();
			throw new Exception("Tipo de produto não encontrado");
		}
	}

	private List<TipoProduto> criarListaTipoProduto(ResultSet rs) {
		List<TipoProduto> listaTipoProduto = new ArrayList<>();
		try {
			while (rs.next()) {
				TipoProduto tipoProduto = criarTipoProduto(rs);
				listaTipoProduto.add(tipoProduto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listaTipoProduto;
	}

	public List<TipoProduto> recuperarTodosTipoProduto() throws Exception {
		ResultSet rs = new TipoProdutoDAO().buscarTodosTipoProdutos(connection);
		if (rs != null && rs.next()) {
			rs.previous();
			List<TipoProduto> listaTipoProduto = criarListaTipoProduto(rs);
			connection.close();
			return listaTipoProduto;
		} else {
			connection.close();
			return null;
		}
	}
}
