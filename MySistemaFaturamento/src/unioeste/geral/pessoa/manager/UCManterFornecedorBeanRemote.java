package unioeste.geral.pessoa.manager;

import unioeste.geral.bo.pessoa.Fornecedor;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface UCManterFornecedorBeanRemote {

	public void cadastrarFornecedor(Fornecedor fornecedor) throws Exception;

	public Fornecedor obterFornecedor(Fornecedor fornecedor) throws Exception;

	public List<Fornecedor> obterTodosFornecedores() throws Exception;

	public void alterarFornecedor(Fornecedor fornecedor) throws Exception;

	public void excluirFornecedor(Fornecedor fornecedor) throws Exception;

}
