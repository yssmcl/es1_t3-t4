package unioeste.geral.pessoa.manager;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.pessoa.Cliente;
import unioeste.geral.bo.pessoa.DDD;
import unioeste.geral.bo.pessoa.Telefone;
import unioeste.geral.bo.pessoa.TipoTelefone;
import unioeste.geral.pessoa.dao.TelefoneClienteDAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class TelefoneClienteControle {
	private final Connection connection;
	private static final String URL_BD_FATURAMENTO = "jdbc:mysql://localhost/faturamento";
	private static final String USUARIO_BD = "root";
	private static final String SENHA_BD = "";

	public TelefoneClienteControle() {
		this.connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD);
	}

	public List<Telefone> criarListaTelefoneCliente(ResultSet rs, Cliente cliente) {
		List<Telefone> listaTelefones = new ArrayList<>();
		try {
			while (rs.next()) {
				DDD ddd = new DDD(rs.getInt("dddTelefoneCliente"));
				TipoTelefone tipoTelefone = new TipoTelefone();
				tipoTelefone.setIdTipoTelefone(rs.getInt("TipoTelefone_idTipoTelefone"));
				tipoTelefone = new TipoTelefoneControle().recuperarTipoTelefonePorPK(tipoTelefone);
				Telefone telefone = new Telefone(rs.getInt("numeroTelefoneCliente"), ddd, tipoTelefone, cliente);
				listaTelefones.add(telefone);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listaTelefones;
	}

	private void salvarTelefoneCliente(Telefone telefone, Cliente cliente) {
		telefone.setCliente(cliente);
		new TelefoneClienteDAO().inserirTelefoneCliente(telefone, connection);
	}

	public void salvarListaTelefoneCliente(List<Telefone> listaTelefones, Cliente cliente) throws Exception {
		for (Telefone e : listaTelefones) {
			salvarTelefoneCliente(e, cliente);
		}
		connection.close();
	}

	public List<Telefone> recuperarTelefonesPorCliente(Cliente cliente) throws Exception {
		ResultSet rs = new TelefoneClienteDAO().buscarTelefonesPorCliente(cliente, connection);
		if (rs != null && rs.next()) {
			rs.previous();
			List<Telefone> listaTelefones = criarListaTelefoneCliente(rs, cliente);
			connection.close();
			return listaTelefones;
		} else {
			connection.close();
            return null;
		}
	}
}
