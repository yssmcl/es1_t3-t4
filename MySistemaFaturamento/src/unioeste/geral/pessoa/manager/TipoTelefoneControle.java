package unioeste.geral.pessoa.manager;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.pessoa.TipoTelefone;
import unioeste.geral.pessoa.dao.TipoTelefoneDAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TipoTelefoneControle {
	private final Connection connection;
	private static final String URL_BD_FATURAMENTO = "jdbc:mysql://localhost/faturamento";
	private static final String USUARIO_BD = "root";
	private static final String SENHA_BD = "";

	public TipoTelefoneControle() {
		this.connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD);
	}

	private TipoTelefone criarTipoTelefone(ResultSet rs) {
		try {
			return new TipoTelefone(rs.getInt("idTipoTelefone"), rs.getString("nomeTipoTelefone"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public TipoTelefone recuperarTipoTelefonePorPK(TipoTelefone tipoTelefone) throws Exception {
		ResultSet rs = new TipoTelefoneDAO().buscarTipoTelefonePorPK(tipoTelefone.getIdTipoTelefone(), connection);
		if (rs != null && rs.next()) {
			TipoTelefone tipoTelefoneRecuperado = criarTipoTelefone(rs);
			connection.close();
			return tipoTelefoneRecuperado;
		} else {
			connection.close();
			throw new Exception("Tipo de telefone não encontrado");
		}
	}
}
