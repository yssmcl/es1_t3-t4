package unioeste.geral.pessoa.manager;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.pessoa.Cliente;
import unioeste.geral.bo.pessoa.Email;
import unioeste.geral.pessoa.dao.EmailClienteDAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmailClienteControle {
	private final Connection connection;
	private static final String URL_BD_FATURAMENTO = "jdbc:mysql://localhost/faturamento";
	private static final String USUARIO_BD = "root";
	private static final String SENHA_BD = "";

	public EmailClienteControle() {
		this.connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD);
	}

	private List<Email> criarListaEmailCliente(ResultSet rs, Cliente cliente) {
		List<Email> listaEmails = new ArrayList<>();
		try {
			while (rs.next()) {
				Email email = new Email(rs.getString("enderecoEmailCliente"), cliente);
				listaEmails.add(email);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listaEmails;
	}

	private void salvarEmailCliente(Email email, Cliente cliente) {
		email.setCliente(cliente);
		new EmailClienteDAO().inserirEmailCliente(email, connection);
	}

	public void salvarListaEmailCliente(List<Email> listaEmails, Cliente cliente) throws Exception {
		for (Email e : listaEmails) {
			salvarEmailCliente(e, cliente);
		}
		connection.close();
	}

	public List<Email> recuperarEmailsPorCliente(Cliente cliente) throws Exception {
		ResultSet rs = new EmailClienteDAO().buscarEmailsPorCliente(cliente, connection);
		if (rs != null && rs.next()) {
			rs.previous();
			List<Email> listaEmails = criarListaEmailCliente(rs, cliente);
			connection.close();
			return listaEmails;
		} else {
			connection.close();
            return null;
		}
	}
}
