package unioeste.geral.pessoa.manager;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.endereco.Endereco;
import unioeste.geral.bo.pessoa.EnderecoEspecifico;
import unioeste.geral.endereco.dao.EnderecoDAO;
import unioeste.geral.endereco.manager.EnderecoControle;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EnderecoEspecificoControle {
	private final Connection connection;
	private static final String URL_BD_ENDERECO = "jdbc:mysql://localhost/endereco";
	private static final String USUARIO_BD = "root";
	private static final String SENHA_BD = "";

	public EnderecoEspecificoControle() {
		this.connection = new Conector().getConnection(URL_BD_ENDERECO, USUARIO_BD, SENHA_BD);
	}

	public EnderecoEspecifico criarEnderecoEspecificoFornecedor(ResultSet rs) {
		EnderecoEspecifico enderecoEspecifico = new EnderecoEspecifico();
		try {
			ResultSet rsEndereco = new EnderecoDAO().buscarEnderecoPorID(rs.getInt("Endereco_idEndereco"),
				connection);
			if (rsEndereco != null && rsEndereco.next()) {
				Endereco endereco = new EnderecoControle().populaApartirDoBD(rsEndereco.getInt("idEndereco"),
					rsEndereco.getString("cep"), rsEndereco.getInt("Logradouro_idLogradouro"),
					rsEndereco.getInt("Bairro_idBairro"), rsEndereco.getInt("Cidade_idCidade"),
					rsEndereco.getInt("Cidade_UnidadeFederativa_idUnidadeFederativa"));
				enderecoEspecifico = new EnderecoEspecifico(rs.getString("nroEnderecoFornecedor"),
					rs.getString("complementoEnderecoFornecedor"), endereco);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return enderecoEspecifico;
	}

	public EnderecoEspecifico criarEnderecoEspecificoCliente(ResultSet rs) {
		EnderecoEspecifico enderecoEspecifico = new EnderecoEspecifico();
		try {
			ResultSet rsEndereco = new EnderecoDAO().buscarEnderecoPorID(rs.getInt("Endereco_idEndereco"),
					connection);
			if (rsEndereco != null && rsEndereco.next()) {
				Endereco endereco = new EnderecoControle().populaApartirDoBD(rsEndereco.getInt("idEndereco"),
						rsEndereco.getString("cep"), rsEndereco.getInt("Logradouro_idLogradouro"),
						rsEndereco.getInt("Bairro_idBairro"), rsEndereco.getInt("Cidade_idCidade"),
						rsEndereco.getInt("Cidade_UnidadeFederativa_idUnidadeFederativa"));
				enderecoEspecifico = new EnderecoEspecifico(rs.getString("nroEnderecoCliente"),
						rs.getString("complementoEnderecoCliente"), endereco);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return enderecoEspecifico;
	}
}
