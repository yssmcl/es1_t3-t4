package unioeste.geral.pessoa.manager;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.pessoa.*;
import unioeste.geral.pessoa.dao.ClienteDAO;
import unioeste.geral.pessoa.dao.EmailClienteDAO;
import unioeste.geral.pessoa.dao.TelefoneClienteDAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClienteControle {
	private final Connection connection;
	private static final String URL_BD_FATURAMENTO = "jdbc:mysql://localhost/faturamento";
	private static final String USUARIO_BD = "root";
	private static final String SENHA_BD = "";

	public ClienteControle() {
		this.connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD);
	}

	public int isCPFValido(CPF cpf) {
		String numeroCPFFormatado = cpf.getNumeroCPFFormatado();

		if (numeroCPFFormatado.length() != 14) {
			return 1;

			// Elimina CPFs inválidos conhecidos
		} else if (numeroCPFFormatado.equals("000.000.000-00") ||
				numeroCPFFormatado.equals("111.111.111-11") ||
				numeroCPFFormatado.equals("222.222.222-22") ||
				numeroCPFFormatado.equals("333.333.333-33") ||
				numeroCPFFormatado.equals("444.444.444-44") ||
				numeroCPFFormatado.equals("555.555.555-55") ||
				numeroCPFFormatado.equals("666.666.666-66") ||
				numeroCPFFormatado.equals("777.777.777-77") ||
				numeroCPFFormatado.equals("888.888.888-88") ||
				numeroCPFFormatado.equals("999.999.999-99")) {
			return 2;
		} else if (!numeroCPFFormatado.matches("[0-9]{3}\\.[0-9]{3}\\.[0-9]{3}-[0-9]{2}")) {
			return 3;
		}

		return 0;
	}

	public boolean isNomePessoaValido(NomePessoa nomePessoa) {
		return nomePessoa.getNomeCompleto().matches("[\\p{L}\\w\\s-]+") && nomePessoa.getNomeCompleto().length() < 45;
	}

	public int isSexoValido(Sexo sexo) {
		String nomeSexo = sexo.getNomeSexo().toUpperCase();
		char siglaSexo = Character.toUpperCase(sexo.getSiglaSexo());

		if (siglaSexo != 'M' && siglaSexo != 'F') {
			return 1;
		} else if (!nomeSexo.equals("MASCULINO") && !nomeSexo.equals("FEMININO")) {
			return 2;
		}
		return 0;
	}

	public void validarPessoaFisica(PessoaFisica pessoaFisica) throws Exception {
		int isCPFValido = isCPFValido(pessoaFisica.getCpf());
		boolean isNomePessoaValido = isNomePessoaValido(pessoaFisica.getNomePessoa());
		int isSexoValido = isSexoValido(pessoaFisica.getSexo());

		if (isCPFValido == 1) {
			throw new Exception("CPF \"" + pessoaFisica.getCpf().getNumeroCPFFormatado() + "\" possui tamanho inválido");
		} else if (isCPFValido == 2) {
			throw new Exception("CPF \"" + pessoaFisica.getCpf().getNumeroCPFFormatado() + "\" possui dígitos repetidos");
		} else if (isCPFValido == 3) {
			throw new Exception("CPF \"" + pessoaFisica.getCpf().getNumeroCPFFormatado() + "\" possui formato inválido");
		} else if (!isNomePessoaValido) {
			throw new Exception("Nome inválido");
		} else if (isSexoValido == 1) {
			throw new Exception("Sigla do sexo inválida");
		} else if (isSexoValido == 2) {
			throw new Exception("Nome do sexo inválido");
		}
	}

	public boolean isNomeFantasiaValido(String nomeFantasia) {
		// \p{L}: expressão regular para qualquer letra Unicode Latin-1 Supplement
		return nomeFantasia.matches("[\\p{L}\\w\\s-]+") && nomeFantasia.length() <= 45;
	}

	public boolean isNomeRazaoSocialValido(String nomeRazaoSocial) {
		return nomeRazaoSocial.matches("[\\p{L}\\w\\s-]+") && nomeRazaoSocial.length() <= 45;
	}

	public int isCNPJValido(CNPJ cnpj) {
		String numeroCNPJFormatado = cnpj.getNumeroCNPJFormatado();

		if (numeroCNPJFormatado.length() != 18) {
			return 1;
		} else if (!numeroCNPJFormatado.matches("[0-9]{2}\\.[0-9]{3}\\.[0-9]{3}/[0-9]{4}-[0-9]{2}")) {
			return 2;
		}
		return 0;
	}

	public void validarPessoaJuridica(PessoaJuridica pessoaJuridica) throws Exception {
		if (!isNomeFantasiaValido(pessoaJuridica.getNomeFantasia())) {
			throw new Exception("Nome fantasia \"" + pessoaJuridica.getNomeFantasia() + "\" inválido");
		} else if (!isNomeRazaoSocialValido(pessoaJuridica.getNomeRazaoSocial())) {
			throw new Exception("Nome razão social \"" + pessoaJuridica.getNomeRazaoSocial() + "\" inválido");
		} else if (isCNPJValido(pessoaJuridica.getCnpj()) == 1) {
			throw new Exception("Número CNPJ \"" + pessoaJuridica.getCnpj().getNumeroCNPJFormatado() + "\" possui tamanho inválido");
		} else if (isCNPJValido(pessoaJuridica.getCnpj()) == 2) {
			throw new Exception("Número CNPJ \"" + pessoaJuridica.getCnpj().getNumeroCNPJFormatado()  + "\" possui formato inválido");
		}
	}

	private boolean cnpjExiste(Cliente cliente) {
		try {
			ResultSet rs = new ClienteDAO().buscarTodosClientes(connection);
			while (rs.next()) {
				if (rs.getString("cnpjCliente") != null
					&& rs.getString("cnpjCliente").equals(cliente.getCnpj().getNumeroCNPJFormatado())) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private boolean nomeRazaoSocialExiste(Cliente cliente) {
		try {
			ResultSet rs = new ClienteDAO().buscarTodosClientes(connection);
			while (rs.next()) {
				if (rs.getString("nomeRazaoSocialCliente") != null
					&& rs.getString("nomeRazaoSocialCliente").equals(cliente.getNomeRazaoSocial())) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private boolean cpfExiste(Cliente cliente) {
		try {
			ResultSet rs = new ClienteDAO().buscarTodosClientes(connection);
			while (rs.next()) {
                if (rs.getString("cpfCliente") != null
					&&	rs.getString("cpfCliente").equals(cliente.getCpf().getNumeroCPFFormatado())) {
                    return true;
                }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private boolean telefoneExiste(Telefone telefone) {
		try {
			ResultSet rs = new TelefoneClienteDAO().buscarTelefonePorPK(telefone, connection);
			if (rs != null && rs.next() &&
				rs.getInt("numeroTelefoneCliente") == telefone.getNumeroTelefone() &&
				rs.getInt("dddTelefoneCliente") == telefone.getDdd().getNumeroDDD()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private boolean emailExiste(Email email) {
		try {
			ResultSet rs = new EmailClienteDAO().buscarEmailPorPK(email, connection);
			if (rs != null && rs.next() &&
				rs.getString("enderecoEmailCliente").equals(email.getEnderecoEmail())) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private Cliente criarClientePJ(ResultSet rs) {
		Cliente cliente = new Cliente();
		cliente.setDadosCliente(new PessoaJuridica(){});
		try {
			cliente.setIdCliente(rs.getInt("idCliente"));

			EnderecoEspecifico enderecoEspecifico = new EnderecoEspecificoControle().criarEnderecoEspecificoCliente(rs);
			List<Email> listaEmails = new EmailClienteControle().recuperarEmailsPorCliente(cliente);
			List<Telefone> listaTelefones = new TelefoneClienteControle().recuperarTelefonesPorCliente(cliente);
			CNPJ cnpj = new CNPJ(rs.getString("cnpjCliente"));

			cliente.setNomeFantasia(rs.getString("nomeFantasiaCliente"));
			cliente.setNomeRazaoSocial(rs.getString("nomeRazaoSocialCliente"));
			cliente.setEnderecoPrincipal(enderecoEspecifico);
			cliente.setListaEmails(listaEmails);
			cliente.setListaTelefones(listaTelefones);
			cliente.setCnpj(cnpj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cliente;
	}

	private Cliente criarClientePF(ResultSet rs) {
		Cliente cliente = new Cliente();
		cliente.setDadosCliente(new PessoaFisica(){});
		try {
			cliente.setIdCliente(rs.getInt("idCliente"));

			EnderecoEspecifico enderecoEspecifico = new EnderecoEspecificoControle().criarEnderecoEspecificoCliente(rs);
			List<Email> listaEmails = new EmailClienteControle().recuperarEmailsPorCliente(cliente);
			List<Telefone> listaTelefones = new TelefoneClienteControle().recuperarTelefonesPorCliente(cliente);
			Sexo sexo = new Sexo(rs.getString("sexoCliente"));
			CPF cpf = new CPF(rs.getString("cpfCliente"));
			NomePessoa nomePessoa = new NomePessoa(rs.getString("nomeCliente"));

			cliente.setEnderecoPrincipal(enderecoEspecifico);
			cliente.setListaEmails(listaEmails);
			cliente.setListaTelefones(listaTelefones);
			cliente.setSexo(sexo);
			cliente.setCpf(cpf);
			cliente.setNomePessoa(nomePessoa);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cliente;
	}

	public void salvarCliente(Cliente cliente) throws Exception {
		if (cliente.isPessoaJuridica()) {
			validarPessoaJuridica((PessoaJuridica) cliente.getDadosCliente());

			if (nomeRazaoSocialExiste(cliente)) {
				throw new Exception("Nome razão social já existe");
			} else if (cnpjExiste(cliente)) {
				throw new Exception("Número CNPJ já existe");
			}
		} else  { // cliente é pessoa física
			validarPessoaFisica((PessoaFisica) cliente.getDadosCliente());

			if (cpfExiste(cliente)) {
				throw new Exception("CPF já existe");
			}
		}

		for (Email e : cliente.getListaEmails()) {
			if (emailExiste(e)) {
				throw new Exception("E-mail \"" + e.getEnderecoEmail() + "\" já existe");
			}
		}
		for (Telefone t : cliente.getListaTelefones()) {
			if (telefoneExiste(t)) {
				throw new Exception("Telefone \"" + t.getNumeroTelefone() + "\" já existe");
			}
		}

		new ClienteDAO().inserirCliente(cliente, connection);
		new EmailClienteControle().salvarListaEmailCliente(cliente.getListaEmails(), cliente);
		new TelefoneClienteControle().salvarListaTelefoneCliente(cliente.getListaTelefones(), cliente);
		connection.close();
	}

	public Cliente recuperarClientePorPK(Cliente cliente) throws Exception {
		ResultSet rs = new ClienteDAO().buscarClientePorPK(cliente.getIdCliente(), connection);
		Cliente clienteRecuperado;
		if (rs != null && rs.next()) {
			if (rs.getBoolean("ehPessoaJuridica")) {
				clienteRecuperado = criarClientePJ(rs);
			} else { // cliente é pessoa física
				clienteRecuperado = criarClientePF(rs);
			}
			connection.close();
			return clienteRecuperado;
		} else {
			connection.close();
			throw new Exception("Cliente não encontrado");
		}
	}

	private List<Cliente> criarListaClientes(ResultSet rs) {
		List<Cliente> listaClientes = new ArrayList<>();
		Cliente cliente;
		try {
			while (rs.next()) {
				if (rs.getBoolean("ehPessoaJuridica")) {
					cliente = criarClientePJ(rs);
				} else { // cliente é pessoa física
					cliente = criarClientePF(rs);
				}
				listaClientes.add(cliente);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listaClientes;
	}

	public List<Cliente> recuperarTodosClientes() throws Exception {
		ResultSet rs = new ClienteDAO().buscarTodosClientes(connection);
		if (rs != null && rs.next()) {
			rs.previous();
			List<Cliente> listaClientes = criarListaClientes(rs);
			connection.close();
			return listaClientes;
		} else {
			connection.close();
			return null;
		}
	}

	public int modificarCliente(Cliente cliente) throws Exception {
		Cliente novoCliente;
		if (cliente.isPessoaJuridica()) {
			novoCliente = new Cliente(cliente.getIdCliente(), cliente.getListaEmails(), cliente.getListaTelefones(),
					cliente.getEnderecoPrincipal(), cliente.getNomeFantasia(), cliente.getNomeRazaoSocial(),
					cliente.getCnpj());

			validarPessoaJuridica((PessoaJuridica) cliente.getDadosCliente());

		} else { // cliente é pessoa física
			novoCliente = new Cliente(cliente.getIdCliente(), cliente.getListaEmails(), cliente.getListaTelefones(),
				cliente.getEnderecoPrincipal(), cliente.getSexo(), cliente.getCpf(), cliente.getNomePessoa());

			validarPessoaFisica((PessoaFisica) cliente.getDadosCliente());

		}

		int updateCount = new ClienteDAO().atualizarCliente(novoCliente, connection);
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updateCount;
	}

	public int removerCliente(Cliente cliente) {
		int updateCount = new ClienteDAO().deletarCliente(cliente, connection);
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updateCount;
	}

}

