package unioeste.geral.pessoa.manager;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.pessoa.CNPJ;
import unioeste.geral.bo.pessoa.EnderecoEspecifico;
import unioeste.geral.bo.pessoa.Fornecedor;
import unioeste.geral.bo.pessoa.PessoaJuridica;
import unioeste.geral.pessoa.dao.FornecedorDAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FornecedorControle {
    private final Connection connection;
    private static final String URL_BD_FATURAMENTO = "jdbc:mysql://localhost/faturamento";
    private static final String USUARIO_BD = "root";
    private static final String SENHA_BD = "";

    public FornecedorControle() {
        this.connection = new Conector().getConnection(URL_BD_FATURAMENTO, USUARIO_BD, SENHA_BD);
    }

	public boolean isNomeFantasiaValido(String nomeFantasia) {
		// \p{L}: expressão regular para qualquer letra Unicode Latin-1 Supplement
		return nomeFantasia.matches("[\\p{L}\\w\\s-]+") && nomeFantasia.length() <= 45;
	}

	public boolean isNomeRazaoSocialValido(String nomeRazaoSocial) {
		return nomeRazaoSocial.matches("[\\p{L}\\w\\s-]+") && nomeRazaoSocial.length() <= 45;
	}

	public int isCNPJValido(CNPJ cnpj) {
		String numeroCNPJFormatado = cnpj.getNumeroCNPJFormatado();

		if (numeroCNPJFormatado.length() != 18) {
			return 1;
		} else if (!numeroCNPJFormatado.matches("[0-9]{2}\\.[0-9]{3}\\.[0-9]{3}/[0-9]{4}-[0-9]{2}")) {
			return 2;
		}
		return 0;
	}

	public void validarPessoaJuridica(PessoaJuridica pessoaJuridica) throws Exception {
		if (!isNomeFantasiaValido(pessoaJuridica.getNomeFantasia())) {
			throw new Exception("Nome fantasia \"" + pessoaJuridica.getNomeFantasia() + "\" inválido");
		} else if (!isNomeRazaoSocialValido(pessoaJuridica.getNomeRazaoSocial())) {
			throw new Exception("Nome razão social \"" + pessoaJuridica.getNomeRazaoSocial() + "\" inválido");
		} else if (isCNPJValido(pessoaJuridica.getCnpj()) == 1) {
			throw new Exception("Número CNPJ \"" + pessoaJuridica.getCnpj().getNumeroCNPJFormatado() + "\" possui tamanho inválido");
		} else if (isCNPJValido(pessoaJuridica.getCnpj()) == 2) {
			throw new Exception("Número CNPJ \"" + pessoaJuridica.getCnpj().getNumeroCNPJFormatado()  + "\" possui formato inválido");
		}
	}

	private boolean cnpjExiste(Fornecedor fornecedor) {
		try {
			ResultSet rs = new FornecedorDAO().buscarTodosFornecedores(connection);
			while (rs.next()) {
				if (rs.getString("cnpjFornecedor").equals(fornecedor.getCnpj().getNumeroCNPJFormatado())) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private boolean nomeRazaoSocialExiste(Fornecedor fornecedor) {
		try {
			ResultSet rs = new FornecedorDAO().buscarTodosFornecedores(connection);
			while (rs.next()) {
				if (rs.getString("nomeRazaoSocialFornecedor").equals(fornecedor.getNomeRazaoSocial())) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

    public Fornecedor criarFornecedor(ResultSet rs) {
		Fornecedor fornecedor = null;
		try {
			EnderecoEspecifico enderecoEspecifico = new EnderecoEspecificoControle().criarEnderecoEspecificoFornecedor(rs);
			CNPJ cnpj = new CNPJ(rs.getString("cnpjFornecedor"));

			fornecedor = new Fornecedor(rs.getInt("idFornecedor"), rs.getString("nomeFantasiaFornecedor"),
					rs.getString("nomeRazaoSocialFornecedor"), cnpj, enderecoEspecifico);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fornecedor;
    }

    public void salvarFornecedor(Fornecedor fornecedor) throws Exception {
		validarPessoaJuridica(fornecedor);

		if (nomeRazaoSocialExiste(fornecedor)) {
			throw new Exception("Nome razão social já existe");
		} else if (cnpjExiste(fornecedor)) {
			throw new Exception("Número CNPJ já existe");
		}

		new FornecedorDAO().inserirFornecedor(fornecedor, connection);
		connection.close();
    }

    public Fornecedor recuperarFornecedorPorPK(Fornecedor fornecedor) throws Exception {
        ResultSet rs = new FornecedorDAO().buscarFornecedorPorPK(fornecedor.getIdFornecedor(), connection);
        if (rs != null && rs.next()) {
			Fornecedor fornecedorRecuperado = criarFornecedor(rs);
			connection.close();
			return fornecedorRecuperado;
		} else {
			connection.close();
			throw new Exception("Fornecedor não encontrado");
		}
    }

	public Fornecedor recuperarFornecedorPorAtributo(String atributo, Object valor) throws Exception {
		ResultSet rs = new FornecedorDAO().buscarFornecedorPorAtributo(atributo, valor, connection);
		if (rs != null && rs.next()) {
			Fornecedor fornecedorRecuperado = criarFornecedor(rs);
			connection.close();
			return fornecedorRecuperado;
		} else {
			connection.close();
			throw new Exception("Fornecedor não encontrado");
		}
	}

    private List<Fornecedor> criarListaFornecedores(ResultSet rs) {
		List<Fornecedor> listaFornecedor = new ArrayList<>();
		try {
			while (rs.next()) {
				Fornecedor fornecedor = criarFornecedor(rs);
				listaFornecedor.add(fornecedor);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listaFornecedor;
	}

    public List<Fornecedor> recuperarTodosFornecedores() throws Exception {
        ResultSet rs = new FornecedorDAO().buscarTodosFornecedores(connection);
		if (rs != null && rs.next()) {
			rs.previous();
			List<Fornecedor> listaFornecedor = criarListaFornecedores(rs);
			connection.close();
			return listaFornecedor;
		} else {
			connection.close();
			return null;
		}
	}

    public int modificarFornecedor(Fornecedor fornecedor) throws Exception {
        Fornecedor novoFornecedor = new Fornecedor(fornecedor.getIdFornecedor(), fornecedor.getNomeFantasia(),
				fornecedor.getNomeRazaoSocial(), fornecedor.getCnpj(), fornecedor.getEnderecoPrincipal());

		validarPessoaJuridica(fornecedor);

		int updateCount = new FornecedorDAO().atualizarFornecedor(novoFornecedor, connection);
		connection.close();
        return updateCount;
    }

    public int removerFornecedor(Fornecedor fornecedor) {
		int updateCount = new FornecedorDAO().deletarFornecedor(fornecedor, connection);
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updateCount;
    }

}
