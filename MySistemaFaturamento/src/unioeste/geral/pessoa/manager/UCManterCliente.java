package unioeste.geral.pessoa.manager;

import unioeste.geral.bo.pessoa.Cliente;

import java.util.List;

public class UCManterCliente {

	public void cadastrarCliente(Cliente cliente) throws Exception {
		new ClienteControle().salvarCliente(cliente);
	}

	public Cliente obterCliente(Cliente cliente) throws Exception {
		Cliente clienteObtido = new ClienteControle().recuperarClientePorPK(cliente);
		if (clienteObtido == null) {
			throw new Exception("Cliente não encontrado");
		}
		return clienteObtido;
	}

	public List<Cliente> obterTodosClientes() throws Exception {
		List<Cliente> listaCliente = new ClienteControle().recuperarTodosClientes();
		if (listaCliente != null) {
			return listaCliente;
		} else {
			throw new Exception("Nenhum cliente cadastrado");
		}
	}

	public void alterarCliente(Cliente cliente) throws Exception {
		if (new ClienteControle().modificarCliente(cliente) == 0) {
			throw new Exception("Nenhum cliente alterado");
		}
	}

	public void excluirCliente(Cliente cliente) throws Exception {
		if (new ClienteControle().removerCliente(cliente) == 0) {
			throw new Exception("Nenhum cliente excluído");
		}
	}

}
