package unioeste.geral.pessoa.manager;

import unioeste.geral.bo.pessoa.Fornecedor;

import javax.ejb.Stateless;
import java.util.List;

@Stateless(name = "UCManterFornecedorEJB")
public class UCManterFornecedorBean implements UCManterFornecedorBeanRemote {

	@Override
    public void cadastrarFornecedor(Fornecedor fornecedor) throws Exception {
		new FornecedorControle().salvarFornecedor(fornecedor);
	}

	@Override
    public Fornecedor obterFornecedor(Fornecedor fornecedor) throws Exception {
		Fornecedor fornecedorObtido = new FornecedorControle().recuperarFornecedorPorPK(fornecedor);
		if (fornecedorObtido == null) {
			throw new Exception("Fornecedor não encontrado");
		}
        return fornecedorObtido;
    }

	@Override
    public List<Fornecedor> obterTodosFornecedores() throws Exception {
		List<Fornecedor> listaFornecedor = new FornecedorControle().recuperarTodosFornecedores();
		if (listaFornecedor != null) {
			return listaFornecedor;
		} else {
			throw new Exception("Nenhum fornecedor cadastrado");
		}
    }

	@Override
    public void alterarFornecedor(Fornecedor fornecedor) throws Exception {
		if (new FornecedorControle().modificarFornecedor(fornecedor) == 0) {
			throw new Exception("Nenhum fornecedor alterado");
		}
    }

	@Override
    public void excluirFornecedor(Fornecedor fornecedor) throws Exception {
		if (new FornecedorControle().removerFornecedor(fornecedor) == 0) {
			throw new Exception("Nenhum fornecedor excluído");
		}
    }

}