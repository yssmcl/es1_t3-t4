package unioeste.geral.pessoa.dao;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.pessoa.Fornecedor;

import java.sql.*;

public class FornecedorDAO {
    public void inserirFornecedor(Fornecedor objeto, Connection connection) {
        String sql =
                "INSERT INTO Fornecedor (" +
                    "nomeFantasiaFornecedor," +
                    "nomeRazaoSocialFornecedor," +
                    "cnpjFornecedor," +
                    "nroEnderecoFornecedor," +
                    "complementoEnderecoFornecedor," +
                    "Endereco_idEndereco" +
                ") VALUES (?,?,?,?,?,?)";

		try (PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			stmt.setString(++i, objeto.getNomeFantasia());
			stmt.setString(++i, objeto.getNomeRazaoSocial());
			stmt.setString(++i, objeto.getCnpj().getNumeroCNPJFormatado());
			stmt.setString(++i, objeto.getEnderecoPrincipal().getNroEndereco());
			stmt.setString(++i, objeto.getEnderecoPrincipal().getComplementoEndereco());
			stmt.setInt(++i, objeto.getEnderecoPrincipal().getEndereco().getIdEndereco());

			stmt.execute();

			ResultSet rs = stmt.getGeneratedKeys();
			rs.next();
			objeto.setIdFornecedor(rs.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }

    public ResultSet buscarFornecedorPorPK(int idFornecedor, Connection connection) {
		//language=MySQL
        String sql =
                "SELECT * " +
                "FROM Fornecedor " +
                "WHERE idFornecedor = ?";
		ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection, idFornecedor);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

	public ResultSet buscarFornecedorPorAtributo(String atributo, Object valor, Connection connection) {
		//language=MySQL
		ResultSet rs = null;
		try {
			rs = new Conector().buscarDados("Fornecedor", atributo, valor, connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}

    public ResultSet buscarTodosFornecedores(Connection connection) {
		//language=MySQL
        String sql =
                "SELECT * " +
                "FROM Fornecedor";
		ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public int atualizarFornecedor(Fornecedor objeto, Connection connection) {
        String sql =
                "UPDATE Fornecedor " +
                "SET " +
                    "nomeFantasiaFornecedor = ?, " +
                    "nomeRazaoSocialFornecedor = ?, " +
                    "cnpjFornecedor = ?, " +
                    "nroEnderecoFornecedor = ?, " +
                    "complementoEnderecoFornecedor = ?, " +
                    "Endereco_idEndereco = ? " +
                "WHERE idFornecedor = ?";

		int updateCount = 0;
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			int i = 0;
			stmt.setString(++i, objeto.getNomeFantasia());
			stmt.setString(++i, objeto.getNomeRazaoSocial());
			stmt.setString(++i, objeto.getCnpj().getNumeroCNPJFormatado());
			stmt.setString(++i, objeto.getEnderecoPrincipal().getNroEndereco());
			stmt.setString(++i, objeto.getEnderecoPrincipal().getComplementoEndereco());
			stmt.setInt(++i, objeto.getEnderecoPrincipal().getEndereco().getIdEndereco());
			stmt.setInt(++i, objeto.getIdFornecedor());

			updateCount = stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updateCount;
    }

    public int deletarFornecedor(Fornecedor objeto, Connection connection) {
        String sql =
                "DELETE FROM Fornecedor " +
                "WHERE idFornecedor = ?";

		int updateCount = 0;
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			stmt.setInt(1, objeto.getIdFornecedor());
			updateCount = stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updateCount;
    }
}
