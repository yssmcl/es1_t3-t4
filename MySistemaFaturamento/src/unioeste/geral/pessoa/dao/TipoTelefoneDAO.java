package unioeste.geral.pessoa.dao;

import unioeste.apoio.bd.Conector;

import java.sql.Connection;
import java.sql.ResultSet;

public class TipoTelefoneDAO {
	
    public ResultSet buscarTipoTelefonePorPK(int idTipoTelefone, Connection connection) {
		//language=MySQL
		String sql =
                "SELECT * " +
                "FROM TipoTelefone " +
                "WHERE idTipoTelefone = ?";
		ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection, idTipoTelefone);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }
    
}
