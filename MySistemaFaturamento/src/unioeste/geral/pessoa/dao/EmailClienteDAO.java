package unioeste.geral.pessoa.dao;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.pessoa.Cliente;
import unioeste.geral.bo.pessoa.Email;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EmailClienteDAO {
    public void inserirEmailCliente(Email objeto, Connection connection) {
        String sql =
                "INSERT INTO EmailCliente (" +
                    "enderecoEmailCliente," +
                    "Cliente_idCliente" +
                ") VALUES (?,?)";

		try {
			try (PreparedStatement stmt = connection.prepareStatement(sql)) {
				int i = 0;
				stmt.setString(++i, objeto.getEnderecoEmail());
				stmt.setInt(++i, objeto.getCliente().getIdCliente());

				stmt.execute();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ResultSet buscarEmailPorPK(Email email, Connection connection) {
		//language=MySQL
		String sql =
				"SELECT * " +
				"FROM EmailCliente " +
				"WHERE enderecoEmailCliente = ?";
		ResultSet rs = null;
		try {
			rs = new Conector().buscarDados(sql, connection, email.getEnderecoEmail());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}

	public ResultSet buscarEmailsPorCliente(Cliente cliente, Connection connection) {
		//language=MySQL
		String sql =
				"SELECT * " +
				"FROM EmailCliente " +
				"WHERE Cliente_idCliente = ?";
		ResultSet rs = null;
		try {
			rs = new Conector().buscarDados(sql, connection, cliente.getIdCliente());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}

}
