package unioeste.geral.pessoa.dao;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.pessoa.Cliente;
import unioeste.geral.bo.pessoa.Telefone;

import java.sql.*;

public class TelefoneClienteDAO {
    public void inserirTelefoneCliente(Telefone objeto, Connection connection) {
        String sql =
                "INSERT INTO TelefoneCliente (" +
                    "numeroTelefoneCliente," +
                    "dddTelefoneCliente," +
                    "TipoTelefone_idTipoTelefone," +
					"Cliente_idCliente" +
                ") VALUES (?,?,?,?)";

		try (PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			stmt.setInt(++i, objeto.getNumeroTelefone());
			stmt.setInt(++i, objeto.getDdd().getNumeroDDD());
			stmt.setInt(++i, objeto.getTipoTelefone().getIdTipoTelefone());
			stmt.setInt(++i, objeto.getCliente().getIdCliente());

			stmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }

    public ResultSet buscarTelefonePorPK(Telefone telefone, Connection connection) {
		//language=MySQL
		String sql =
                "SELECT * " +
                "FROM TelefoneCliente " +
                "WHERE numeroTelefoneCliente = ?";
		ResultSet rs = null;
        try {
            rs = new Conector().buscarDados(sql, connection, telefone.getNumeroTelefone());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

	public ResultSet buscarTelefonesPorCliente(Cliente cliente, Connection connection) {
		//language=MySQL
		String sql =
				"SELECT * " +
				"FROM TelefoneCliente " +
				"WHERE Cliente_idCliente = ?";
		ResultSet rs = null;
		try {
			rs = new Conector().buscarDados(sql, connection, cliente.getIdCliente());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}

}
