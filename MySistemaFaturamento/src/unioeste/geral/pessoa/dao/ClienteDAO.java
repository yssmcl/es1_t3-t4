package unioeste.geral.pessoa.dao;

import unioeste.apoio.bd.Conector;
import unioeste.geral.bo.pessoa.Cliente;

import java.sql.*;

public class ClienteDAO {
	private void setAtributosCliente(Cliente objeto, PreparedStatement stmt) {
		try {
			int i = 0;
			if (objeto.isPessoaFisica()) {
				stmt.setString(++i, objeto.getNomePessoa().getNomeCompleto());
				stmt.setString(++i, objeto.getCpf().getNumeroCPFFormatado());
				stmt.setString(++i, String.valueOf(objeto.getSexo().getSiglaSexo()));
				stmt.setNull(++i, Types.VARCHAR);
				stmt.setNull(++i, Types.VARCHAR);
				stmt.setNull(++i, Types.VARCHAR);
				stmt.setByte(++i, (byte) 1);
				stmt.setByte(++i, (byte) 0);
			} else {
				stmt.setNull(++i, Types.VARCHAR);
				stmt.setNull(++i, Types.VARCHAR);
				stmt.setNull(++i, Types.VARCHAR);
				stmt.setString(++i, objeto.getNomeFantasia());
				stmt.setString(++i, objeto.getNomeRazaoSocial());
				stmt.setString(++i, objeto.getCnpj().getNumeroCNPJFormatado());
				stmt.setByte(++i, (byte) 0);
				stmt.setByte(++i, (byte) 1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void inserirCliente(Cliente objeto, Connection connection) {
		String sql =
			"INSERT INTO Cliente (" +
				"nomeCliente," +
				"cpfCliente," +
				"sexoCliente," +
				"nomeFantasiaCliente," +
				"nomeRazaoSocialCliente," +
				"cnpjCliente," +
				"ehPessoaFisica," +
				"ehPessoaJuridica," +
				"nroEnderecoCliente," +
				"complementoEnderecoCliente," +
				"Endereco_idEndereco" +
			") VALUES (?,?,?,?,?,?,?,?,?,?,?)";

		try (PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			setAtributosCliente(objeto, stmt);
			stmt.setString(9, objeto.getEnderecoPrincipal().getNroEndereco());
			stmt.setString(10, objeto.getEnderecoPrincipal().getComplementoEndereco());
			stmt.setInt(11, objeto.getEnderecoPrincipal().getEndereco().getIdEndereco());

			stmt.execute();

			ResultSet rs = stmt.getGeneratedKeys();
			rs.next();
			objeto.setIdCliente(rs.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ResultSet buscarClientePorPK(int idCliente, Connection connection) {
		//language=MySQL
		String sql =
			"SELECT * " +
			"FROM Cliente " +
			"WHERE idCliente = ?";
		ResultSet rs = null;
		try {
			rs = new Conector().buscarDados(sql, connection, idCliente);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}

	public ResultSet buscarTodosClientes(Connection connection) {
		//language=MySQL
		String sql =
			"SELECT * " +
			"FROM Cliente";
		ResultSet rs = null;
		try {
			rs = new Conector().buscarDados(sql, connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}

	public int atualizarCliente(Cliente objeto, Connection connection) {
		String sql =
			"UPDATE Cliente " +
			"SET " +
				"nomeCliente = ?," +
				"cpfCliente = ?," +
				"sexoCliente = ?," +
				"nomeFantasiaCliente = ?," +
				"nomeRazaoSocialCliente = ?," +
				"cnpjCliente = ?," +
				"ehPessoaFisica = ?," +
				"ehPessoaJuridica = ?," +
				"nroEnderecoCliente = ?," +
				"complementoEnderecoCliente = ?," +
				"Endereco_idEndereco = ? " +
			"WHERE idCliente = ?";

		int updateCount = 0;
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			setAtributosCliente(objeto, stmt);
			stmt.setString(9, objeto.getEnderecoPrincipal().getNroEndereco());
			stmt.setString(10, objeto.getEnderecoPrincipal().getComplementoEndereco());
			stmt.setInt(11, objeto.getEnderecoPrincipal().getEndereco().getIdEndereco());
			stmt.setInt(12, objeto.getIdCliente());

			updateCount = stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updateCount;
	}

	public int deletarCliente(Cliente objeto, Connection connection) {
		String sql =
			"DELETE FROM Cliente " +
			"WHERE idCliente = ?";

		int updateCount = 0;
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			stmt.setInt(1, objeto.getIdCliente());
			updateCount = stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return updateCount;
	}
}
